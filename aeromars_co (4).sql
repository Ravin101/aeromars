-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2021 at 08:01 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aeromars_co`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Ravi', 'admin@gmail.com', '$2a$04$jxMBra.MDe5kJIlwAfZPk.5Bvhy8FNqc/5ilKNlSSgJQXdgQ9L7T6', '2020-11-10 01:16:14', '2020-11-10 01:16:14');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) NOT NULL DEFAULT '0',
  `subcategory_id` int(10) NOT NULL DEFAULT '0',
  `innercategory_id` int(10) NOT NULL DEFAULT '0',
  `sub_inner_category_id` int(10) NOT NULL DEFAULT '0',
  `banner_cat_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `admin_id`, `title`, `type`, `description`, `image`, `thumb`, `category_id`, `subcategory_id`, `innercategory_id`, `sub_inner_category_id`, `banner_cat_type`, `category`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(9, 1, 'Raaaaj', 'banner', 'Jvhjvhjvh', 'image1608096445.jpeg', 'image1608096445.jpeg', 3, 0, 0, 0, 'main', 3, 1, 1, '2020-12-16 03:27:25', '2020-12-16 04:32:12'),
(10, 1, 'Raaaaj', 'banner', 'Jvhjvhjvh', 'image1608096468.jpeg', 'image1608096468.jpeg', 3, 0, 0, 0, 'main', 3, 1, 1, '2020-12-16 03:27:49', '2020-12-16 04:16:01'),
(11, 1, 'Carvan Radio', 'banner', 'Very good', 'image1608100328.jpeg', 'image1608100328.jpeg', 3, 0, 0, 0, 'main', 3, 1, 1, '2020-12-16 04:32:08', '2020-12-18 10:27:03'),
(12, 1, 'Nokia', 'banner', 'Ultimate gagets', 'images1608294405.png', '1608294405.png', 8, 0, 0, 0, 'main', 8, 1, 0, '2020-12-18 10:26:45', '2020-12-18 10:26:45'),
(13, 1, 'Sa RE Ga Ma Carvaan', 'banner', 'It having thousands of songs', 'images1608294566.jpeg', '1608294566.jpeg', 6, 23, 0, 0, 'sub', 23, 1, 1, '2020-12-18 10:29:27', '2021-01-12 02:17:25'),
(14, 1, 'Smart TV', 'banner', 'Having Quality Features', 'images1610425134.png', '1610425134.png', 2, 7, 0, 0, 'sub', 7, 1, 0, '2021-01-12 02:18:55', '2021-01-12 02:18:55'),
(15, 1, 'Wqsadfdgfav', 'banner', 'Sdfvcwrgtrwcvsf', 'images1611134380.png', '1611134380.png', 11, 31, 0, 0, 'sub', 31, 1, 0, '2021-01-20 07:19:42', '2021-01-20 07:19:42');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `thumbnail`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Electronic', '1607767735.jpg', '1607767735.jpg', 1, 1, '2020-12-12 08:08:55', '2021-01-11 12:31:12'),
(2, 'Electronic Gagets', 'images1608284459.png', '1608284459.png', 1, 0, '2020-12-12 08:10:17', '2020-12-18 07:54:31'),
(4, 'Seasonal', '1608140811.png', '1608140811.png', 1, 0, '2020-12-16 15:46:51', '2020-12-16 15:48:04'),
(5, 'Microphones', '1608140826.png', '1608140826.png', 1, 1, '2020-12-16 15:47:06', '2020-12-16 15:47:51'),
(6, 'Audio Players', 'images1608276533.jpeg', '1608276533.jpeg', 1, 1, '2020-12-18 05:28:54', '2020-12-18 06:17:46'),
(7, 'Electronic Gagets', 'images1608283681.png', '1608283681.png', 1, 1, '2020-12-18 07:28:01', '2020-12-18 07:28:01'),
(8, 'Cellphones', 'images1608292815.png', '1608292815.png', 1, 1, '2020-12-18 10:00:15', '2020-12-18 10:00:15'),
(9, 'Air Conditioner', 'images1608313829.png', '1608313829.png', 1, 1, '2020-12-18 15:50:29', '2020-12-18 15:50:29'),
(10, 'Chemical', 'images1610361160.jpeg', '1610361160.jpeg', 1, 0, '2021-01-11 08:32:40', '2021-01-11 08:32:40'),
(11, 'Home appliances', 'images1610361197.jpeg', '1610361197.jpeg', 1, 0, '2021-01-11 08:33:19', '2021-01-11 08:33:19'),
(12, 'Lighting appliances', 'images1610361252.jpeg', '1610361252.jpeg', 1, 0, '2021-01-11 08:34:12', '2021-01-11 08:34:12'),
(13, 'Geysors', 'images1611212272.png', '1611212272.png', 1, 0, '2021-01-21 04:57:52', '2021-01-21 04:57:52');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`, `created_at`, `updated_at`) VALUES
(1, 'North and Middle Andaman', 32, NULL, NULL),
(2, 'South Andaman', 32, NULL, NULL),
(3, 'Nicobar', 32, NULL, NULL),
(4, 'Adilabad', 1, NULL, NULL),
(5, 'Anantapur', 1, NULL, NULL),
(6, 'Chittoor', 1, NULL, NULL),
(7, 'East Godavari', 1, NULL, NULL),
(8, 'Guntur', 1, NULL, NULL),
(9, 'Hyderabad', 1, NULL, NULL),
(10, 'Kadapa', 1, NULL, NULL),
(11, 'Karimnagar', 1, NULL, NULL),
(12, 'Khammam', 1, NULL, NULL),
(13, 'Krishna', 1, NULL, NULL),
(14, 'Kurnool', 1, NULL, NULL),
(15, 'Mahbubnagar', 1, NULL, NULL),
(16, 'Medak', 1, NULL, NULL),
(17, 'Nalgonda', 1, NULL, NULL),
(18, 'Nellore', 1, NULL, NULL),
(19, 'Nizamabad', 1, NULL, NULL),
(20, 'Prakasam', 1, NULL, NULL),
(21, 'Rangareddi', 1, NULL, NULL),
(22, 'Srikakulam', 1, NULL, NULL),
(23, 'Vishakhapatnam', 1, NULL, NULL),
(24, 'Vizianagaram', 1, NULL, NULL),
(25, 'Warangal', 1, NULL, NULL),
(26, 'West Godavari', 1, NULL, NULL),
(27, 'Anjaw', 3, NULL, NULL),
(28, 'Changlang', 3, NULL, NULL),
(29, 'East Kameng', 3, NULL, NULL),
(30, 'Lohit', 3, NULL, NULL),
(31, 'Lower Subansiri', 3, NULL, NULL),
(32, 'Papum Pare', 3, NULL, NULL),
(33, 'Tirap', 3, NULL, NULL),
(34, 'Dibang Valley', 3, NULL, NULL),
(35, 'Upper Subansiri', 3, NULL, NULL),
(36, 'West Kameng', 3, NULL, NULL),
(37, 'Barpeta', 2, NULL, NULL),
(38, 'Bongaigaon', 2, NULL, NULL),
(39, 'Cachar', 2, NULL, NULL),
(40, 'Darrang', 2, NULL, NULL),
(41, 'Dhemaji', 2, NULL, NULL),
(42, 'Dhubri', 2, NULL, NULL),
(43, 'Dibrugarh', 2, NULL, NULL),
(44, 'Goalpara', 2, NULL, NULL),
(45, 'Golaghat', 2, NULL, NULL),
(46, 'Hailakandi', 2, NULL, NULL),
(47, 'Jorhat', 2, NULL, NULL),
(48, 'Karbi Anglong', 2, NULL, NULL),
(49, 'Karimganj', 2, NULL, NULL),
(50, 'Kokrajhar', 2, NULL, NULL),
(51, 'Lakhimpur', 2, NULL, NULL),
(52, 'Marigaon', 2, NULL, NULL),
(53, 'Nagaon', 2, NULL, NULL),
(54, 'Nalbari', 2, NULL, NULL),
(55, 'North Cachar Hills', 2, NULL, NULL),
(56, 'Sibsagar', 2, NULL, NULL),
(57, 'Sonitpur', 2, NULL, NULL),
(58, 'Tinsukia', 2, NULL, NULL),
(59, 'Araria', 4, NULL, NULL),
(60, 'Aurangabad', 4, NULL, NULL),
(61, 'Banka', 4, NULL, NULL),
(62, 'Begusarai', 4, NULL, NULL),
(63, 'Bhagalpur', 4, NULL, NULL),
(64, 'Bhojpur', 4, NULL, NULL),
(65, 'Buxar', 4, NULL, NULL),
(66, 'Darbhanga', 4, NULL, NULL),
(67, 'Purba Champaran', 4, NULL, NULL),
(68, 'Gaya', 4, NULL, NULL),
(69, 'Gopalganj', 4, NULL, NULL),
(70, 'Jamui', 4, NULL, NULL),
(71, 'Jehanabad', 4, NULL, NULL),
(72, 'Khagaria', 4, NULL, NULL),
(73, 'Kishanganj', 4, NULL, NULL),
(74, 'Kaimur', 4, NULL, NULL),
(75, 'Katihar', 4, NULL, NULL),
(76, 'Lakhisarai', 4, NULL, NULL),
(77, 'Madhubani', 4, NULL, NULL),
(78, 'Munger', 4, NULL, NULL),
(79, 'Madhepura', 4, NULL, NULL),
(80, 'Muzaffarpur', 4, NULL, NULL),
(81, 'Nalanda', 4, NULL, NULL),
(82, 'Nawada', 4, NULL, NULL),
(83, 'Patna', 4, NULL, NULL),
(84, 'Purnia', 4, NULL, NULL),
(85, 'Rohtas', 4, NULL, NULL),
(86, 'Saharsa', 4, NULL, NULL),
(87, 'Samastipur', 4, NULL, NULL),
(88, 'Sheohar', 4, NULL, NULL),
(89, 'Sheikhpura', 4, NULL, NULL),
(90, 'Saran', 4, NULL, NULL),
(91, 'Sitamarhi', 4, NULL, NULL),
(92, 'Supaul', 4, NULL, NULL),
(93, 'Siwan', 4, NULL, NULL),
(94, 'Vaishali', 4, NULL, NULL),
(95, 'Pashchim Champaran', 4, NULL, NULL),
(96, 'Bastar', 36, NULL, NULL),
(97, 'Bilaspur', 36, NULL, NULL),
(98, 'Dantewada', 36, NULL, NULL),
(99, 'Dhamtari', 36, NULL, NULL),
(100, 'Durg', 36, NULL, NULL),
(101, 'Jashpur', 36, NULL, NULL),
(102, 'Janjgir-Champa', 36, NULL, NULL),
(103, 'Korba', 36, NULL, NULL),
(104, 'Koriya', 36, NULL, NULL),
(105, 'Kanker', 36, NULL, NULL),
(106, 'Kawardha', 36, NULL, NULL),
(107, 'Mahasamund', 36, NULL, NULL),
(108, 'Raigarh', 36, NULL, NULL),
(109, 'Rajnandgaon', 36, NULL, NULL),
(110, 'Raipur', 36, NULL, NULL),
(111, 'Surguja', 36, NULL, NULL),
(112, 'Diu', 29, NULL, NULL),
(113, 'Daman', 29, NULL, NULL),
(114, 'Central Delhi', 25, NULL, NULL),
(115, 'East Delhi', 25, NULL, NULL),
(116, 'New Delhi', 25, NULL, NULL),
(117, 'North Delhi', 25, NULL, NULL),
(118, 'North East Delhi', 25, NULL, NULL),
(119, 'North West Delhi', 25, NULL, NULL),
(120, 'South Delhi', 25, NULL, NULL),
(121, 'South West Delhi', 25, NULL, NULL),
(122, 'West Delhi', 25, NULL, NULL),
(123, 'North Goa', 26, NULL, NULL),
(124, 'South Goa', 26, NULL, NULL),
(125, 'Ahmedabad', 5, NULL, NULL),
(126, 'Amreli District', 5, NULL, NULL),
(127, 'Anand', 5, NULL, NULL),
(128, 'Banaskantha', 5, NULL, NULL),
(129, 'Bharuch', 5, NULL, NULL),
(130, 'Bhavnagar', 5, NULL, NULL),
(131, 'Dahod', 5, NULL, NULL),
(132, 'The Dangs', 5, NULL, NULL),
(133, 'Gandhinagar', 5, NULL, NULL),
(134, 'Jamnagar', 5, NULL, NULL),
(135, 'Junagadh', 5, NULL, NULL),
(136, 'Kutch', 5, NULL, NULL),
(137, 'Kheda', 5, NULL, NULL),
(138, 'Mehsana', 5, NULL, NULL),
(139, 'Narmada', 5, NULL, NULL),
(140, 'Navsari', 5, NULL, NULL),
(141, 'Patan', 5, NULL, NULL),
(142, 'Panchmahal', 5, NULL, NULL),
(143, 'Porbandar', 5, NULL, NULL),
(144, 'Rajkot', 5, NULL, NULL),
(145, 'Sabarkantha', 5, NULL, NULL),
(146, 'Surendranagar', 5, NULL, NULL),
(147, 'Surat', 5, NULL, NULL),
(148, 'Vadodara', 5, NULL, NULL),
(149, 'Valsad', 5, NULL, NULL),
(150, 'Ambala', 6, NULL, NULL),
(151, 'Bhiwani', 6, NULL, NULL),
(152, 'Faridabad', 6, NULL, NULL),
(153, 'Fatehabad', 6, NULL, NULL),
(154, 'Gurgaon', 6, NULL, NULL),
(155, 'Hissar', 6, NULL, NULL),
(156, 'Jhajjar', 6, NULL, NULL),
(157, 'Jind', 6, NULL, NULL),
(158, 'Karnal', 6, NULL, NULL),
(159, 'Kaithal', 6, NULL, NULL),
(160, 'Kurukshetra', 6, NULL, NULL),
(161, 'Mahendragarh', 6, NULL, NULL),
(162, 'Mewat', 6, NULL, NULL),
(163, 'Panchkula', 6, NULL, NULL),
(164, 'Panipat', 6, NULL, NULL),
(165, 'Rewari', 6, NULL, NULL),
(166, 'Rohtak', 6, NULL, NULL),
(167, 'Sirsa', 6, NULL, NULL),
(168, 'Sonepat', 6, NULL, NULL),
(169, 'Yamuna Nagar', 6, NULL, NULL),
(170, 'Palwal', 6, NULL, NULL),
(171, 'Bilaspur', 7, NULL, NULL),
(172, 'Chamba', 7, NULL, NULL),
(173, 'Hamirpur', 7, NULL, NULL),
(174, 'Kangra', 7, NULL, NULL),
(175, 'Kinnaur', 7, NULL, NULL),
(176, 'Kulu', 7, NULL, NULL),
(177, 'Lahaul and Spiti', 7, NULL, NULL),
(178, 'Mandi', 7, NULL, NULL),
(179, 'Shimla', 7, NULL, NULL),
(180, 'Sirmaur', 7, NULL, NULL),
(181, 'Solan', 7, NULL, NULL),
(182, 'Una', 7, NULL, NULL),
(183, 'Anantnag', 8, NULL, NULL),
(184, 'Badgam', 8, NULL, NULL),
(185, 'Bandipore', 8, NULL, NULL),
(186, 'Baramula', 8, NULL, NULL),
(187, 'Doda', 8, NULL, NULL),
(188, 'Jammu', 8, NULL, NULL),
(189, 'Kargil', 8, NULL, NULL),
(190, 'Kathua', 8, NULL, NULL),
(191, 'Kupwara', 8, NULL, NULL),
(192, 'Leh', 8, NULL, NULL),
(193, 'Poonch', 8, NULL, NULL),
(194, 'Pulwama', 8, NULL, NULL),
(195, 'Rajauri', 8, NULL, NULL),
(196, 'Srinagar', 8, NULL, NULL),
(197, 'Samba', 8, NULL, NULL),
(198, 'Udhampur', 8, NULL, NULL),
(199, 'Bokaro', 34, NULL, NULL),
(200, 'Chatra', 34, NULL, NULL),
(201, 'Deoghar', 34, NULL, NULL),
(202, 'Dhanbad', 34, NULL, NULL),
(203, 'Dumka', 34, NULL, NULL),
(204, 'Purba Singhbhum', 34, NULL, NULL),
(205, 'Garhwa', 34, NULL, NULL),
(206, 'Giridih', 34, NULL, NULL),
(207, 'Godda', 34, NULL, NULL),
(208, 'Gumla', 34, NULL, NULL),
(209, 'Hazaribagh', 34, NULL, NULL),
(210, 'Koderma', 34, NULL, NULL),
(211, 'Lohardaga', 34, NULL, NULL),
(212, 'Pakur', 34, NULL, NULL),
(213, 'Palamu', 34, NULL, NULL),
(214, 'Ranchi', 34, NULL, NULL),
(215, 'Sahibganj', 34, NULL, NULL),
(216, 'Seraikela and Kharsawan', 34, NULL, NULL),
(217, 'Pashchim Singhbhum', 34, NULL, NULL),
(218, 'Ramgarh', 34, NULL, NULL),
(219, 'Bidar', 9, NULL, NULL),
(220, 'Belgaum', 9, NULL, NULL),
(221, 'Bijapur', 9, NULL, NULL),
(222, 'Bagalkot', 9, NULL, NULL),
(223, 'Bellary', 9, NULL, NULL),
(224, 'Bangalore Rural District', 9, NULL, NULL),
(225, 'Bangalore Urban District', 9, NULL, NULL),
(226, 'Chamarajnagar', 9, NULL, NULL),
(227, 'Chikmagalur', 9, NULL, NULL),
(228, 'Chitradurga', 9, NULL, NULL),
(229, 'Davanagere', 9, NULL, NULL),
(230, 'Dharwad', 9, NULL, NULL),
(231, 'Dakshina Kannada', 9, NULL, NULL),
(232, 'Gadag', 9, NULL, NULL),
(233, 'Gulbarga', 9, NULL, NULL),
(234, 'Hassan', 9, NULL, NULL),
(235, 'Haveri District', 9, NULL, NULL),
(236, 'Kodagu', 9, NULL, NULL),
(237, 'Kolar', 9, NULL, NULL),
(238, 'Koppal', 9, NULL, NULL),
(239, 'Mandya', 9, NULL, NULL),
(240, 'Mysore', 9, NULL, NULL),
(241, 'Raichur', 9, NULL, NULL),
(242, 'Shimoga', 9, NULL, NULL),
(243, 'Tumkur', 9, NULL, NULL),
(244, 'Udupi', 9, NULL, NULL),
(245, 'Uttara Kannada', 9, NULL, NULL),
(246, 'Ramanagara', 9, NULL, NULL),
(247, 'Chikballapur', 9, NULL, NULL),
(248, 'Yadagiri', 9, NULL, NULL),
(249, 'Alappuzha', 10, NULL, NULL),
(250, 'Ernakulam', 10, NULL, NULL),
(251, 'Idukki', 10, NULL, NULL),
(252, 'Kollam', 10, NULL, NULL),
(253, 'Kannur', 10, NULL, NULL),
(254, 'Kasaragod', 10, NULL, NULL),
(255, 'Kottayam', 10, NULL, NULL),
(256, 'Kozhikode', 10, NULL, NULL),
(257, 'Malappuram', 10, NULL, NULL),
(258, 'Palakkad', 10, NULL, NULL),
(259, 'Pathanamthitta', 10, NULL, NULL),
(260, 'Thrissur', 10, NULL, NULL),
(261, 'Thiruvananthapuram', 10, NULL, NULL),
(262, 'Wayanad', 10, NULL, NULL),
(263, 'Alirajpur', 11, NULL, NULL),
(264, 'Anuppur', 11, NULL, NULL),
(265, 'Ashok Nagar', 11, NULL, NULL),
(266, 'Balaghat', 11, NULL, NULL),
(267, 'Barwani', 11, NULL, NULL),
(268, 'Betul', 11, NULL, NULL),
(269, 'Bhind', 11, NULL, NULL),
(270, 'Bhopal', 11, NULL, NULL),
(271, 'Burhanpur', 11, NULL, NULL),
(272, 'Chhatarpur', 11, NULL, NULL),
(273, 'Chhindwara', 11, NULL, NULL),
(274, 'Damoh', 11, NULL, NULL),
(275, 'Datia', 11, NULL, NULL),
(276, 'Dewas', 11, NULL, NULL),
(277, 'Dhar', 11, NULL, NULL),
(278, 'Dindori', 11, NULL, NULL),
(279, 'Guna', 11, NULL, NULL),
(280, 'Gwalior', 11, NULL, NULL),
(281, 'Harda', 11, NULL, NULL),
(282, 'Hoshangabad', 11, NULL, NULL),
(283, 'Indore', 11, NULL, NULL),
(284, 'Jabalpur', 11, NULL, NULL),
(285, 'Jhabua', 11, NULL, NULL),
(286, 'Katni', 11, NULL, NULL),
(287, 'Khandwa', 11, NULL, NULL),
(288, 'Khargone', 11, NULL, NULL),
(289, 'Mandla', 11, NULL, NULL),
(290, 'Mandsaur', 11, NULL, NULL),
(291, 'Morena', 11, NULL, NULL),
(292, 'Narsinghpur', 11, NULL, NULL),
(293, 'Neemuch', 11, NULL, NULL),
(294, 'Panna', 11, NULL, NULL),
(295, 'Rewa', 11, NULL, NULL),
(296, 'Rajgarh', 11, NULL, NULL),
(297, 'Ratlam', 11, NULL, NULL),
(298, 'Raisen', 11, NULL, NULL),
(299, 'Sagar', 11, NULL, NULL),
(300, 'Satna', 11, NULL, NULL),
(301, 'Sehore', 11, NULL, NULL),
(302, 'Seoni', 11, NULL, NULL),
(303, 'Shahdol', 11, NULL, NULL),
(304, 'Shajapur', 11, NULL, NULL),
(305, 'Sheopur', 11, NULL, NULL),
(306, 'Shivpuri', 11, NULL, NULL),
(307, 'Sidhi', 11, NULL, NULL),
(308, 'Singrauli', 11, NULL, NULL),
(309, 'Tikamgarh', 11, NULL, NULL),
(310, 'Ujjain', 11, NULL, NULL),
(311, 'Umaria', 11, NULL, NULL),
(312, 'Vidisha', 11, NULL, NULL),
(313, 'Ahmednagar', 12, NULL, NULL),
(314, 'Akola', 12, NULL, NULL),
(315, 'Amrawati', 12, NULL, NULL),
(316, 'Aurangabad', 12, NULL, NULL),
(317, 'Bhandara', 12, NULL, NULL),
(318, 'Beed', 12, NULL, NULL),
(319, 'Buldhana', 12, NULL, NULL),
(320, 'Chandrapur', 12, NULL, NULL),
(321, 'Dhule', 12, NULL, NULL),
(322, 'Gadchiroli', 12, NULL, NULL),
(323, 'Gondiya', 12, NULL, NULL),
(324, 'Hingoli', 12, NULL, NULL),
(325, 'Jalgaon', 12, NULL, NULL),
(326, 'Jalna', 12, NULL, NULL),
(327, 'Kolhapur', 12, NULL, NULL),
(328, 'Latur', 12, NULL, NULL),
(329, 'Mumbai City', 12, NULL, NULL),
(330, 'Mumbai suburban', 12, NULL, NULL),
(331, 'Nandurbar', 12, NULL, NULL),
(332, 'Nanded', 12, NULL, NULL),
(333, 'Nagpur', 12, NULL, NULL),
(334, 'Nashik', 12, NULL, NULL),
(335, 'Osmanabad', 12, NULL, NULL),
(336, 'Parbhani', 12, NULL, NULL),
(337, 'Pune', 12, NULL, NULL),
(338, 'Raigad', 12, NULL, NULL),
(339, 'Ratnagiri', 12, NULL, NULL),
(340, 'Sindhudurg', 12, NULL, NULL),
(341, 'Sangli', 12, NULL, NULL),
(342, 'Solapur', 12, NULL, NULL),
(343, 'Satara', 12, NULL, NULL),
(344, 'Thane', 12, NULL, NULL),
(345, 'Wardha', 12, NULL, NULL),
(346, 'Washim', 12, NULL, NULL),
(347, 'Yavatmal', 12, NULL, NULL),
(348, 'Bishnupur', 13, NULL, NULL),
(349, 'Churachandpur', 13, NULL, NULL),
(350, 'Chandel', 13, NULL, NULL),
(351, 'Imphal East', 13, NULL, NULL),
(352, 'Senapati', 13, NULL, NULL),
(353, 'Tamenglong', 13, NULL, NULL),
(354, 'Thoubal', 13, NULL, NULL),
(355, 'Ukhrul', 13, NULL, NULL),
(356, 'Imphal West', 13, NULL, NULL),
(357, 'East Garo Hills', 14, NULL, NULL),
(358, 'East Khasi Hills', 14, NULL, NULL),
(359, 'Jaintia Hills', 14, NULL, NULL),
(360, 'Ri-Bhoi', 14, NULL, NULL),
(361, 'South Garo Hills', 14, NULL, NULL),
(362, 'West Garo Hills', 14, NULL, NULL),
(363, 'West Khasi Hills', 14, NULL, NULL),
(364, 'Aizawl', 15, NULL, NULL),
(365, 'Champhai', 15, NULL, NULL),
(366, 'Kolasib', 15, NULL, NULL),
(367, 'Lawngtlai', 15, NULL, NULL),
(368, 'Lunglei', 15, NULL, NULL),
(369, 'Mamit', 15, NULL, NULL),
(370, 'Saiha', 15, NULL, NULL),
(371, 'Serchhip', 15, NULL, NULL),
(372, 'Dimapur', 16, NULL, NULL),
(373, 'Kohima', 16, NULL, NULL),
(374, 'Mokokchung', 16, NULL, NULL),
(375, 'Mon', 16, NULL, NULL),
(376, 'Phek', 16, NULL, NULL),
(377, 'Tuensang', 16, NULL, NULL),
(378, 'Wokha', 16, NULL, NULL),
(379, 'Zunheboto', 16, NULL, NULL),
(380, 'Angul', 17, NULL, NULL),
(381, 'Boudh', 17, NULL, NULL),
(382, 'Bhadrak', 17, NULL, NULL),
(383, 'Bolangir', 17, NULL, NULL),
(384, 'Bargarh', 17, NULL, NULL),
(385, 'Baleswar', 17, NULL, NULL),
(386, 'Cuttack', 17, NULL, NULL),
(387, 'Debagarh', 17, NULL, NULL),
(388, 'Dhenkanal', 17, NULL, NULL),
(389, 'Ganjam', 17, NULL, NULL),
(390, 'Gajapati', 17, NULL, NULL),
(391, 'Jharsuguda', 17, NULL, NULL),
(392, 'Jajapur', 17, NULL, NULL),
(393, 'Jagatsinghpur', 17, NULL, NULL),
(394, 'Khordha', 17, NULL, NULL),
(395, 'Kendujhar', 17, NULL, NULL),
(396, 'Kalahandi', 17, NULL, NULL),
(397, 'Kandhamal', 17, NULL, NULL),
(398, 'Koraput', 17, NULL, NULL),
(399, 'Kendrapara', 17, NULL, NULL),
(400, 'Malkangiri', 17, NULL, NULL),
(401, 'Mayurbhanj', 17, NULL, NULL),
(402, 'Nabarangpur', 17, NULL, NULL),
(403, 'Nuapada', 17, NULL, NULL),
(404, 'Nayagarh', 17, NULL, NULL),
(405, 'Puri', 17, NULL, NULL),
(406, 'Rayagada', 17, NULL, NULL),
(407, 'Sambalpur', 17, NULL, NULL),
(408, 'Subarnapur', 17, NULL, NULL),
(409, 'Sundargarh', 17, NULL, NULL),
(410, 'Karaikal', 27, NULL, NULL),
(411, 'Mahe', 27, NULL, NULL),
(412, 'Puducherry', 27, NULL, NULL),
(413, 'Yanam', 27, NULL, NULL),
(414, 'Amritsar', 18, NULL, NULL),
(415, 'Bathinda', 18, NULL, NULL),
(416, 'Firozpur', 18, NULL, NULL),
(417, 'Faridkot', 18, NULL, NULL),
(418, 'Fatehgarh Sahib', 18, NULL, NULL),
(419, 'Gurdaspur', 18, NULL, NULL),
(420, 'Hoshiarpur', 18, NULL, NULL),
(421, 'Jalandhar', 18, NULL, NULL),
(422, 'Kapurthala', 18, NULL, NULL),
(423, 'Ludhiana', 18, NULL, NULL),
(424, 'Mansa', 18, NULL, NULL),
(425, 'Moga', 18, NULL, NULL),
(426, 'Mukatsar', 18, NULL, NULL),
(427, 'Nawan Shehar', 18, NULL, NULL),
(428, 'Patiala', 18, NULL, NULL),
(429, 'Rupnagar', 18, NULL, NULL),
(430, 'Sangrur', 18, NULL, NULL),
(431, 'Ajmer', 19, NULL, NULL),
(432, 'Alwar', 19, NULL, NULL),
(433, 'Bikaner', 19, NULL, NULL),
(434, 'Barmer', 19, NULL, NULL),
(435, 'Banswara', 19, NULL, NULL),
(436, 'Bharatpur', 19, NULL, NULL),
(437, 'Baran', 19, NULL, NULL),
(438, 'Bundi', 19, NULL, NULL),
(439, 'Bhilwara', 19, NULL, NULL),
(440, 'Churu', 19, NULL, NULL),
(441, 'Chittorgarh', 19, NULL, NULL),
(442, 'Dausa', 19, NULL, NULL),
(443, 'Dholpur', 19, NULL, NULL),
(444, 'Dungapur', 19, NULL, NULL),
(445, 'Ganganagar', 19, NULL, NULL),
(446, 'Hanumangarh', 19, NULL, NULL),
(447, 'Juhnjhunun', 19, NULL, NULL),
(448, 'Jalore', 19, NULL, NULL),
(449, 'Jodhpur', 19, NULL, NULL),
(450, 'Jaipur', 19, NULL, NULL),
(451, 'Jaisalmer', 19, NULL, NULL),
(452, 'Jhalawar', 19, NULL, NULL),
(453, 'Karauli', 19, NULL, NULL),
(454, 'Kota', 19, NULL, NULL),
(455, 'Nagaur', 19, NULL, NULL),
(456, 'Pali', 19, NULL, NULL),
(457, 'Pratapgarh', 19, NULL, NULL),
(458, 'Rajsamand', 19, NULL, NULL),
(459, 'Sikar', 19, NULL, NULL),
(460, 'Sawai Madhopur', 19, NULL, NULL),
(461, 'Sirohi', 19, NULL, NULL),
(462, 'Tonk', 19, NULL, NULL),
(463, 'Udaipur', 19, NULL, NULL),
(464, 'East Sikkim', 20, NULL, NULL),
(465, 'North Sikkim', 20, NULL, NULL),
(466, 'South Sikkim', 20, NULL, NULL),
(467, 'West Sikkim', 20, NULL, NULL),
(468, 'Ariyalur', 21, NULL, NULL),
(469, 'Chennai', 21, NULL, NULL),
(470, 'Coimbatore', 21, NULL, NULL),
(471, 'Cuddalore', 21, NULL, NULL),
(472, 'Dharmapuri', 21, NULL, NULL),
(473, 'Dindigul', 21, NULL, NULL),
(474, 'Erode', 21, NULL, NULL),
(475, 'Kanchipuram', 21, NULL, NULL),
(476, 'Kanyakumari', 21, NULL, NULL),
(477, 'Karur', 21, NULL, NULL),
(478, 'Madurai', 21, NULL, NULL),
(479, 'Nagapattinam', 21, NULL, NULL),
(480, 'The Nilgiris', 21, NULL, NULL),
(481, 'Namakkal', 21, NULL, NULL),
(482, 'Perambalur', 21, NULL, NULL),
(483, 'Pudukkottai', 21, NULL, NULL),
(484, 'Ramanathapuram', 21, NULL, NULL),
(485, 'Salem', 21, NULL, NULL),
(486, 'Sivagangai', 21, NULL, NULL),
(487, 'Tiruppur', 21, NULL, NULL),
(488, 'Tiruchirappalli', 21, NULL, NULL),
(489, 'Theni', 21, NULL, NULL),
(490, 'Tirunelveli', 21, NULL, NULL),
(491, 'Thanjavur', 21, NULL, NULL),
(492, 'Thoothukudi', 21, NULL, NULL),
(493, 'Thiruvallur', 21, NULL, NULL),
(494, 'Thiruvarur', 21, NULL, NULL),
(495, 'Tiruvannamalai', 21, NULL, NULL),
(496, 'Vellore', 21, NULL, NULL),
(497, 'Villupuram', 21, NULL, NULL),
(498, 'Dhalai', 22, NULL, NULL),
(499, 'North Tripura', 22, NULL, NULL),
(500, 'South Tripura', 22, NULL, NULL),
(501, 'West Tripura', 22, NULL, NULL),
(502, 'Almora', 33, NULL, NULL),
(503, 'Bageshwar', 33, NULL, NULL),
(504, 'Chamoli', 33, NULL, NULL),
(505, 'Champawat', 33, NULL, NULL),
(506, 'Dehradun', 33, NULL, NULL),
(507, 'Haridwar', 33, NULL, NULL),
(508, 'Nainital', 33, NULL, NULL),
(509, 'Pauri Garhwal', 33, NULL, NULL),
(510, 'Pithoragharh', 33, NULL, NULL),
(511, 'Rudraprayag', 33, NULL, NULL),
(512, 'Tehri Garhwal', 33, NULL, NULL),
(513, 'Udham Singh Nagar', 33, NULL, NULL),
(514, 'Uttarkashi', 33, NULL, NULL),
(515, 'Agra', 23, NULL, NULL),
(516, 'Allahabad', 23, NULL, NULL),
(517, 'Aligarh', 23, NULL, NULL),
(518, 'Ambedkar Nagar', 23, NULL, NULL),
(519, 'Auraiya', 23, NULL, NULL),
(520, 'Azamgarh', 23, NULL, NULL),
(521, 'Barabanki', 23, NULL, NULL),
(522, 'Badaun', 23, NULL, NULL),
(523, 'Bagpat', 23, NULL, NULL),
(524, 'Bahraich', 23, NULL, NULL),
(525, 'Bijnor', 23, NULL, NULL),
(526, 'Ballia', 23, NULL, NULL),
(527, 'Banda', 23, NULL, NULL),
(528, 'Balrampur', 23, NULL, NULL),
(529, 'Bareilly', 23, NULL, NULL),
(530, 'Basti', 23, NULL, NULL),
(531, 'Bulandshahr', 23, NULL, NULL),
(532, 'Chandauli', 23, NULL, NULL),
(533, 'Chitrakoot', 23, NULL, NULL),
(534, 'Deoria', 23, NULL, NULL),
(535, 'Etah', 23, NULL, NULL),
(536, 'Kanshiram Nagar', 23, NULL, NULL),
(537, 'Etawah', 23, NULL, NULL),
(538, 'Firozabad', 23, NULL, NULL),
(539, 'Farrukhabad', 23, NULL, NULL),
(540, 'Fatehpur', 23, NULL, NULL),
(541, 'Faizabad', 23, NULL, NULL),
(542, 'Gautam Buddha Nagar', 23, NULL, NULL),
(543, 'Gonda', 23, NULL, NULL),
(544, 'Ghazipur', 23, NULL, NULL),
(545, 'Gorkakhpur', 23, NULL, NULL),
(546, 'Ghaziabad', 23, NULL, NULL),
(547, 'Hamirpur', 23, NULL, NULL),
(548, 'Hardoi', 23, NULL, NULL),
(549, 'Mahamaya Nagar', 23, NULL, NULL),
(550, 'Jhansi', 23, NULL, NULL),
(551, 'Jalaun', 23, NULL, NULL),
(552, 'Jyotiba Phule Nagar', 23, NULL, NULL),
(553, 'Jaunpur District', 23, NULL, NULL),
(554, 'Kanpur Dehat', 23, NULL, NULL),
(555, 'Kannauj', 23, NULL, NULL),
(556, 'Kanpur Nagar', 23, NULL, NULL),
(557, 'Kaushambi', 23, NULL, NULL),
(558, 'Kushinagar', 23, NULL, NULL),
(559, 'Lalitpur', 23, NULL, NULL),
(560, 'Lakhimpur Kheri', 23, NULL, NULL),
(561, 'Lucknow', 23, NULL, NULL),
(562, 'Mau', 23, NULL, NULL),
(563, 'Meerut', 23, NULL, NULL),
(564, 'Maharajganj', 23, NULL, NULL),
(565, 'Mahoba', 23, NULL, NULL),
(566, 'Mirzapur', 23, NULL, NULL),
(567, 'Moradabad', 23, NULL, NULL),
(568, 'Mainpuri', 23, NULL, NULL),
(569, 'Mathura', 23, NULL, NULL),
(570, 'Muzaffarnagar', 23, NULL, NULL),
(571, 'Pilibhit', 23, NULL, NULL),
(572, 'Pratapgarh', 23, NULL, NULL),
(573, 'Rampur', 23, NULL, NULL),
(574, 'Rae Bareli', 23, NULL, NULL),
(575, 'Saharanpur', 23, NULL, NULL),
(576, 'Sitapur', 23, NULL, NULL),
(577, 'Shahjahanpur', 23, NULL, NULL),
(578, 'Sant Kabir Nagar', 23, NULL, NULL),
(579, 'Siddharthnagar', 23, NULL, NULL),
(580, 'Sonbhadra', 23, NULL, NULL),
(581, 'Sant Ravidas Nagar', 23, NULL, NULL),
(582, 'Sultanpur', 23, NULL, NULL),
(583, 'Shravasti', 23, NULL, NULL),
(584, 'Unnao', 23, NULL, NULL),
(585, 'Varanasi', 23, NULL, NULL),
(586, 'Birbhum', 24, NULL, NULL),
(587, 'Bankura', 24, NULL, NULL),
(588, 'Bardhaman', 24, NULL, NULL),
(589, 'Darjeeling', 24, NULL, NULL),
(590, 'Dakshin Dinajpur', 24, NULL, NULL),
(591, 'Hooghly', 24, NULL, NULL),
(592, 'Howrah', 24, NULL, NULL),
(593, 'Jalpaiguri', 24, NULL, NULL),
(594, 'Cooch Behar', 24, NULL, NULL),
(595, 'Kolkata', 24, NULL, NULL),
(596, 'Malda', 24, NULL, NULL),
(597, 'Midnapore', 24, NULL, NULL),
(598, 'Murshidabad', 24, NULL, NULL),
(599, 'Nadia', 24, NULL, NULL),
(600, 'North 24 Parganas', 24, NULL, NULL),
(601, 'South 24 Parganas', 24, NULL, NULL),
(602, 'Purulia', 24, NULL, NULL),
(603, 'Uttar Dinajpur', 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `device_tokens`
--

CREATE TABLE `device_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `device_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_tokens`
--

INSERT INTO `device_tokens` (`id`, `user_id`, `device_type`, `device_token`, `created_at`, `updated_at`) VALUES
(1, '82', 'iphone', 'xxxxxx123', '2020-12-24 08:29:13', '2020-12-24 08:29:13'),
(2, '2', 'android', '123', '2020-12-26 03:05:42', '2020-12-26 03:05:42'),
(3, '3', 'android', '073cc91d76cbc30e', '2020-12-26 03:15:42', '2020-12-26 03:15:42'),
(4, '4', 'android', '123456', '2020-12-26 04:34:26', '2020-12-26 04:34:26'),
(5, '12', 'android', '12345', '2020-12-26 04:46:09', '2020-12-26 04:46:09');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_11_10_055028_create_admins_table', 1),
(9, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(10, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(11, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(12, '2016_06_01_000004_create_oauth_clients_table', 2),
(13, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(14, '2020_11_27_035317_create_categories_table', 3),
(15, '2020_11_27_035350_create_subcategories_table', 3),
(16, '2020_11_27_035414_create_products_table', 3),
(17, '2020_11_27_035500_create_device_tokens_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01f31c28a0a582b9f9dde2ad7b4b0bb618240b6a84e415d94700c1fe041f364659ac4cebbc70dfce', 1, 1, NULL, '[]', 0, '2020-12-17 04:54:52', '2020-12-17 04:54:52', '2021-12-17 06:54:52'),
('0b298479e59c0b41a231f5fe1836c13ee436f8d40e8dda687bd01c4984ba3b7cd9c60c99bc4c5218', 23, 1, NULL, '[]', 0, '2020-12-22 08:36:28', '2020-12-22 08:36:28', '2021-12-22 10:36:28'),
('115f51cd830436edb00a2ed8586f48456bd6c65aa0c4b65d68165a21b51dbffa3167474e9d15be9d', 22, 1, NULL, '[]', 0, '2020-12-22 08:33:32', '2020-12-22 08:33:32', '2021-12-22 10:33:32'),
('1a1029bc6420df9b91ee2a84c9d2da88d63eface22117b6110fb321c5881ee371793fc9b358285ac', 18, 1, NULL, '[]', 0, '2020-12-22 06:44:07', '2020-12-22 06:44:07', '2021-12-22 08:44:07'),
('1e0c928e856477e077d43026a1371fb8120968f706f0c6c0afdbc5c87862ad94430cd0a1a30ced3e', 21, 1, NULL, '[]', 0, '2020-12-22 07:57:51', '2020-12-22 07:57:51', '2021-12-22 09:57:51'),
('1ee68797978e9c739dc8159a4fa32a2855d3c12537f3a17b9279106f5e9a980dad9862aa988e8447', 1, 1, NULL, '[]', 0, '2020-12-17 04:55:41', '2020-12-17 04:55:41', '2021-12-17 06:55:41'),
('237be1e96e0548a22f486eeabbe0fd5eac5616df1a0f66f4198f2073537cfb62ec6261256ae01a44', 27, 1, 'MyApp', '[]', 0, '2021-01-04 07:40:29', '2021-01-04 07:40:29', '2022-01-04 09:40:29'),
('2485bf1e2d5ed7d871687e5ca5d5077d1dbf49f096c3f105d85d2ce5e7e943d8038a0f69629ab561', 11, 1, NULL, '[]', 0, '2020-12-21 10:14:42', '2020-12-21 10:14:42', '2021-12-21 12:14:42'),
('254879b202fe9048eb816ef53bc4171034b13726a177533ca2eadb5a556a80ea968ed32731d5ddea', 1, 1, NULL, '[]', 0, '2020-12-17 04:40:53', '2020-12-17 04:40:53', '2021-12-17 06:40:53'),
('2a4dc0f5d2b48df50c8b3ae86b4d1cd41e4e0f93f483c8e2932058cb8692e1b052924de46ba204b8', 36, 1, NULL, '[]', 0, '2020-12-23 06:38:39', '2020-12-23 06:38:39', '2021-12-23 08:38:39'),
('2a7158b3a495572617ea5d4cd848f125d114aa0baa23163623dde85be32fd7a9db203d7450561d3d', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-26 04:59:00', '2020-12-26 04:59:00', '2021-12-26 06:59:00'),
('2b09c413a1cb2bcdab8b7c82f59803ecb06f63c701e82eb3c3b9021530a7d2b00cb6eb2b5c5fb892', 14, 1, NULL, '[]', 0, '2020-12-22 02:38:37', '2020-12-22 02:38:37', '2021-12-22 04:38:37'),
('30221964545407c51b97ecd4de0b4c6fee7431ac10b459b684add311c4182347f3ce4928e5c4da2a', 1, 1, NULL, '[]', 0, '2020-12-17 05:08:08', '2020-12-17 05:08:08', '2021-12-17 07:08:08'),
('30f562f8cec208be247d9e8b4b9e1e87848b90eade1d93b3f05e523d56cd9e795defe64ec54494d3', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-26 13:55:20', '2020-12-26 13:55:20', '2021-12-26 15:55:20'),
('32abecbb1bf9204c6ae59a3a3e1e556ec5f3e6f3d36d587d814292220b1baff2cfa5a96cf0d8b46e', 2, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-26 04:54:32', '2020-12-26 04:54:32', '2021-12-26 06:54:32'),
('393176529e36cd1b5fb577af88e9c846c23dd82839d3c03f9fcdfeb58b77d045bd4cc961a85004b7', 1, 1, NULL, '[]', 0, '2020-12-22 02:34:58', '2020-12-22 02:34:58', '2021-12-22 04:34:58'),
('3c215e892085f5840eb30dbf9d599fe8b3f80f603ed047c2ccd00605cafd97a4fb9f0003dd75b491', 18, 1, NULL, '[]', 0, '2020-12-22 06:56:27', '2020-12-22 06:56:27', '2021-12-22 08:56:27'),
('3cd31fbd9da5d493a6e9034e2d33b0f4609576ae9c73dd923f5b5bf76dbb784cf6128ae5d0a2003e', 1, 1, NULL, '[]', 0, '2020-12-17 04:41:42', '2020-12-17 04:41:42', '2021-12-17 06:41:42'),
('4799ed2279b48a026ced19c959afdedd28b21009aebe1acb6d969a7182a0b55d4e662ef01698b668', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-28 05:35:13', '2020-12-28 05:35:13', '2021-12-28 07:35:13'),
('4b376dd26d79eccbc0b1046de7d92bdbecdd9ce233ab5a28bcb98f78521cb185098856cebfc35089', 1, 1, NULL, '[]', 0, '2020-12-17 04:36:11', '2020-12-17 04:36:11', '2021-12-17 06:36:11'),
('53af9a95afe9bf85d8def2db93c2fa13b1bdb43e2135d4fb63641c56249074d0a9e864d5d3ad88bd', 2, 1, 'MyApp', '[]', 0, '2020-12-26 03:05:41', '2020-12-26 03:05:41', '2021-12-26 05:05:41'),
('57f8c53d7c5b0080acc42a013647d60accf2dbeb30361e7add4ccc1977fe35ed7f4b377dc377d369', 18, 1, NULL, '[]', 0, '2020-12-22 07:20:34', '2020-12-22 07:20:34', '2021-12-22 09:20:34'),
('64ea7c1e4166f60ba50c393ab99fe523ace8efd3cbe27b783478c134951a2867f94acdf066d75c2e', 18, 1, NULL, '[]', 0, '2020-12-22 07:14:52', '2020-12-22 07:14:52', '2021-12-22 09:14:52'),
('65b75476835ce23fdcdba55bc71ee7fe23cba3eb30b53d9eb2b0b514e1ce2ca471788ad147e86ec3', 11, 1, NULL, '[]', 0, '2020-12-21 10:06:10', '2020-12-21 10:06:10', '2021-12-21 12:06:10'),
('671c7256e1ead84dd17d770553bbe0e23f90516db38de55b1e90c896095667dc2b5bb1031398cab5', 21, 1, NULL, '[]', 0, '2020-12-22 08:32:39', '2020-12-22 08:32:39', '2021-12-22 10:32:39'),
('6a778e41cf5ce314952fefeab1c39313572dfb981443e6823b0a0da78dcc4d6086c9515bf164d522', 1, 1, NULL, '[]', 0, '2020-12-17 04:59:13', '2020-12-17 04:59:13', '2021-12-17 06:59:13'),
('6f08266b58a2f0a2083b2385e76c4abe43c0197bc9c172afa8fe5ab33da59dca24fb2e314acebb03', 97, 1, 'MyApp', '[]', 0, '2020-12-25 03:21:44', '2020-12-25 03:21:44', '2021-12-25 05:21:44'),
('735cfda3b0817a4e02ab82b82d745cd32244e9648783ef32ab816475880c25e2108bba85d0dec50e', 12, 1, 'MyApp', '[]', 0, '2020-12-26 04:46:08', '2020-12-26 04:46:08', '2021-12-26 06:46:08'),
('853e15300baf52d43665a3c496ac7153dc7731c2310f81943f875ceebcb4a6eb7c689833cecc4fc6', 7, 1, 'MyApp', '[]', 0, '2020-12-26 04:40:38', '2020-12-26 04:40:38', '2021-12-26 06:40:38'),
('8f50d09ecf1d4ae2fe408857548679ea956a81f962c17bb2771dec68b1ce2424b8185b391d53f957', 18, 1, NULL, '[]', 0, '2020-12-22 07:06:47', '2020-12-22 07:06:47', '2021-12-22 09:06:47'),
('976a14da12aa59decbfb97e9bdd1893f2230b38927743fcf0008cafafa6471de3fa275279eaa30a5', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-25 03:33:01', '2020-12-25 03:33:01', '2021-12-25 05:33:01'),
('9ec0b72b9c68cdbc1d1ccb99fb9a9846c26b349599ca27cdd3c41f9e025b4bb14215b8913d9cabe1', 1, 1, NULL, '[]', 0, '2020-12-18 01:54:00', '2020-12-18 01:54:00', '2021-12-18 03:54:00'),
('9f0ab3deee91afaad3c431b400639743a8bdfafe251f474f6726f6552580cb79a547d56928cbd150', 1, 1, NULL, '[]', 0, '2020-12-18 02:00:24', '2020-12-18 02:00:24', '2021-12-18 04:00:24'),
('ab8e3a7d2f7b6439a5a1bdb6e7ef6d0eca01b4ac4c0892974c011e40e016ba9e5536ea098926c6bc', 38, 1, NULL, '[]', 0, '2020-12-23 06:49:08', '2020-12-23 06:49:08', '2021-12-23 08:49:08'),
('ad4d7b917d040193cf31f7c5e615bf4ab5db8a12752b7c1957625eafad5c1c84b95a1dd61082e5c2', 97, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-25 03:23:29', '2020-12-25 03:23:29', '2021-12-25 05:23:29'),
('b80756433e182dd8f0a94f63e4d618dd7bcafb65838c023df8edb4bb1f405ec2531c53071a35497f', 1, 1, NULL, '[]', 0, '2020-12-17 04:52:27', '2020-12-17 04:52:27', '2021-12-17 06:52:27'),
('b8d3539f0c1f2bdf2864d135e0065c14822164171488827274e026402f14e46468a684438feda715', 33, 1, NULL, '[]', 0, '2020-12-22 08:58:17', '2020-12-22 08:58:17', '2021-12-22 10:58:17'),
('bb37e21e4b86015e0a5d1b9e8a41ad4446e1ff05ced0ecd0647d1ceb72456b89f00c68f8db29e683', 4, 1, 'MyApp', '[]', 0, '2020-12-26 04:34:26', '2020-12-26 04:34:26', '2021-12-26 06:34:26'),
('bccac6ab5b8b1ce039e74a2927bf549f2af6477f9a7a38854b94fe82518dd7d0ba2e924c3ec2dadd', 18, 1, NULL, '[]', 0, '2020-12-22 07:04:18', '2020-12-22 07:04:18', '2021-12-22 09:04:18'),
('bd5b56a09dfd64d6a1302fe1c50b91ea2e23aebf6421738079c6503f7e025e8f53f6e91deab62021', 16, 1, 'MyApp', '[]', 0, '2020-12-26 04:53:14', '2020-12-26 04:53:14', '2021-12-26 06:53:14'),
('be3ea465c4ee4ed4d50781f367ad753c9e08edbb1459926b2f9daae0fed8be373525b63db6083eeb', 14, 1, 'MyApp', '[]', 0, '2020-12-26 04:51:31', '2020-12-26 04:51:31', '2021-12-26 06:51:31'),
('be5dd49c5262939912827633df4d5c5b38b5d15602ffe72c4065d1c3e16a0c6724085d21bd5b2ad6', 1, 1, NULL, '[]', 0, '2020-12-17 04:57:07', '2020-12-17 04:57:07', '2021-12-17 06:57:07'),
('bedd12e815925c6540b8d39fb4460c8c84a93e1dacbd42f8d286f43c4dd3cc0c059977597f8f1269', 1, 1, NULL, '[]', 0, '2020-12-18 03:07:44', '2020-12-18 03:07:44', '2021-12-18 05:07:44'),
('c27d0054ad9083af862395ef28f7431d262ab6dbcee2e3ebdc005ed1c1b3d4cf44a92d293300bd03', 20, 1, NULL, '[]', 0, '2020-12-22 07:25:44', '2020-12-22 07:25:44', '2021-12-22 09:25:44'),
('c6b4fed6e6fb8f2dbca9a54d8b407f4f002336fcb34c6f166bba0e25f8380ae84e1dbc4da88be2a7', 27, 1, 'Laravel Password Grant Client', '[]', 0, '2021-01-04 07:42:31', '2021-01-04 07:42:31', '2022-01-04 09:42:31'),
('ca3088b48506cf7c058dce806bc0a7856b51281eb21c7d34df6300e32de9a2a8caebd943ede5a07a', 1, 1, 'MyApp', '[]', 0, '2020-12-25 03:32:20', '2020-12-25 03:32:20', '2021-12-25 05:32:20'),
('cb2f9b4996d6e7d4c4c27efcb4174a59b39bbfc1afe07c2b9232d05ad2b0f3d12947e345ba38f65d', 13, 1, 'MyApp', '[]', 0, '2020-12-26 04:47:52', '2020-12-26 04:47:52', '2021-12-26 06:47:52'),
('cb91223037406ec5c88113e09e5735c5459ec4140ed56a1da79c017552b28da8a12030f46a3b4ce1', 32, 1, NULL, '[]', 0, '2020-12-22 08:50:18', '2020-12-22 08:50:18', '2021-12-22 10:50:18'),
('ccc1c2301f42c151b5b972fdb7abe934cf3b7d00131b79c8f7e4541865e6dfcd7e861d4f691f0fdd', 1, 1, NULL, '[]', 0, '2020-12-17 05:05:30', '2020-12-17 05:05:30', '2021-12-17 07:05:30'),
('cd4a3f3c2553a765b875c59d2c738db0eced577fc0e1c8656dcec1955d92daafd7f2513aa6e7e392', 1, 1, NULL, '[]', 0, '2020-12-18 01:55:08', '2020-12-18 01:55:08', '2021-12-18 03:55:08'),
('d0b87f75cebc52550ca0e053fc832dfe84f581ae4ddcfc2fd05c8143e71fe526b230fc40e85d6a44', 1, 1, NULL, '[]', 0, '2020-12-17 04:56:10', '2020-12-17 04:56:10', '2021-12-17 06:56:10'),
('d26cda1b61118768516d6f609fad4f3762b2b8e31a7e1bd1c2b80f9d3581b1fd5ae51801639b0e27', 18, 1, NULL, '[]', 0, '2020-12-22 07:13:48', '2020-12-22 07:13:48', '2021-12-22 09:13:48'),
('d8637e7412146872b652d1006b4ee5056ed4b386ea3dd8e2e0efdee518428616094b98b8a6a1301e', 16, 1, 'Laravel Password Grant Client', '[]', 0, '2020-12-26 13:57:32', '2020-12-26 13:57:32', '2021-12-26 15:57:32'),
('dc2633186ba6ab5bcf487981dc188d4be2c91dcecd2af655d7ece57f6e81053808cc13772dc2f3d3', 5, 1, 'MyApp', '[]', 0, '2020-12-26 04:35:11', '2020-12-26 04:35:11', '2021-12-26 06:35:11'),
('e054ba730877ab22a278100ab27c0844b46f27c7436bd09beb30e5cfa9d710fb529b988091baafb9', 16, 1, NULL, '[]', 0, '2020-12-22 07:56:52', '2020-12-22 07:56:52', '2021-12-22 09:56:52'),
('ec421fc174628bce94683a7fb6ad838e1fdd51b82e777de9ba66b8ca13aa7d1814cf7daa1705a02c', 1, 1, NULL, '[]', 0, '2020-12-18 02:04:04', '2020-12-18 02:04:04', '2021-12-18 04:04:04'),
('efe4b5b3f85bad9d842de75051c6b8d0f0166d749c8cfe77453cdbe3be1d1af26fbea6ea4501ef6b', 11, 1, NULL, '[]', 0, '2020-12-21 10:14:20', '2020-12-21 10:14:20', '2021-12-21 12:14:20'),
('f19166c87bb4d027a8c3575d9e9deb4bcbb3460a801945df0b9e6236aee46acc0bc7ed73b37db338', 6, 1, 'MyApp', '[]', 0, '2020-12-26 04:37:01', '2020-12-26 04:37:01', '2021-12-26 06:37:01'),
('f20fd1d357e6834be97537bbe0640ea1b0b99029c9bea2d68705296c9d7e944cbce6005b6e693b94', 3, 1, 'MyApp', '[]', 0, '2020-12-26 03:15:42', '2020-12-26 03:15:42', '2021-12-26 05:15:42'),
('f69db11a3230ec9bf274a0c27e0c9b6017f95ebe8084fe8ceb35aa29f2c8a60da327dcecc24d4c95', 18, 1, NULL, '[]', 0, '2020-12-22 07:05:45', '2020-12-22 07:05:45', '2021-12-22 09:05:45'),
('f8e52160bde2c41a3f626ac355860d1aabc63f59a6f71c98f52b4ba4b391208789402a8fcd44f9e8', 23, 1, 'MyApp', '[]', 0, '2020-12-26 07:36:17', '2020-12-26 07:36:17', '2021-12-26 09:36:17'),
('f90e1e1cb62a9f4d662f66d67e73aa5e3995cbd6d10bc45c7c6c779cf4d25d0bc96c988b49e04e6c', 18, 1, NULL, '[]', 0, '2020-12-22 07:16:27', '2020-12-22 07:16:27', '2021-12-22 09:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'lt3bA466SlP8qVVIy9rMdHS57ccfjb91ZHhFb1fD', NULL, 'http://localhost', 1, 0, 0, '2020-11-25 06:00:29', '2020-11-25 06:00:29'),
(2, NULL, 'Laravel Password Grant Client', 'j2JE4qDD7lLLES9yUwuB3epmt1nwIN9TsiXJJixT', 'users', 'http://localhost', 0, 1, 0, '2020-11-25 06:00:29', '2020-11-25 06:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-25 06:00:29', '2020-11-25 06:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `order_date` date NOT NULL,
  `exp_del_date` date NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('complete','failed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'failed',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bankname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_txn_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_id` int(11) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `timeslot_id` int(11) NOT NULL DEFAULT '0',
  `order_status` enum('pending','received','shipped','delivered','cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_no`, `order_date`, `exp_del_date`, `price`, `transaction_id`, `status`, `type`, `bankname`, `payment_mode`, `bank_txn_id`, `currency`, `gateway_name`, `gift_id`, `address_id`, `timeslot_id`, `order_status`, `created_at`, `updated_at`) VALUES
(1, 18, 'ORDER000123', '2020-12-26', '1970-01-01', 100.00, '', 'complete', 'product', NULL, 'cod', NULL, NULL, NULL, 0, 1, 1, 'pending', '2020-12-26 04:34:27', '2020-12-26 04:34:27'),
(2, 18, 'ORDER000123', '2020-12-26', '1970-01-01', 100.00, '', 'complete', 'product', '', 'cod', '', 'INR', '', 0, 1, 1, 'pending', '2020-12-26 04:46:34', '2020-12-26 04:46:34'),
(3, 18, 'ORDER000123', '2020-12-26', '2020-12-26', 100.00, '', 'complete', 'product', '', 'cod', '', 'INR', '', 0, 1, 1, 'pending', '2020-12-26 18:38:44', '2020-12-26 18:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_cancels`
--

CREATE TABLE `order_cancels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_deliveries`
--

CREATE TABLE `order_deliveries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `additional_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_proof` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `order_date` date NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `total_price` double(8,2) NOT NULL DEFAULT '0.00',
  `payment_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('complete','failed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'failed',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'uploads/images/default.jpg',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'uploads/thumbnail/default.jpg',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gift_id` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `user_id`, `order_id`, `order_no`, `order_date`, `product_id`, `product_name`, `qty`, `price`, `total_price`, `payment_mode`, `status`, `image`, `thumbnail`, `type`, `gift_id`, `description`, `tax`, `created_at`, `updated_at`) VALUES
(1, 18, 1, '1', '2020-12-26', 1, 'User', 1, 2500.00, 10500.00, 'paytm', 'complete', 'uploads/images/1608575350.jpeg', 'uploads/thumbnail/1608575350.jpeg', 'product', 0, '', 100, '2020-12-26 03:29:31', '2020-12-26 03:29:31'),
(2, 18, 1, '1', '2020-12-26', 2, 'New Dress', 1, 2500.00, 10500.00, 'paytm', 'complete', '1608577727cover_1597243897.jpg|1608577727maxresdefault.jpg|1608577728mens-indo-western-dress-500x500.jpg', 'uploads/thumbnail/1608577728.jpeg', 'product', 0, '', 100, '2020-12-26 03:29:32', '2020-12-26 03:29:32'),
(3, 18, 0, '1', '2020-12-26', 1, 'User', 1, 2500.00, 10500.00, 'paytm', 'complete', 'uploads/images/1608748893maxresdefault.jpg', 'uploads/thumbnail/1608748893.jpeg', 'product', 0, '<p>hh</p>', 100, '2020-12-26 18:36:41', '2020-12-26 18:36:41'),
(4, 18, 0, '1', '2020-12-26', 2, 'New Dress', 1, 2500.00, 10500.00, 'paytm', 'complete', 'uploads/images/1608748853cover_1597243897.jpg|uploads/images/1608748853maxresdefault.jpg', 'uploads/thumbnail/1608748853.jpeg', 'product', 0, '<p>test</p>', 100, '2020-12-26 18:36:41', '2020-12-26 18:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `order_shippings`
--

CREATE TABLE `order_shippings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `tracking_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_proof` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `other_addresses`
--

CREATE TABLE `other_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `building_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `state_id` int(11) NOT NULL DEFAULT '0',
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `other_addresses`
--

INSERT INTO `other_addresses` (`id`, `user_id`, `pincode`, `building_name`, `area`, `city_id`, `state_id`, `landmark`, `name`, `phone_number`, `other_phone_number`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 18, '324004', '33', 'kota', 12, 33, 'near vesho mata mandir', 'trilok1', '9166900279', NULL, 1, 0, '2020-12-26 03:01:17', '2020-12-26 03:02:45'),
(2, 18, '324004', '33', 'kota', 12, 33, 'near vesho mata mandir', 'trilok1', '9166900279', NULL, 1, 0, '2020-12-26 18:32:31', '2020-12-26 18:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravinn@gmail.com', '$2y$10$hdRXb3aBFVgZUl.z30ulWOUCLSs3xtTuQ8VDviz0nIqXiYsy4wl8O', '2021-01-04 07:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `subcategory_id` int(11) NOT NULL DEFAULT '0',
  `innercategory_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `min_qty` int(11) NOT NULL DEFAULT '1',
  `max_qty` int(11) NOT NULL DEFAULT '0',
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `mrp` double(8,2) NOT NULL DEFAULT '0.00',
  `date_of_packing` date NOT NULL,
  `date_of_expire` date DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `discount` float NOT NULL DEFAULT '0',
  `unit` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `product_type` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `admin_id`, `category_id`, `subcategory_id`, `innercategory_id`, `code`, `name`, `description`, `qty`, `min_qty`, `max_qty`, `price`, `mrp`, `date_of_packing`, `date_of_expire`, `image`, `thumb`, `status`, `is_deleted`, `tax`, `discount`, `unit`, `weight`, `product_type`, `created_at`, `updated_at`) VALUES
(13, 0, 3, 1, 0, '0313', 'SAMSUNG A5016', 'Very Nice To Use', 1, 1, 5, 50000.00, 50000.00, '1970-01-01', '1970-01-01', '1607941515.png', '1607941515.png', 1, 1, 0, 0, '27', 1, 0, '2020-12-14 08:25:15', '2020-12-21 06:27:07'),
(14, 0, 2, 6, 0, '021', 'Hitachi Air Purifier', 'Very Good Quality Product', 1, 1, 5, 4500.00, 4500.00, '1970-01-01', '1970-01-01', '1608194597.png', '1608194597.png', 1, 1, 0, 0, '27', 2, 0, '2020-12-17 06:43:17', '2020-12-18 15:52:17'),
(15, 0, 2, 7, 0, '0353', 'Sony Bravia Smart TV ET5987', 'Very Good Quality Product', 1, 1, 5, 35000.00, 35000.00, '1970-01-01', '1970-01-01', '1608194712.png', '1608194712.png', 1, 1, 0, 0, '27', 3, 0, '2020-12-17 06:45:12', '2020-12-21 06:27:10'),
(16, 0, 2, 5, 0, '12213', 'Bajaj Table Fan 46564df', 'Very Good Quality Product', 1, 1, 10, 1800.00, 1800.00, '1970-01-01', '1970-01-01', '1608194794.png', '1608194794.png', 1, 1, 0, 0, '27', 1, 0, '2020-12-17 06:46:34', '2020-12-21 06:27:16'),
(17, 0, 3, 1, 0, 'ASD2321', 'Samsung Galaxy S6 Edge Plus', 'Most Recommended Phone In This Price Segment', 1, 1, 5, 20000.00, 20000.00, '1970-01-01', '1970-01-01', '1608195139.png', '1608195139.png', 1, 1, 0, 0, '27', 0, 0, '2020-12-17 06:52:19', '2020-12-21 06:27:03'),
(18, 0, 3, 1, 0, '35464', 'Samsung S9', 'Very Good Features Of This Phone', 1, 1, 5, 45000.00, 45000.00, '1970-01-01', '1970-01-01', '1608195267.png', '1608195267.png', 1, 1, 0, 0, '27', 500, 0, '2020-12-17 06:54:27', '2020-12-18 09:46:12'),
(19, 0, 7, 25, 0, '55656', 'Fan', 'Gsdgd', 1, 5, 10, 3200.00, 3200.00, '1970-01-01', '1970-01-01', '1608371334.png', '1608371334.png', 1, 1, 0, 0, '27', 1, 0, '2020-12-19 07:48:54', '2020-12-21 06:26:54'),
(20, 0, 6, 23, 0, 'A5654', 'Sa Re Ga Ma Carvaan A205', 'This Product Having Awesome Features', 1, 5, 10, 2100.00, 2100.00, '1970-01-01', '1970-01-01', 'images1608373809.jpeg', '1608373809.jpeg', 1, 1, 0, 0, '27', 1, 0, '2020-12-19 08:30:09', '2021-01-15 05:15:04'),
(21, 0, 7, 29, 0, 'Q74', 'Bajaj Fan A454', 'Having Best Quality Features', 1, 1, 5, 2500.00, 2500.00, '1970-01-01', '1970-01-01', 'images1608540360.png', '1608540360.png', 1, 0, 0, 0, '27', 1, 0, '2020-12-21 06:46:00', '2020-12-21 06:46:00'),
(22, 0, 7, 29, 0, '123', 'Orient Fan Supreme Model', 'Very Good Quality Product', 1, 5, 10, 2572.00, 2572.00, '1970-01-01', '1970-01-01', 'images1608540488.png', '1608540488.png', 1, 1, 0, 0, '27', 0, 0, '2020-12-21 06:48:08', '2021-01-11 12:27:31'),
(24, 0, 3, 3, 0, '123ASD', 'Nokia 7S', 'Good Quality', 1, 2, 5, 12500.00, 12500.00, '1970-01-01', '1970-01-01', 'images1610526403.png', '1610526403.png', 1, 1, 0, 0, '41', 1, 0, '2021-01-13 06:26:43', '2021-01-21 05:15:24'),
(25, 0, 3, 24, 0, '1254', 'Nokia 10', 'Good Quality', 1, 2, 5, 14000.00, 14000.00, '1970-01-01', '1970-01-01', 'images1610526536.png', '1610526536.png', 1, 0, 0, 0, '41', 1, 0, '2021-01-13 06:28:56', '2021-01-13 06:31:50'),
(26, 0, 2, 6, 0, '55', 'Gsdfgsdrsdfg', 'Dgfghh', 464, 66, 656, 5.00, 45.00, '1970-01-01', '1970-01-01', 'images1610527700.png', '1610527700.png', 1, 1, 0, 0, '42', 7, 0, '2021-01-13 06:48:20', '2021-01-13 06:48:26'),
(28, 0, 2, 6, 0, '1321', 'Air Purifire', 'Having Good Quality', 1, 2, 5, 12345.00, 12345.00, '1970-01-01', '1970-01-01', 'images1610695158.png', '1610695158.png', 1, 1, 0, 0, '41', 1, 0, '2021-01-15 05:19:20', '2021-01-15 05:19:42'),
(29, 0, 2, 6, 0, '1233', 'Air Purifier', 'Having Quality Features', 2, 1, 10, 12999.00, 12999.00, '1970-01-01', '1970-01-01', 'images1610695276.png', '1610695276.png', 1, 0, 0, 0, '41', 1, 0, '2021-01-15 05:21:16', '2021-01-15 05:21:16'),
(30, 0, 3, 3, 0, '121AS', 'Nokia 6', 'Good Quality Phone', 1, 1, 5, 13950.00, 13950.00, '1970-01-01', '1970-01-01', 'images1610695398.png', '1610695398.png', 1, 0, 0, 0, '41', 1, 0, '2021-01-15 05:23:18', '2021-01-15 05:23:18'),
(31, 0, 2, 7, 0, '1213GF', 'Sony Bravia Samrt TV', 'Having High Resolultion Feature', 1, 1, 2, 44599.00, 44599.00, '1970-01-01', '1970-01-01', 'images1610695522.png', '1610695522.png', 1, 0, 0, 0, '41', 1, 0, '2021-01-15 05:25:22', '2021-01-15 05:25:22'),
(32, 0, 13, 32, 0, '121QW', 'Havells AE201 Geysor', 'Great Quality Product', 1, 2, 5, 6000.00, 6000.00, '1970-01-01', '1970-01-01', 'images1611212490.png', '1611212490.png', 1, 1, 0, 0, '41', 1, 0, '2021-01-21 05:01:30', '2021-01-21 05:01:54'),
(33, 0, 13, 32, 0, '454AS', 'Orient Chrome Geysor', 'Good Quality Product', 1, 2, 5, 6500.00, 6500.00, '1970-01-01', '1970-01-01', 'images1611212612.png', '1611212612.png', 1, 0, 0, 0, '41', 3, 0, '2021-01-21 05:03:32', '2021-01-21 05:03:32'),
(34, 0, 11, 33, 0, '445Q', 'Russle Hobbs Cattley', 'Good Quality Product', 1, 2, 5, 2100.00, 2100.00, '1970-01-01', '1970-01-01', 'images1611213055.png', '1611213055.png', 1, 0, 0, 0, '41', 1, 0, '2021-01-21 05:10:56', '2021-01-21 05:10:56');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 3, 'uploads/images/1608748680cover_1597243897.jpg', 1, 0, '2020-12-24 01:38:02', '2020-12-24 01:38:02'),
(2, 3, 'uploads/images/1608748682maxresdefault.jpg', 1, 0, '2020-12-24 01:38:02', '2020-12-24 01:38:02'),
(3, 3, 'uploads/images/1608748682mens-indo-western-dress-500x500.jpg', 1, 0, '2020-12-24 01:38:02', '2020-12-24 01:38:02'),
(4, 2, 'uploads/images/1608748853cover_1597243897.jpg', 1, 0, '2020-12-24 01:40:53', '2020-12-24 01:40:53'),
(5, 2, 'uploads/images/1608748853maxresdefault.jpg', 1, 0, '2020-12-24 01:40:53', '2020-12-24 01:40:53'),
(6, 1, 'uploads/images/1608748893maxresdefault.jpg', 1, 0, '2020-12-24 01:41:33', '2020-12-24 01:41:33');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('flat','percentage') COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `code`, `type`, `discount`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'New Year', 'flat', 10, 1, 0, '2020-12-26 22:03:44', '2020-12-27 11:10:18'),
(2, 'DEE', 'flat', 10, 1, 1, '2021-01-02 04:32:47', '2021-01-02 09:42:54'),
(4, 'FEES', 'flat', 10, 1, 1, '2021-01-02 09:46:07', '2021-01-02 09:54:35'),
(5, 'ENJOY', 'flat', 15, 1, 0, '2021-01-02 09:47:27', '2021-01-02 09:47:27'),
(6, 'GIFT', 'flat', 13, 1, 0, '2021-01-02 09:54:17', '2021-01-02 09:54:17'),
(7, 'AERO', 'flat', 23, 1, 0, '2021-01-12 06:16:05', '2021-01-12 06:16:05'),
(8, 'SALEIT', 'flat', 13, 1, 1, '2021-01-12 06:29:37', '2021-01-12 06:30:08'),
(9, 'BONANZA', 'percentage', 18, 1, 0, '2021-01-12 06:46:40', '2021-01-12 06:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'ANDHRA PRADESH', 105, NULL, NULL),
(2, 'ASSAM', 105, NULL, NULL),
(3, 'ARUNACHAL PRADESH', 105, NULL, NULL),
(4, 'BIHAR', 105, NULL, NULL),
(5, 'GUJRAT', 105, NULL, NULL),
(6, 'HARYANA', 105, NULL, NULL),
(7, 'HIMACHAL PRADESH', 105, NULL, NULL),
(8, 'JAMMU & KASHMIR', 105, NULL, NULL),
(9, 'KARNATAKA', 105, NULL, NULL),
(10, 'KERALA', 105, NULL, NULL),
(11, 'MADHYA PRADESH', 105, NULL, NULL),
(12, 'MAHARASHTRA', 105, NULL, NULL),
(13, 'MANIPUR', 105, NULL, NULL),
(14, 'MEGHALAYA', 105, NULL, NULL),
(15, 'MIZORAM', 105, NULL, NULL),
(16, 'NAGALAND', 105, NULL, NULL),
(17, 'ORISSA', 105, NULL, NULL),
(18, 'PUNJAB', 105, NULL, NULL),
(19, 'RAJASTHAN', 105, NULL, NULL),
(20, 'SIKKIM', 105, NULL, NULL),
(21, 'TAMIL NADU', 105, NULL, NULL),
(22, 'TRIPURA', 105, NULL, NULL),
(23, 'UTTAR PRADESH', 105, NULL, NULL),
(24, 'WEST BENGAL', 105, NULL, NULL),
(25, 'DELHI', 105, NULL, NULL),
(26, 'GOA', 105, NULL, NULL),
(27, 'PONDICHERY', 105, NULL, NULL),
(28, 'LAKSHDWEEP', 105, NULL, NULL),
(29, 'DAMAN & DIU', 105, NULL, NULL),
(30, 'DADRA & NAGAR', 105, NULL, NULL),
(31, 'CHANDIGARH', 105, NULL, NULL),
(32, 'ANDAMAN & NICOBAR', 105, NULL, NULL),
(33, 'UTTARANCHAL', 105, NULL, NULL),
(34, 'JHARKHAND', 105, NULL, NULL),
(35, 'CHATTISGARH', 105, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `name`, `image`, `thumbnail`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 8, 'Samsung phones', 'default.jpg', '1607919184.png', 1, 1, '2020-12-14 02:13:04', '2021-01-14 09:06:53'),
(3, 3, 'Nokia', '1607922667.png', '1607922667.png', 1, 1, '2020-12-14 03:11:07', '2020-12-17 09:29:29'),
(5, 2, 'Fan', '1608194399.png', '1608194399.png', 1, 1, '2020-12-17 06:39:59', '2020-12-21 06:28:33'),
(6, 2, 'Air purifier', '1608194430.png', '1608194430.png', 1, 1, '2020-12-17 06:40:30', '2020-12-21 06:28:46'),
(7, 2, 'Smart TV', '1608194457.png', '1608194457.png', 1, 1, '2020-12-17 06:40:57', '2020-12-21 06:28:41'),
(9, 2, 'Microwave', 'default.jpg', '1608199624.png', 1, 1, '2020-12-17 08:07:04', '2020-12-18 08:00:54'),
(20, 3, 'Xiaomi', '/tmp/phpCP60JF', 'image1608206272.png', 1, 1, '2020-12-17 09:57:52', '2020-12-18 06:18:11'),
(23, 6, 'carvann', 'image1608280813.jpeg', '/tmp/phpcHLV2N', 1, 1, '2020-12-18 06:40:13', '2020-12-18 06:54:03'),
(24, 3, 'Nokia', 'images1608284933.png', '1608284933.png', 1, 1, '2020-12-18 06:53:08', '2020-12-18 07:59:06'),
(25, 7, 'MIcrowave', 'images1608285683.png', '1608285683.png', 1, 1, '2020-12-18 08:01:23', '2020-12-18 09:57:19'),
(26, 3, 'Nokia', 'images1608285910.png', '1608285910.png', 1, 1, '2020-12-18 08:05:10', '2020-12-18 08:08:35'),
(28, 7, 'MIcrowave', 'images1608292670.png', '1608292670.png', 1, 0, '2020-12-18 09:57:50', '2020-12-18 09:57:50'),
(29, 7, 'Ceiling Fans And Table Fans', 'images1608540727.jpeg', '1608540727.jpeg', 1, 0, '2020-12-21 06:42:01', '2020-12-21 06:52:07'),
(30, 8, 'HTC Phones', 'default.jpg', '1608541616.png', 1, 1, '2020-12-21 07:06:57', '2021-01-14 09:06:33'),
(32, 13, 'Top Rated Geysors', 'images1611212326.png', '1611212326.png', 1, 0, '2021-01-21 04:58:46', '2021-01-21 04:58:46'),
(33, 11, 'Electric Cattle', 'images1611212694.png', '1611212694.png', 1, 0, '2021-01-21 05:04:54', '2021-01-21 05:04:54');

-- --------------------------------------------------------

--
-- Table structure for table `time_slots`
--

CREATE TABLE `time_slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_slots`
--

INSERT INTO `time_slots` (`id`, `name`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, '12 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(2, '1 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(3, '2 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(4, '3 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(5, '4 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(6, '5 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(7, '6 AM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(8, '7 AM TO 8AM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(9, '8 AM TO 9 AM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(10, '9 AM TO 10 AM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(11, '10 AM TO 11 AM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(12, '11 AM TO 12 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(13, '12 PM TO 2 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(14, '2 PM TO 4 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(15, '4 PM TO 6 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(16, '6 PM  TO 8 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(17, '4 PM', 1, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(18, '5 PM', 1, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(19, '6 PM', 1, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(20, '7 PM', 1, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(21, '8 PM', 1, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(22, '9 PM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(23, '10 PM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(24, '11 PM', 0, 1, '2020-12-26 02:58:22', '2020-12-26 02:58:22'),
(25, '8 PM TO 9 PM', 1, 0, '2020-12-26 02:58:22', '2020-12-26 02:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'BAG - BAGS', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(2, 'BAL - BALE', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(3, 'BDL - BUNDLES', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(4, 'BDL - BUNDLES', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(5, 'BOU - BILLIONS OF UNITS', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(6, 'BOX - BOX', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(7, 'BTL - BOTTLES', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(8, 'BUN - BUNCHES', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(9, 'CAN - CANS', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(10, 'CBM - CUBIC', '2019-03-03 04:05:33', '2019-03-03 04:05:33'),
(11, 'METER CCM - CUBIC', '2019-03-03 04:07:11', '2019-03-03 04:07:11'),
(12, 'CENTIMETER CMS - CENTIMETER', '2019-03-03 04:07:11', '2019-03-03 04:07:11'),
(37, 'TON - TONNES', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(38, 'THD - THOUSANDS', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(39, 'TUB - TUBES', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(40, 'UGS - US GALLONS', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(41, 'UNT - UNITS', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(42, 'YDS - YARDS', '2019-03-03 04:11:14', '2019-03-03 04:11:14'),
(43, 'OTH - OTHERS', '2019-03-03 04:11:14', '2019-03-03 04:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `phoneno`, `email`, `email_verified_at`, `password`, `address`, `profile_pic`, `device_type`, `device_token`, `deleted_at`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ravin', 'B', '9788844888', 'ravinns@gmail.com', '2021-01-04 09:56:15', '$2y$10$MyEef4EVCKOw8PMjlNacGeOc8t6F5KO.5kMPUJRXUQDhBRRwtmEvm', 'kota', 'images/1609221754.jpeg', NULL, NULL, 0, 1, 'esjtvidlRLZeO44Z3yIpsdzqd0yXR7T7R7PZ33z3UBf0hTgwIBELOFKr91D6LLNMcZl1tmEyEmYcPA0PRHZP0TBSbF3nXWp11tWN', '2020-12-25 03:32:20', '2021-01-04 07:56:15'),
(2, 'Navnit', 'Namdev', '7597477117', 'navnitnamdev4@gmail.com', '2020-12-26 06:54:32', '$2y$10$ZXTwYaGztqKfYE0SG7Vbx.lDATajZ9ivAxv6xX1MJ80zsaQkK4fU.', 'Jaipur', NULL, NULL, NULL, 0, 1, 'XOc8CTvrgRQrFf1fkGPtvdQ7ArAoPvHBn5oH72Gsy1njjtAydlgMMNe9ue3SCtblKNUG5BYPFF8bPtzRBm0SmeY1oY1iUsStgF2i', '2020-12-26 03:05:41', '2020-12-26 04:54:32'),
(3, 'Rahul', 'Rahul@mail.com', '1234567891', 'rahul@mail.com', '2020-12-26 05:15:42', '$2y$10$m3TRYteiicZt1tarBL6b6eMITt9g09vgvVX9QfI7XLgO.TSxtQb52', 'Jaipur', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 03:15:42', '2020-12-26 03:15:42'),
(4, 'Mina', 'Mina', '1234567891', 'mina@mail.com', '2020-12-26 06:34:26', '$2y$10$AcoyybkcpKwc8vv2h6fNleFwCytZ/S.Y9//1NyA6.DSfwxWmdXiuS', 'Do Gd', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:34:26', '2020-12-26 04:34:26'),
(5, 'Xd', 'D', '8989865568', 'jgj@mail.com', '2020-12-26 06:35:11', '$2y$10$BUZUzW4vMK/OB/fOmGrfCuGT17Q4ZXDVXPq18XhwZxocPzo/MvwIW', 'Ffxg', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:35:11', '2020-12-26 04:35:11'),
(6, 'Xv', 'Hfhf', '5655688875', 'fh@mail.com', '2020-12-26 06:37:01', '$2y$10$yq6hzNGmrJQbMC36J6tRUuuKt/UGMKBMrYxE.N4NWRMBLCD0yODou', 'Gxh', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:37:01', '2020-12-26 04:37:01'),
(7, 'Gdg', 'Fhxhd', '5554785765', 'do@mail.com', '2020-12-26 06:40:38', '$2y$10$ASTlHVAjzt/vROm6jaS64O6hfmeLKsHep23G.125UxTmLiPvPx1r6', 'Zfgd', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:40:38', '2020-12-26 04:40:38'),
(12, 'Rtu', 'Hhf', '9856868556', 'ugfj@mail.com', '2020-12-26 06:46:08', '$2y$10$UhR1DufsfqSGTk/RYYPy3uBJ35d7bFUOrS5mCbEQqXzrZGR0qvLMe', 'Dg F', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:46:08', '2020-12-26 04:46:08'),
(13, 'F', 'Hx', '7888688686', 'gd@mail.com', '2020-12-26 06:47:52', '$2y$10$lhR1m3pU3PpuNPzT3zYJtO0dIoZslZzya1DzSXIa3TL8XBOThtjR.', 'Gxgdd', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:47:52', '2020-12-26 04:47:52'),
(14, 'Y', 'Hd', '5655886566', 'f@mail.com', '2020-12-26 06:51:31', '$2y$10$x/1wZCW2.WNe5OEVJLNUC.IL262jULx.78UMjniiGVxGxn04S1DnS', 'Gddg', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 04:51:31', '2020-12-26 04:51:31'),
(16, 'Rr', 'Gg', '6565556865', 'tr1@mail.com', '2020-12-28 07:35:13', '$2y$10$YMVFU9X2XI3.v.VrwLQyAOh8ZST6JAyJSpXX4p/EBP2x6pWooemxe', 'Gg', NULL, NULL, NULL, 0, 1, 'ATfcCe93toJCQa9UaxaKJLF9F4QQ3u5vZ4b4YoA43tnpxpLBj69V5URkNPLHH3lnIx6yYRIXNQQG4bxBBnG11GxMGwCh0xk7TcN0', '2020-12-26 04:53:14', '2020-12-28 05:35:13'),
(23, 'Tina', 'Tina', '5654457688', 'tia@mail.com', '2020-12-26 09:36:17', '$2y$10$W2oR50BYRepbJN8QMdt2L.n/Bx2fkvKXX5D41DzKj5nK9pY3ZWtC2', 'Zgddy', NULL, NULL, NULL, 0, 1, NULL, '2020-12-26 07:36:17', '2020-12-26 07:36:17'),
(27, 'Ravin', 'Kumar', '97444585', 'ravinn@gmail.com', '2021-01-04 09:42:31', '$2y$10$n5GQYqCyfdRgJcxjzq/dWuYrGZSdD4vit9AFRotPwxo0I2/sh6gO6', '', NULL, NULL, NULL, 0, 1, 'mGGCRyJdwXck8auAqnzDiC7rdfyGy8o399MTnTTXY5GbBxVi1u6dhPhyQArOjTjovYxj0lIMClUCdRGgfcXlzsX9MIdjSqt7e6kL', '2021-01-04 07:40:29', '2021-01-04 07:42:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_tokens`
--
ALTER TABLE `device_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_cancels`
--
ALTER TABLE `order_cancels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_deliveries`
--
ALTER TABLE `order_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_shippings`
--
ALTER TABLE `order_shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_addresses`
--
ALTER TABLE `other_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`admin_id`,`category_id`,`subcategory_id`,`innercategory_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_slots`
--
ALTER TABLE `time_slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=604;

--
-- AUTO_INCREMENT for table `device_tokens`
--
ALTER TABLE `device_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_cancels`
--
ALTER TABLE `order_cancels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_deliveries`
--
ALTER TABLE `order_deliveries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_shippings`
--
ALTER TABLE `order_shippings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `other_addresses`
--
ALTER TABLE `other_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `time_slots`
--
ALTER TABLE `time_slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
