var Product = function(){
    this.__construct = function(){
        this.cartSave();
    }

    this.cartSave = function(){
        $(document).on("click",".cart-save",function(e){
            e.preventDefault();
            var formData = $("#cartForm").serialize();
            var url = $("#cartForm").attr('action');

            $.post(url,formData,function(data){
                if(data.result == true)
                {
                    $(".cart-save").text("Added to cart");
                }
                else
                {
                    console.log(data);
                    $(".cart-save").html("Add to cart");

                }
            });
        });
    }



    this.__construct();
};

var product = new Product;
