<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('orders'))
        {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->string('order_no')->default(0);
            $table->date('order_date');
            $table->date('exp_del_date');
            $table->float('price')->default(0);
            $table->string('transaction_id');
            $table->enum('status',['complete','failed'])->default('failed');
            $table->string('type');
            $table->string('bankname')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('bank_txn_id')->nullable();
            $table->string('currency')->nullable();
            $table->string('gateway_name')->nullable();
            $table->integer('gift_id')->default(0);
            $table->integer('other_address_id')->default(0);
            $table->integer('timeslot_id')->default(0);
            $table->enum('order_status',['pending','received','shipped','delivered','cancelled'])->default('pending');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
