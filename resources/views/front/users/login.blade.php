@extends('front.layouts.front')
@section('content')

<div class="modal-wrapper">
    <div class="container">
        <div class="row row-sparse">
            <div class="col-md-6">
                <h2 class="title mb-2">Login</h2>
@if(session('message'))
<p class="alert alert-success">{{session('message')}}</p>
@endif

                <form action="{{url('/dologin')}}"  method="post" class="mb-1">
				@csrf
                    <label for="email">Email address <span class="required">*</span></label>
                    <input type="email" name="email" class="form-input form-wide mb-2" id="email" required />

                    <label for="password">Password <span class="required">*</span></label>
                    <input type="password" name="password" class="form-input form-wide mb-2" id="password" required />

                    <div class="form-footer">
                        <button type="submit" name="submit" class="btn btn-primary btn-md">LOGIN</button>

                        <div class="custom-control custom-checkbox form-footer-right">
                            <input type="checkbox" class="custom-control-input" id="lost-password">
                            <label class="custom-control-label form-footer-right" for="lost-password">Remember Me</label>
                        </div>
                    </div><!-- End .form-footer -->
                    <a href="#" class="forget-password">Forgot your password?</a>
                </form>
				
				 <div class="social-login-wrapper">
        <p>Access your account through  your social networks.</p>

        <div class="btn-group">
            <a class="btn btn-social-login btn-md btn-gplus mb-1"><i class="fab fa-google pt-1"></i><span>Google</span></a>
            <a class="btn btn-social-login btn-md btn-facebook mb-1"><i class="icon-facebook"></i><span>Facebook</span></a>
            <a class="btn btn-social-login btn-md btn-twitter mb-1"><i class="icon-twitter"></i><span>Twitter</span></a>
        </div>
    </div>
</div>
            </div><!-- End .col-md-6 -->

   
@endsection