@extends('front.layouts.front')
@section('content')
<div class="col-md-6">

                <h2 class="title mb-2">Register</h2>
@if(session('message'))
                                    <p class="alert alert-success">{{session('message')}}</p>
                                @endif

                <form role="form" action="{{url('/signup')}}" method="post">
				@csrf
                    <label for="email">Email address <span class="required">*</span></label>
					
                    <input type="email" name="email" class="form-input form-wide mb-2" id="email" required>
                     <label for="phoneno">Phone Number <span class="required">*</span></label>
					 <input type="texy" name="phoneno" class="form-input form-wide mb-2" id="phoneno" required>
					 <label for="password">Password <span class="required">*</span></label>
                    <input type="password" name ="password" class="form-input form-wide mb-2" id="password" required>

                    <div class="form-footer">
                        <button type="submit" name="submit" class="btn btn-primary btn-md">Register</button>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="newsletter-signup">
                            <label class="custom-control-label" for="newsletter-signup">Sing up our Newsletter</label>
                        </div><!-- End .custom-checkbox -->
                    </div><!-- End .form-footer -->
                </form>
            </div><!-- End .col-md-6 -->
        </div><!-- End .row -->
</div><!-- End .container -->
@endsection