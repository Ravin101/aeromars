@extends('front.layouts.front')
@section('title','Home')
@section('content')

@include('front.includes.slider')

<section>
	<div class="row">
		@include('front.includes.sidebar')
		<div class="col-lg-9 col-md-8 m-b-3">
			<h2 class="section-title ls-n-20 m-b-1 line-height-1 text-center appear-animate" data-animation-delay="100" data-animation-duration="1500">Products</h2>
			<h3 class="section-sub-title ls-n-20 font-weight-normal m-b-4 text-center appear-animate" data-animation-delay="100" data-animation-duration="1500">All our new arrivals in a exclusive brand selection</h2>
			<div class="row">
				@foreach($products as $product)
				<div class="col-6 col-md-4">
					<div class="product-default inner-quickview inner-icon appear-animate" data-animation-name="fadeInRightShorter">
						<figure>
                            <a href="{{url('product/'.$product->id)}}">
                   
					   <img src="{{url($product->thumbnail)}}" alt="product" width="400" height="400" />
                          
						  </a>
							<div class="label-group">
								<span class="product-label label-sale">-26%</span>
							</div>
							<div class="btn-icon-group">
								<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>
							</div>
							<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a>
							</figure>
						<div class="product-details">
							<div class="category-wrap">
								<div class="category-list">
									<a href="category.html" class="product-category">shoes</a>,
									<a href="category.html" class="product-category">toys</a>
								</div>
								<a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>
							</div>
							<h2 class="product-title">
								<a href="product.html">{{$product->name}}</a>
							</h2>
							<div class="ratings-container">
								<div class="product-ratings">
									<span class="ratings" style="width:0%"></span><!-- End .ratings -->
									<span class="tooltiptext tooltip-top"></span>
								</div><!-- End .product-ratings -->
							</div><!-- End .product-container -->
							<div class="price-box">
								<span class="product-price">&#x20B9;{{$product->price}}</span>
							</div><!-- End .price-box -->
						</div><!-- End .product-details -->
					</div>
				</div><!-- End .col-lg-4 -->
				@endforeach


			</div><!-- End .row -->
		</div>
	</div>
</section>

<!-- include section1  area-->
<!-- include section2  area-->
<!-- include section3  area-->
<!-- include section4  area-->
<!-- include section5  area-->
<!-- include section6  area-->


@endsection
