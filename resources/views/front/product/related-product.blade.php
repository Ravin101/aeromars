<div class="products-section pt-0">
					<h2 class="section-title">Related Products</h2>

					<div class="products-slider owl-carousel owl-theme dots-top">
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-14.jpg')}}">
								</a>
								<div class="label-group">
									<span class="product-label label-sale">-20%</span>
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-13.jpg')}}">
								</a>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-12.jpg')}}">
								</a>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-11.jpg')}}">
								</a>
								<div class="label-group">
									<span class="product-label label-hot">HOT</span>
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-10.jpg')}}">
								</a>
								<div class="label-group">
									<span class="product-label label-hot">HOT</span>
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-9.jpg')}}">
								</a>
								<div class="label-group">
									<span class="product-label label-sale">-30%</span>
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
						<div class="product-default inner-quickview inner-icon">
							<figure>
								<a href="product.html">
									<img src="{{url('public/front/assets/images/products/product-8.jpg')}}">
								</a>
								<div class="label-group">
									<span class="product-label label-sale">-20%</span>
								</div>
								<div class="btn-icon-group">
									<button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-shopping-cart"></i></button>
								</div>
								<a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 
							</figure>
							<div class="product-details">
								<div class="category-wrap">
									<div class="category-list">
										<a href="category.html" class="product-category">category</a>
									</div>
								</div>
								<h3 class="product-title">
									<a href="product.html">Product Short Name</a>
								</h3>
								<div class="ratings-container">
									<div class="product-ratings">
										<span class="ratings" style="width:100%"></span><!-- End .ratings -->
										<span class="tooltiptext tooltip-top"></span>
									</div><!-- End .product-ratings -->
								</div><!-- End .ratings-container -->
								<div class="price-box">
									<span class="old-price">$59.00</span>
									<span class="product-price">$49.00</span>
								</div><!-- End .price-box -->
							</div><!-- End .product-details -->
						</div>
					</div><!-- End .products-slider -->
				</div><!-- End .products-section -->