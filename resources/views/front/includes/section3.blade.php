<section class="sale-banner m-t-3 appear-animate">
                    <div class="banner">
                        <img src="{{url('front/assets/images/banners/banner-4.jpg')}}"  width="1120" height="380" alt="banner" />
                        <div class="banner-layer banner-layer-middle banner-layer-left">
                            <h5 class="font-weight-normal m-b-3 font3 text-left">Outlet Selected Items</h5>
                            <h4 class="mb-0 text-left text-uppercase text-white">big sale up to</h4>
                            <h3 class="text-sale text-left text-white mb-0">80%<small>OFF</small></h3>
                        </div>
                    </div>
                </section>
