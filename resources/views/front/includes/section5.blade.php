<div class="m-b-1"></div><!-- margin -->
                <div class="newsletter-banner appear-animate">
                    <div class="d-flex flex-wrap">
                        <div class="col-md-6 px-3">
                            <h2 class="text-center ls-n-20 m-b-1 text-md-right">Subscribe To Our Newsletter</h2>
                            <h5 class="font-weight-normal text-center text-md-right">Get all the latest information on events, sales and offers.</h5>
                        </div>
                        <div class="col-md-5 widget-newsletter px-3">
                            <form action="#" class="d-flex justify-content-center mb-0">
                                <input type="email" class="form-control mb-0" placeholder="Email address" required/>
                                <button class="btn btn-primary">subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>