 <section class="appear-animate p-y-5">
                    <div class="container">
                            <div class="brands-slider images-center owl-carousel owl-theme nav-pos-outside show-nav-hover appear-animate" data-owl-options="{
                                'margin': 0,
                                'loop': true,
                                'autoplay': true
                            }">
                                <img src="{{url('public/front/assets/images/logos/1.png')}}" width="140"  height="60" alt="logo" />
                                <img src="{{url('public/front/assets/images/logos/2.png')}}" width="140"  height="60" alt="logo" />
                                <img src="{{url('public/front/assets/images/logos/3.png')}}" width="140"  height="60" alt="logo" />
                                <img src="{{url('public/front/assets/images/logos/4.png')}}" width="140"  height="60" alt="logo" />
                                <img src="{{url('public/front/assets/images/logos/5.png')}}" width="140"  height="60" alt="logo" />
                                <img src="{{url('public/front/assets/images/logos/6.png')}}" width="140"  height="60" alt="logo" />
                            </div><!-- End .partners-carousel -->
                        </div><!-- End .container -->
                </section>