 @php
 $categories = DB::table('categories')->where(['is_deleted'=>0])->orderby('id','desc')->get();
 @endphp
 <div class="page-wrapper">
        <header class="header mb-0">
            <div class="header-top">
                <div class="container">
                    <div class="header-left">
                        <div class="header-dropdown mr-auto mr-sm-3 mr-md-0">
                            <a href="#" class="pl-0"><img src="{{url('front/assets/images/flags/ind.png')}}" width="16" height="11" alt="flag" />INDIA</a>
                            <div class="header-menu">
                                <ul>
                                    <li><a href="#"><img src="{{url('front/assets/images/flags/ind.png')}}" width="16" height="11" alt="flag" />INDIA</a></li>

                                </ul>
                            </div><!-- End .header-menu -->
                        </div>
                        <div class="header-dropdown">
                                <a href="#" class="pl-0" >INR</a>
                                {{-- <div class="header-menu">
                                     <ul>
                                        <li><a href="#">INR</a></li>
                                        <li><a href="#">USD</a></li>
                                    </ul> -
                                </div><!-- End .header-menu --> --}}
                        </div>
                    </div><!-- End .header-left -->

                    <div class="header-right">
                      <!--  <div class="wel-msg text-uppercase d-none d-sm-block p-r-2">FREE Returns. Standard Shipping Orders $99+</div> -->
                        <span class="separator d-none d-sm-inline-block"></span>
                        <ul class="top-links mega-menu show-arrow d-none d-xl-block mt-0 mb-0">
                            <li class="menu-item narrow"><a href="{{url('/myaccount')}}">My Account</a></li>
                            <li class="menu-item narrow"><a href="about.html">About Us</a></li>

                            <li class="menu-item narrow"><a href="#">Wishlist</a></li>
                            <li class="menu-item narrow"><a href="cart.html">Cart</a></li>
                            <li class="menu-item">
							@if (Auth::check())
							<h6>{{Auth::user()->first_name}}</h6>
						    <a href="{{url('/logout')}}" class="btn-logout" id="logged">Logout </a> 
							 @else
						 <a class="login" href="{{url('/login')}}">Login</a>
						 
							 @endif
			
                            </li>
							<li class="menu-item">
                                <a class="login" href="{{url('/signup')}}">Register</a>
                            </li>
                        </ul>
                        <span class="separator d-none d-xl-block"></span>
                        <div class="social-icons py-2 py-xl-0">
                            <a target="_blank" rel="nofollow" class="social-icon social-facebook icon-facebook" href="#" title="Facebook"></a>
                            <a target="_blank" rel="nofollow" class="social-icon social-twitter icon-twitter" href="#" title="Twitter"></a>
                            <a target="_blank" rel="nofollow" class="social-icon social-instagram icon-instagram" href="#" title="Instagram"></a>
                        </div>
                    </div><!-- End .header-right -->
                </div><!-- End .container -->
            </div><!-- End .header-top -->
            <div class="header-middle">
                <div class="container">
                    <div class="header-left">
                        <button class="mobile-menu-toggler mr-2" type="button">
                            <i class="icon-menu"></i>
                        </button>
                        <a href="{{url('/')}}" class="logo">
                            <img src="{{url('front/assets/images/aeromars.jpg')}}" alt="Aeromars Logo" width="170" height="80" />
                        </a>
                    </div><!-- End .header-left -->
                    <div class="header-center flex-1 ml-0 justify-content-end justify-content-lg-start">
                        <div class="header-search header-search-inline header-search-category w-lg-max pr-2 pr-lg-0">
                            <a href="#" class="search-toggle header-icon d-none d-sm-inline-block d-lg-none mr-0" role="button"><i class="icon-search-3"></i></a>
                            <form id="search_form" action="{{ url('/') }}" method="get">
                                <div class="header-search-wrapper">
                                    <input type="search" class="form-control" name="search" id="q" value="{{ $search ?? "" }}" placeholder="Search..." required="">
                                    <div class="select-custom">
                                        <select onchange="category_search()" id="cat" name="cat">
                                            {{-- <option value="">All Categories</option> --}}
                                            <option value="">Select Category</option>
                                            @foreach($categories as $category)
                                            <option value="{{ $category->name }}" @if(isset($cat)) {{ ($category->name == $cat) ? "selected":"" }} @endif>- {{ $category->name }}</option>
                                            @endforeach

                                        </select>
                                    </div><!-- End .select-custom -->
                                    <button class="btn icon-search-3" type="submit"></button>
                                </div><!-- End .header-search-wrapper -->
                            </form>
                        </div><!-- End .header-search -->
                    </div>
                    <div class="header-right">
                        <div class="header-contact d-none d-lg-flex ml-5">
                            <i class="icon-phone-2"></i>
                            <h6>Call us now<a href="tel:#" class="font1 text-secondary">0141-46627882 / +91-8088877234</a></h6>
                        </div>

                        <a  href="{{url('/myaccount')}}" class="header-icon"><i class="icon-user-2"></i></a>

                        <a href="#" class="header-icon"><i class="icon-wishlist-2"></i></a>

                        <div class="dropdown cart-dropdown">
                            <a href="#" class="dropdown-toggle dropdown-arrow" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                <i class="icon-shopping-cart"></i>
                                <span class="cart-count badge-circle">2</span>
                            </a>

                            <div class="dropdown-menu">
                                <div class="dropdownmenu-wrapper">
                                    <div class="dropdown-cart-header">
                                        <span> 2 itmes</span>

                                        <a href="{{url('cartlist')}}" class="float-right">View Cart</a>
                                    </div><!-- End .dropdown-cart-header -->
                              
							   @foreach($products as $product)
                                    <div class="dropdown-cart-products">
                                        <div class="product">
                                            <div class="product-details">
                                                <h4 class="product-title">
                                                    <a href="product.html">{{$product->name}}</a>
                                                </h4>

                                                <span class="cart-product-info">
                                                    <span class="cart-product-qty">1</span>
                                                   {{$product->price}}
                                                </span>
                                            </div><!-- End .product-details -->

                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="{{url($product->thumbnail)}}" alt="product" width="80" height="80" />
                                                </a>
                                                <a href="#" class="btn-remove icon-cancel" title="Remove Product"></a>
                                            </figure>
                                        </div><!-- End .product -->
                                    </div><!-- End .cart-product -->
                                   @endforeach
                                    <div class="dropdown-cart-total">
                                        <span>Total</span>

                                        <span class="cart-total-price float-right">{{$product->total}}</span>
                                    </div><!-- End .dropdown-cart-total -->

                                    <div class="dropdown-cart-action">
                                        <a href="checkout-shipping.html" class="btn btn-primary btn-block">Checkout</a>
                                    </div><!-- End .dropdown-cart-total -->
                                </div><!-- End .dropdownmenu-wrapper -->
                            </div><!-- End .dropdown-menu -->
                        </div><!-- End .dropdown -->
                    </div><!-- End .header-right -->
                </div>
            </div>
            <div class="header-bottom mb-lg-2 sticky-header" data-sticky-options="{
				'move': [
					{
						'item': '.header-icon:not(.search-toggle)',
						'position': 'end',
						'clone': false
					},
					{
						'item': '.cart-dropdown',
						'position': 'end',
						'clone': false
					}
				],
				'moveTo': '.container',
				'changes': [
					{
						'item': '.logo-white',
						'removeClass': 'd-none'
					},
					{
						'item': '.header-icon:not(.search-toggle)',
                        'removeClass': 'pb-md-1',
                        'addClass': 'text-white'
					},
					{
						'item': '.cart-dropdown',
                        'addClass': 'text-white'
					}
				]
			}">
                <div class="container">
                    <div class="logo logo-transition logo-white w-100 d-none">
                        <a href="index.html">
                            <img src="{{url('front/assets/images/aeromarsindia.png')}}" alt="Aeromars Logo" width="170" height="55"/>
                        </a>
                    </div>
                    <div class="bg-dark w-100">
                        <nav class="main-nav">
                            <ul class="menu d-flex justify-content-center">
                                <li class="active">
                                    <a href="{{ url('/') }}">Home</a>
                                </li>
                                <li>
                                    <a href="{{ url('/') }}">Categories</a>
                                    <div class="megamenu megamenu-fixed-width megamenu-3cols">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <a href="#" class="nolink">Type</a>
                                                <ul class="submenu">
                                                    <li><a href="category.html">Accessories</a></li>
                                                    <li><a href="category-banner-boxed-slider.html">Home Appliances</a></li>
                                                    <li><a href="category-banner-boxed-image.html">Electronics</a></li>
                                                    <li><a href="category.html">Home Decore</a></li>
                                                    <li><a href="category-sidebar-right.html">Kitchen Crafts</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- End .megamenu -->
                                </li>
                                <li>
                                    <a href="{{url('/')}}">Products</a>
                                    <div class="megamenu megamenu-fixed-width">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <a href="#" class="nolink">Variations 1</a>
                                                <ul class="submenu">
                                                    <li><a href="product.html">Horizontal Thumbs</a></li>
                                                    <li><a href="product-full-width.html">Vertical Thumbnails</a></li>
                                                    <li><a href="product.html">Inner Zoom</a></li>
                                                    <li><a href="product-addcart-sticky.html">Addtocart Sticky</a></li>
                                                    <li><a href="product-sidebar-left.html">Accordion Tabs</a></li>
                                                </ul>
                                            </div><!-- End .col-lg-4 -->
                                            <div class="col-lg-3">
                                                <a href="#" class="nolink">Variations 2</a>
                                                <ul class="submenu">
                                                    <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
                                                    <li><a href="product-simple.html">Simple Product</a></li>
                                                    <li><a href="product-sidebar-left.html">With Left Sidebar</a></li>
                                                </ul>
                                            </div><!-- End .col-lg-4 -->

                                  <div class="col-lg-3 p-0">

                                            </div><!-- End .col-lg-4 -->
                                        </div><!-- End .row -->
                                    </div><!-- End .megamenu -->
                                </li>
                                 <li><a href="about.html">About Us</a></li>
                              </ul>
                          </nav>
                    </div>
                </div>
            </div>
        </header><!-- End .header -->

		<main class="main">
            <div class="container">

@push('scripts')
<script>
function category_search()
{

        console.log("check");
        $("#search_form").submit();

};
</script>
@endpush
