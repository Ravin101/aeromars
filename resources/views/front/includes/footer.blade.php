  </div><!-- End .container -->
        </main><!-- End .main -->
  <footer class="footer bg-dark appear-animate">
            <div class="container">
                <div class="footer-top">
                    <div class="row row-sm">
                        <div class="col-lg-5">
                            <div class="widget widget-about">
                                <h4 class="widget-title">About Us</h4>
                                <a href="index.html" class="logo mb-2">
                                    <img src="{{url('front/assets/images/aeromarsindia.png')}}" class="m-b-3" width="110" height="46" alt="Porto Logo" />
                                </a>
                                <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec vestibulum magna, et dapibus lacus. <a href="#" class="text-white"><strong><ins>read more...</ins></strong></a></p>
                            </div><!-- End .widget -->

                            <div>
                                <div class="widget">
                                    <h4 class="widget-title">Contact Info</h4>
                                    <ul class="contact-info d-flex flex-wrap">
                                        <li>
                                            <span class="contact-info-label">Address:</span>Aeromars India Technologies
											G-474, Road No-9A, Vishwakarma Industrial Area, 
											Jaipur-302013
                                        </li>
                                        <li>
                                            <span class="contact-info-label">Phone:</span><a href="tel:">0141-46627882/ +91-8088877234 /+91-9057289044</a>
                                        </li>
                                        <li>
                                            <span class="contact-info-label">Email:</span> <a href="mailto:mail@example.com">info@aeromars.co.in</a>
                                        </li>
                                        <li>
                                            <span class="contact-info-label">Working Days/Hours:</span><a href="tel:">Mon - Sun / 9:00 AM - 8:00 PM</a>
                                        </li>
                                    </ul>
                                </div><!-- End .widget -->
                                <div class="social-icons">
                                    <a href="#" class="social-icon social-facebook icon-facebook" target="_blank" title="Facebook"></a>
                                    <a href="#" class="social-icon social-twitter icon-twitter" target="_blank" title="Twitter"></a>
                                    <a href="#" class="social-icon social-instagram icon-instagram" target="_blank" title="Instagram"></a>
                                </div>
                            </div><!-- End .widget -->
                        </div><!-- End .col-lg-6 -->
                       <!-- <div class="col-lg-3">
                            <div class="widget">
                                <h4 class="widget-title">Featured Products</h4>
                                <div class="product-default left-details product-widget">
                                    <figure>
                                        <a href="product.html">
                                            <img src="{{url('front/assets/images/products/product-7.jpg')}}" width="95" height="95">
                                        </a>
                                    </figure>
                                    <div class="product-details">
                                        <h2 class="product-title text-white">
                                            <a href="product.html">Woman Black Blouse</a>
                                        </h2>
                                        <div class="ratings-container">
                                            <div class="product-ratings">
                                                <span class="ratings" style="width:100%"></span>End .ratings 
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div><!-- End .product-ratings 
                                        </div><!-- End .product-container 
                                        <div class="price-box">
                                            <span class="product-price">$129.00</span>
                                        </div><!-- End .price-box 
                                    </div><!-- End .product-details 
                                </div>
                                <div class="product-default left-details product-widget">
                                    <figure>
                                        <a href="product.html">
                                            <img src="{{url('front/assets/images/products/product-4.jpg')}}" width="95" height="95">
                                        </a>
                                    </figure>
                                    <div class="product-details">
                                        <h2 class="product-title text-white">
                                            <a href="product.html">Jeans Wear</a>
                                        </h2>
                                        <div class="ratings-container">
                                            <div class="product-ratings">
                                                <span class="ratings" style="width:100%"></span><!-- End .ratings 
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div><!-- End .product-ratings 
                                        </div><!-- End .product-container 
                                        <div class="price-box">
                                            <span class="product-price">$185.00</span>
                                        </div><!-- End .price-box 
                                    </div><!-- End .product-details 
                                </div>
                                <div class="product-default left-details product-widget">
                                    <figure>
                                        <a href="product.html">
                                            <img src="{{url('front/assets/images/products/product-3.jpg')}}" width="95" height="95">
                                        </a>
                                    </figure>
                                    <div class="product-details">
                                        <h2 class="product-title text-white">
                                            <a href="product.html">Porto Sticky Info</a>
                                        </h2>
                                        <div class="ratings-container">
                                            <div class="product-ratings">
                                                <span class="ratings" style="width:100%"></span End .ratings 
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div><!-- End .product-ratings 
                                        </div><!-- End .product-container 
                                        <div class="price-box">
                                            <span class="product-price">$129.00</span>
                                        </div><!-- End .price-box 
                                    </div><!-- End .product-details 
                                </div>
                            </div><!-- End .widget 
                        </div> -->
                        <div class="col-lg-4">
                            <div class="widget">
                                <h4 class="widget-title">Twitter Feeds</h4>

                                <div class="twitter-feed">
                                    <div class="twitter-feed-content">
                                        <a class="d-inline-block align-top text-white" href="#">
                                            <i class="mr-2 icon-twitter"></i>
                                        </a>
                                        <p class="d-inline-block">Oops, our twitter feed is unavailable right now.<br>
                                        <a class="meta" href="http://twitter.com/swtheme">Follow us on Twitter</a></p>

                                    </div><!-- End .twitter-feed-content -->
                                </div><!-- End .twitter-feed -->
                            </div><!-- End .widget -->
                        </div>
                    </div><!-- End .row -->
                </div>
            </div>

            <div class="container">
                <div class="footer-middle">
                    <div class="row">
                        <div class="col-lg-5 mb-4 mb-lg-0">
                            <div class="widget">
                                <h4 class="widget-title">Customer Service</h4>

                                <ul class="links link-parts row mb-0">
                                    <div class="link-part col-md-6 col-sm-12">
                                        <li><a href="about.html">About us</a></li>
                                        <li><a href="contact.html">Contact us</a></li>
                                        <li><a href="#">My Account</a></li>
                                    </div>
                                    <div class="link-part col-md-6 col-sm-12">
                                        <li><a href="#">Order history</a></li>
                                        <li><a href="#">Advanced search</a></li>
                                        <li><a href="#" class="login-link">Login</a></li>
                                    </div>
                                </ul>
                            </div>
                        </div><!-- End .col-lg-3 -->

                        <div class="col-lg-7">
                            <div class="widget">
                                <h4 class="widget-title">Main Features</h4>

                                <ul class="links link-parts row mb-0">
                                    <div class="link-part col-md-6 col-sm-12">
                                        <li><a href="#">Super Fast Wordpress Theme</a></li>
                                        <li><a href="#">1st Fully working Ajax Theme</a></li>
                                        <li><a href="#">34 Unique Shop Layouts</a></li>
                                    </div>
                                    <div class="link-part col-md-6 col-sm-12">
                                        <li><a href="#">Powerful Admin Panel</a></li>
                                        <li><a href="#">Mobile & Retina Optimized</a></li>
                                    </div>
                                </ul>
                            </div>
                        </div><!-- End .col-lg-3 -->
                    </div><!-- End .row -->
                </div><!-- End .footer-middle -->
            </div>

            <div class="container">
                <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                    <p class="footer-copyright py-3 pr-4 mb-0">&copy; AEROMARS INDIA. 2020. All Rights Reserved</p>

                    <img src="{{url('front/assets/images/payments.png')}}" alt="payment" width="240" height="52" class="footer-payments py-3" />
                </div><!-- End .footer-bottom -->
            </div><!-- End .container -->
        </footer><!-- End .footer -->

	</div><!-- End .page-wrapper -->
	<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

    <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li class="active"><a href="index.html">Home</a></li>
                    <li>
                        <a href="category.html">Categories</a>
                        <ul>
                            <li><a href="category-banner-full-width.html">Full Width Banner</a></li>
                            <li><a href="category-banner-boxed-slider.html">Boxed Slider Banner</a></li>
                            <li><a href="category-banner-boxed-image.html">Boxed Image Banner</a></li>
                            <li><a href="category.html">Left Sidebar</a></li>
                            <li><a href="category-sidebar-right.html">Right Sidebar</a></li>
                            <li><a href="category-flex-grid.html">Product Flex Grid</a></li>
                            <li><a href="category-horizontal-filter1.html">Horizontal Filter 1</a></li>
                            <li><a href="category-horizontal-filter2.html">Horizontal Filter 2</a></li>
                            <li><a href="#">Product List Item Types</a></li>
                            <li><a href="category-infinite-scroll.html">Ajax Infinite Scroll<span class="tip tip-new">New</span></a></li>
                            <li><a href="category-banner-full-width.html">3 Columns Products</a></li>
                            <li><a href="category.html">4 Columns Products</a></li>
                            <li><a href="category-5col.html">5 Columns Products</a></li>
                            <li><a href="category-6col.html">6 Columns Products</a></li>
                            <li><a href="category-7col.html">7 Columns Products</a></li>
                            <li><a href="category-8col.html">8 Columns Products</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="product.html">Products</a>
                        <ul>
                            
                            <li>
                                
                            </li>
                         </ul>
                    </li>
                    <li>
                        <a href="#">Pages<span class="tip tip-hot">Hot!</span></a>
                        <ul>
                            <li><a href="cart.html">Shopping Cart</a></li>
                            <li>
                                <a href="#">Checkout</a>
                                <ul>
                                    <li><a href="checkout-shipping.html">Checkout Shipping</a></li>
                                    <li><a href="checkout-shipping-2.html">Checkout Shipping 2</a></li>
                                    <li><a href="checkout-review.html">Checkout Review</a></li>
                                </ul>
                            </li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="#" class="login-link">Login</a></li>
                            <li><a href="forgot-password.html">Forgot Password</a></li>
                        </ul>
                    </li>
                    <li><a href="blog.html">Blog</a>
                        <ul>
                            <li><a href="single.html">Blog Post</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Contact Us</a></li>
                    <li><a href="#">Special Offer!<span class="tip tip-hot"></span></a></li>
                    <li><a href="https://1.envato.market/DdLk5" target="_blank">a></li>
                </ul>
            </nav><!-- End .mobile-nav -->

            <div class="social-icons">
                <a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .mobile-menu-wrapper -->
    </div><!-- End .mobile-menu-container -->

   <!-- <div class="newsletter-popup mfp-hide bg-img bg-img" id="newsletter-popup-form" style="background-image: url(assets/images/newsletter_popup_bg.jpg)">
        <div class="newsletter-popup-content">
            <img src="{{url('front/assets/images/logo-black.png')}}" alt="Logo" class="logo-newsletter" width="95" height="49" />
            <h2>BE THE FIRST TO KNOW</h2>
            <p>Subscribe to the Porto eCommerce newsletter to receive timely updates from your favorite products.</p>
            <form action="#">
                <div class="input-group">
                    <input type="email" class="form-control" id="newsletter-email" name="newsletter-email" placeholder="Email address" required>
                    <input type="submit" class="btn" value="Go!">
                </div><!-- End .from-group -->
            <!--</form>
            <div class="newsletter-subscribe">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="1">
                        Don't show this popup again
                    </label>
                </div>
            </div>
        </div><!-- End .newsletter-popup-content -->
    </div>
	<!-- End .newsletter-popup -->
    <!-- Add Cart Modal -->
    <div class="modal fade" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body add-cart-box text-center">
            <p>You've just added this product to the<br>cart:</p>
            <h4 id="productTitle"></h4>
            <img src="" id="productImage" width="100" height="100" alt="adding cart image">
            <div class="btn-actions">
                <a href="cart.html"><button class="btn-primary">Go to cart page</button></a>
                <a href="#"><button class="btn-primary" data-dismiss="modal">Continue</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <a id="scroll-top" href="#top" title="Top" role="button"><i class="fas fa-chevron-up"></i></a>

    <!-- Plugins JS File -->
    <script src="{{url('front/assets/js/jquery.min.js')}}"></script>
    <script src="{{url('front/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{url('front/assets/js/plugins.min.js')}}"></script>
    <script src="{{url('front/assets/js/jquery.appear.min.js')}}"></script>
    <!-- Main JS File -->
    <script src="{{url('front/assets/js/main.min.js')}}"></script>

    @stack('scripts')
</body>

</html>
