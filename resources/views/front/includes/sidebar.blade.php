<aside class="sidebar-home col-lg-3 col-md-4 order-lg-first">
                            <div class="side-menu-wrapper m-b-3 border-0">
                                <h2 class="side-menu-title bg-primary font-weight-sb text-white"><i class="icon-menu"></i>Shop By Category</h2>

                                <nav class="side-nav border border-top-0">
                                    <ul class="menu menu-vertical sf-arrows">
                                        <li><a href="#">Accessories</a></li>
                                        <li><a href="#">Home Appliance</a></li>
                                        <li><a href="#">Electronics</a></li>
                                        <li><a href="#">Home Decor</a></li>
                                        <li><a href="#">Kitchen Crafts</a></li>
                                        <li><a href="#"></a></li>
                                    </ul>
                                </nav>
                            </div><!-- End .side-menu-container -->
                            <div class="feature-boxes-container mt-3">
                                <div class="row mb-1">
                                    <div class="col-12 appear-animate" data-animation-name="fadeInUpShorter">
                                        <div class="feature-box m-b-5 mx-sm-5 mx-md-3 feature-box-simple text-center">
                                            <i class="icon-earphones-alt"></i>
            
                                            <div class="feature-box-content">
                                                <h3 class="mb-0 pb-1">Customer Support</h3>
                                                <h5>Need Assistence?</h5>
            
                                                <p>Lorem ipsum dolor amet, consectetur.</p>
                                            </div><!-- End .feature-box-content -->
                                        </div><!-- End .feature-box -->
                                    </div><!-- End .col-md-4 -->
                                    <div class="col-12 appear-animate" data-animation-delay="500" data-animation-name="fadeInUpShorter">
                                        <div class="feature-box m-b-5 mx-sm-5 mx-md-3 feature-box-simple text-center">
                                            <i class="icon-credit-card"></i>
            
                                            <div class="feature-box-content">
                                                <h3 class="mb-0 pb-1">Secured Payment</h3>
                                                <h5>Safe &amp; Fast</h5>
            
                                                <p>Lorem ipsum dolor amet, consectetur.</p>
                                            </div><!-- End .feature-box-content -->
                                        </div><!-- End .feature-box -->
                                    </div><!-- End .col-md-4 -->
                                    <div class="col-12 appear-animate" data-animation-delay="800" data-animation-name="fadeInUpShorter">
                                        <div class="feature-box m-b-5 mx-sm-5 mx-md-3 feature-box-simple text-center">
                                            <i class="icon-action-undo "></i>
            
                                            <div class="feature-box-content">
                                                <h3 class="mb-0 pb-1">RETURNS</h3>
                                                <h5>Easy &amp; Free</h5>
            
                                                <p>Lorem ipsum dolor sit amet.</p>
                                            </div><!-- End .feature-box-content -->
                                        </div><!-- End .feature-box -->
                                    </div><!-- End .col-md-4 -->
                                </div><!-- End .feature-boxes-container.row -->
                            </div>
                        </aside><!-- End .col-lg-3 -->