<div class="home-slider owl-carousel owl-carousel-lazy owl-theme nav-pos-outside show-nav-hover slide-animate">
                    @foreach($banners as $banner)
                    <div class="home-slide banner banner-md-vw-small">
                        <img class="owl-lazy slide-bg" src="{{ asset($banner->image) }}" data-src="{{ asset($banner->image)}}" alt="banner" width="1120" height="445"/>
                        <div class="banner-layer slide-2 banner-layer-left banner-layer-middle text-right">
                            <h4 class="m-b-3 text-right appear-animate" data-animation-delay="700" data-animation-name="splitRight">{{ $banner->title }}</h4>

                            <h5 class="d-inline-block ls-n-20 m-r-3 p-t-3 text-left appear-animate" data-animation-delay="1900" data-animation-duration="1200" data-animation-name="fadeInLeft">STARTING AT</h5>
                            <h4 class="text-price ls-n-20 m-b-4 text-left float-right appear-animate" data-animation-delay="1900" data-animation-duration="1200" data-animation-name="fadeInUp">$<b>199</b>99</h4>
                            <div class="clearfix"></div>
                            <div class="mb-0 appear-animate" data-animation-delay="2400" data-animation-name="fadeInUpShorter">
                                <a href="#">
                                    <button class="btn btn-modern btn-lg btn-dark">Shop Now!</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{-- <div class="home-slide banner banner-md-vw-small">
                        <img class="owl-lazy slide-bg" src="{{ asset('public/front/assets/images/slider/slide-2.jpg')}}" data-src="{{ asset('public/front/assets/images/slider/slide-2.jpg')}}" alt="banner" width="1120" height="445"/>
                        <div class="banner-layer slide-1 banner-layer-right banner-layer-middle">
                            <h4 class="m-b-3 appear-animate" data-animation-delay="700" data-animation-name="splitRight">Trending Items We Love</h4>

                            <h5 class="ls-n-20 float-left m-r-3 p-t-3 appear-animate" data-animation-delay="1900" data-animation-duration="1200" data-animation-name="fadeInLeft" >STARTING AT</h5>
                            <h4 class="ls-n-20 text-price m-b-4 appear-animate" data-animation-delay="1900" data-animation-duration="1200" data-animation-name="fadeInUp">$<b>19</b>99</h4>
                            <div class="mb-0 appear-animate" data-animation-delay="2400" data-animation-name="fadeInUpShorter">
                                <a href="#">
                                    <button class="btn btn-modern btn-lg btn-dark">Shop Now!</button>
                                </a>
                            </div>
                        </div>
                    </div> --}}
                </div><!-- End .home-slider -->

				 <section>
                    <div class="container">
                        <div class="info-boxes-container border-bottom mb-3">
                            <div class="row row-joined">
                                <div class="info-box col-sm-6 col-md-6 col-lg-3">
                                    <div class="info-box-icon-left w-100">
                                        <i class="icon-shipping"></i>
                                        <div class="info-box-content">
                                            <h4>FREE SHIPPING &amp; RETURN</h4>
                                        </div><!-- End .info-box-content -->
                                    </div>
                                </div><!-- End .info-box -->
                                <div class="info-box col-sm-6 col-md-6 col-lg-3">
                                    <div class="info-box-icon-left w-100">
                                        <i class="icon-money"></i>
                                        <div class="info-box-content">
                                            <h4>MONEY BACK GUARANTEE</h4>
                                        </div><!-- End .info-box-content -->
                                    </div>
                                </div><!-- End .info-box -->
                                <div class="info-box col-sm-6 col-md-6 col-lg-3">
                                    <div class="info-box-icon-left w-100">
                                        <i class="icon-support"></i>
                                        <div class="info-box-content">
                                            <h4>ONLINE SUPPORT 24/7</h4>
                                        </div><!-- End .info-box-content -->
                                    </div>
                                </div><!-- End .info-box -->
                                <div class="info-box col-sm-6 col-md-6 col-lg-3">
                                    <div class="info-box-icon-left w-100">
                                        <i class="icon-secure-payment"></i>
                                        <div class="info-box-content">
                                            <h4>SECURE PAYMENT</h4>
                                        </div><!-- End .info-box-content -->
                                    </div>
                                </div><!-- End .info-box -->
                            </div><!-- End .row -->
                        </div><!-- End .info-boxes-container -->
                    </div>
                </section>
