 <section class="banner-container appear-animate">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="banner">
                                <img src="{{url('front/assets/images/banners/banner-1.png')}}"  width="360" height="250" alt="banner" />
                                <div class="banner-layer banner-layer-right banner-layer-middle text-right">
                                    <h3 class="m-b-1 font3 text-right text-primary">Orange</h3>
                                    <h5 class="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                    <h4 class="ls-n-20 text-price float-right text-left">$<b>19</b>99</h4>
                                    <div class="mb-0 clearfix text-right">
                                        <a href="category.html">
                                            <button class="btn btn-modern btn-sm btn-dark">Shop Now!</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="banner">
                                <img src="{{url('front/assets/images/banners/banner-2.png')}}"  width="360" height="250" alt="banner" />
                                <div class="banner-layer banner-layer-right banner-layer-middle text-right">
                                    <h3 class="m-b-1 font3 text-right text-primary">White</h3>
                                    <h5 class="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                    <h4 class="ls-n-20 text-price float-right text-left">$<b>29</b>99</h4>
                                    <div class="mb-0 clearfix text-right">
                                        <a href="category.html">
                                            <button class="btn btn-modern btn-sm btn-dark">Shop Now!</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="banner">
                                <img src="{{url('front/assets/images/banners/banner-3.jpeg')}}"  width="360" height="250" alt="banner" />
                                <div class="banner-layer banner-layer-right banner-layer-middle text-right">
                                    <h3 class="m-b-1 font3 text-right text-primary">Black</h3>
                                    <h5 class="ls-n-20 d-inline-block m-r-2 text-left">FROM</h5>
                                    <h4 class="ls-n-20 text-price float-right text-left">$<b>39</b>99</h4>
                                    <div class="mb-0 clearfix text-right">
                                        <a href="category.html">
                                            <button class="btn btn-modern btn-sm btn-dark">Shop Now!</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
