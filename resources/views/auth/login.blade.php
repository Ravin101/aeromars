<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>User Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{url('public/assets/img/icon.ico')}}" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="{{url('public/assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ["{{url('public/assets/css/fonts.css')}}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	
	<!-- CSS Files -->
	<link rel="stylesheet" href="{{url('public/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('public/assets/css/azzara.min.css')}}">
	<style>
	.login{
	 background: linear-gradient(to bottom left, #0066cc 0%, #9966ff 100%);
	}
	
	.whole-signin {
    min-height: 30px;
    min-width: 30px;
    resize: both;
    overflow: auto;
    max-height: fit-content;
    max-width: fit-content;
}

.form-group {
  font-family: Arial, Helvetica, sans-serif;
}
	</style>
</head>


<body class="login">
 
    <div class="wrapper wrapper-login">
	
			<form action=" " method="post">
			 @csrf
			<div class="whole-signin" id="test">
		<div class="container container-login animated fadeIn">
		 <a href="{{url('dashboard')}}" class="logo">
		<img src="{{url('public/images/aeromarsindia.png')}}" 
		alt="..."  style="width:150px; margin-left:100px;margin-bottom:13px;margin-top:5px">
					
				</a>
			
			<h1 class="text-center" style="color:#0f121c"></h1>
			<h2 class="text-center" style="color:#fffff ;margin-right:10px";><b>User Login</b></h2>
   @if(session('message'))
   <p class="alert alert-success">{{session('message')}}</p>
   @endif
@if(session('errorMsg'))
   <p class="alert alert-danger">{{session('errorMsg')}}</p>
   @endif




    @if(session('msg'))
<p class="alert alert-success">{{session('msg')}}</p>
   @endif
             
			<div class="login-form">
			
				<div class="form-group form-floating-label">
					<input id="email" name="email" type="text" class="form-control input-border-bottom" required>
					<label for="email" class="placeholder"><b>Email</b></label>
				</div>
				<div class="form-group form-floating-label">
					<input id="password" name="password" type="password" class="form-control input-border-bottom" required>
					<label for="password" class="placeholder"><b>Password</b></label>
					<div class="show-password">
						<i class="icon-eye"></i>
					</div>
				</div>
				<div class="row form-sub m-0">
				<!--	<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="rememberme">
						<label class="custom-control-label" for="rememberme">Remember Me</label>
					</div> -->
					
					<a href="#" id="forget-button" class="link float-right" >Forget Password ?</a>
				</div>
				<div class="form-action mb-3">
					 <button type="submit"  class="btn btn-primary btn-rounded btn-login">Login</button>
				</div>
			
			</div>
			</form>
		</div>
		</div>
  
	
		<div class="forget-form" id="forget1" style="display:none">
	<div class="container container-login animated fadeIn">
	<form method="post" action="{{url('/forget-password')}}">
	@csrf
	 <a href="{{url('dashboard')}}" class="logo">
					<img src="{{url('public/images/b2_logo.png')}}" alt="..." class=" rounded-circle" style="width:100px; margin-left:113px; margin-bottom:11px;">
				</a>
	
				<div class="form-group form-floating-label">
					<input id="email" name="email" type="text" class="form-control input-border-bottom" required>
					<label for="Email" class="placeholder">Email</label>
				</div>
			
				
				<div class="form-action mb-3">
					<button type="submit"  class="btn btn-primary btn-rounded btn-login mr-3">Submit</button>
				</div>
				</form>
				<div class="login-account">
					<span class="msg">Did you have password</span>
					<a href="#"  class="link login-page">Login</a>
				</div>
				
			</div>
			</div>
	</div>
	
	<script src="{{url('public/assets/js/core/jquery.3.2.1.min.js')}}"></script>
	<script src="{{url('public/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script src="{{url('public/assets/js/core/popper.min.js')}}"></script>
	<script src="{{url('public/assets/js/core/bootstrap.min.js')}}"></script>
	<script src="{{url('public/assets/js/atlantis.min.js')}}"></script>
	<script>
    $(document).ready(function(){
 
   $('.alert-success').fadeOut(5000);
     });
	 
	 $(document).ready(function(){
  $("#forget-button").click(function(){
    $("#forget1").show();
    $(".whole-signin").hide();
    $(".whole-signup").hide();
  });
  $(".login-page").click(function(){
	  $(".whole-signin").show();
	  $(".whole-signup").hide();
	 $("#forget1").hide();
	  
  });
});
 </script>

</body>
   
</html>