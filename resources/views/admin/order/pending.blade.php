@extends('layouts.admin')
@section('title','Products')
@section('content')



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Order Tables
        <small>Order Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('order')}}">Order</a></li>
        <li class="active">Order tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         @if(session('meesage'))
         <p class="alert alert-success">{{session('message')}}</p>
         @endif
          <div class="box">

            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
           
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Order Id</th>
                  <th>Price</th>
                  <th>Transaction Id</th>
                  <th>UserId</th>
                  <th>Gift Id</th>
                  <th>Status</th>
                  <th>Action</th> 
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($orders as $order)	
                <tr>		
                  <td>{{$i}}</td>
                  <td>{{$order->order_id}}</td>
                  <td>{{$order->price}}</td>
                  <td>{{$order->transaction_id}}</td>
                  <td>{{$order->user_id}}</td>
                  <td>{{$order->gift_id}}</td>
				  <td>{{$order->order_status}}</td>
                  <td>
				  
      <form method="post" id="delete_form_{{$order->id}}" action="{{url('order/'.$order->id)}}"  >
        @method('DELETE')
        @csrf
        <input type="hidden" name="token" id="token" value="{{$order->token}}" />
        <input type="hidden" name="id" id="id" value="{{$order->id}}" />
        
          
          <a href="{{url('order/'.$order->id.'/edit')}}" class="btn btn-sm btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
          <button type="submit" id="{{$order->id}}" class="btn btn-sm btn-danger delete_product" ><i class="fa fa-trash" aria-hidden="true"></i></button>
              
        </form> 
                  
                  
                  
                  </td>
				   
                </tr>
				<?php $i++;?>
               @endforeach
              
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
$(document).ready(function(){
  $(".delete_product").click(function(e){
    e.preventDefault();
	var id = $(this).attr('id');
	 bootbox.confirm({
		  message:"Are you sure you want to delete this order?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){
						
			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });

 
 


 

});
</script>
  @endsection