@extends('admin.layouts.admin')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">CMS Pages</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('cms')}}">CMS Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    
                   <div class="col-md-12">
                       
                    
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('cms')}}" method="post" id="exampleValidation">
                    @csrf
                   <div class="card">
                    
                    
                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('cms')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All CMS Pages" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All CMS Pages
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{old('title')}}">
                                        <span class="text-danger">{{$errors->first('title')}}</span>
                                    </div>
                                </div>
                               <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Slug</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter Slug" value="{{old('slug')}}"> 
                                        <span class="text-danger">{{$errors->first('slug')}}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Content</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="editor1" name="content" placeholder="Enter Content" value="{{old('content')}}" >{{old('content')}}</textarea> 
                                        <span class="text-danger">{{$errors->first('content')}}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Meta Title</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Enter Meta Title" value="{{old('meta_title')}}">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Meta Tags</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="meta_tags" name="meta_tags" value="{{old('meta_tags')}}" placeholder="Enter Meta Tags">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 m-t-15">Meta Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="meta_description" name="meta_description" placeholder="Enter Meta Description" value="{{old('meta_description')}}">{{old('meta_description')}}</textarea>
                                        
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <script src="{{url('assets/libs/jquery/dist/jquery.min.js')}}"></script>
           <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
          <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>
          <script>
        $(document).ready(function(){
              CKEDITOR.editorConfig = function (config) {
            config.language = 'es';
            config.uiColor = '#F7B42C';
            config.height = 300;
            config.toolbarCanCollapse = true;

        };
        CKEDITOR.replace('editor1');
            
            /* validate */

        

        $("#exampleValidation").validate({
            validClass: "success",
            rules: {
                title: {
                    required: true
                },
                slug: {
                    required: true
                },
                content: {
                    required: true
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
        });
         
    
    
        
        
        
        });

        

        
        
        
    </script>
@endsection