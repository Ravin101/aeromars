@extends('admin.layouts.admin')
@section('title','Users')
@section('content')

	
	<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Users</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('users')}}">Users</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">All Users</a>
							</li>
						</ul>
					</div>
					<div class="row">
						

						

						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									
										
									</div>
								</div>
								<div class="card-body">
								@if(session('message'))
				<p class="alert alert-success">{{session('message')}}</p>
			@endif
									

									<div class="table-responsive">
                                    <table id="example1" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>CreatedAt</th>
                                                <th>UserType</th>
                                                <th>Profile Pic</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @php $i= $users->toArray()['from'] ?? 1; @endphp
                                            @foreach($users as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{ $row->country_code}} {{$row->phone_number}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{date('d-M-Y',strtotime($row->created_at))}}</td>
                                                <td>{{$row->social_type ?? "SIGNUP"}}</td>
                                                <td><a target="_blank" href="{{url('images/'.$row->profile_pic)}}"><img src="{{url('images/'.$row->profile_pic)}}" width="50" /></a></td>
                                                <td><center><div class="slideparam">
                    <input type_status="users" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox"   {{($row->status==1)?"checked":""}}  />
                    <label for="{{$row->id}}" ></label>
                    </div> </center></td>
                                                <td>

                                    <div class="float-left mr-2"> 
                                      
                                    </div>
                                    <div class="float-left">
                                        <form action="{{ url('/user'.$row->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        
                                        <button type="button" id="{{$row->id}}" class="btn btn-sm btn-danger delete_user" ><i class="fa fa-trash" aria-hidden="true"></i></button>
                                         </form>  
                                        
                                    </div>

                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach
										</tbody>
                                       
                                    </table>
									
                                </div>  
								{{ $users->appends(request()->query())}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>	
		<!-------------------Modal Start----------------->	
			  <div class="container"> 
                        
                        <!-- Trigger the modal with a button --> 
                        
<!-- Modal -->

<div class="modal fade" id="view_modal" role="dialog">
	<div class="modal-dialog modal-lg">		
	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header">
	<h4 class="modal-title">Users Details</h4>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	
	</div>
    <div class="modal-body view_body">
       
    </div>
   <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

</div>
                          </div>
                                </div>
</div>
	<!-------------------Modal End----------------->
	
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
		
		<script>
		$(document).ready(function(){
		$(".delete_user").click(function(e){
    e.preventDefault();
	var id = $(this).attr('id');
	 bootbox.confirm({
		  message:"Are you sure you want to delete this user?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){
						
			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });
		// For view transaction //For view order Details
	$(".view").click(function(e){
		e.preventDefault();
		$("#view_modal").modal();
	  var transaction_id = $(this).attr('id');
	 
	  var url =  $(this).attr('href');
	  
	  $.ajax({
		  url:url,
		  data:{id:transaction_id},
		  type:"get",
		  success:function(data)
		  {
			  $(".view_body").html(data);
		  }
	  })
  });
		});

	</script>
		
@endsection	