@extends('admin.layouts.admin')
@section('content')							


         <div class="main-panel">
			  <div class="content">
				 <div class="page-inner">
					 <div class="page-header">
						<h4 class="page-title">Category</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('category')}}">category</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">edit</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('category')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="All Category">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Category
										</a>	
										
									</div>
								</div>
								<form method="post" action="{{url('category/'.$users->id)}}" id="exampleValidation" class="form-horizontal" enctype="multipart/form-data">
								@csrf
								@method('PUT')
								
								@php $generate_url_thumb = url('public/thumbnail/'.$users->thumb) ;@endphp
									<div class="card-body">
										<div class="form-group form-show-validation row">
											<label for="product_name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Category Name <span class="required-label">*</span></label>
											<div class="col-lg-4 col-md-9 col-sm-8">
												<input type="text" class="form-control" id="name" name="name" placeholder="Enter Category Name" value="{{$users->name}}" />
											</div>
										</div>
										
										
									
										<div class="form-group form-show-validation row">
											<label for="image" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Image <span class="required-label">*</span></label>
											<div class="col-lg-4 ">
												<input type="file" class="form-control form-control-file" name="image" id="image" placeholder="Photo" >
											</div>
											
										</div>
										
										
									
										
										
										
										
										
										
									</div>
									<div class="card-action">
										<div class="row">
											<div class="col-md-12">
											<button type="submit" class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
												<button class="btn btn-danger">Cancel</button>
											</div>										
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<script>
		$(document).ready(function(){
			
			/* validate */

		// validation when select change
		$("#state").change(function(){
			$(this).valid();
		})

		// validation when inputfile change
		$("#uploadImg").on("change", function(){
			$(this).parent('form').validate();
		})

		$("#exampleValidation").validate({
			validClass: "success",
			rules: {
				name: {required: true},
				type: {required: true},
				price: {
					required: true,
					number: true,
				},
				
				// image: {
					// required: true, 
				
				// },
				
				
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
		});
		 function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	}); 
		});

		

		
		
		
	</script>
@endsection					
					
									
									
