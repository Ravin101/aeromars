@extends('admin.layouts.admin')
@section('title','Category Create')
@section('content')


<div class="main-panel">
	<div class="content">
		<div class="page-inner">
			<div class="page-header">
				<h4 class="page-title">Categories</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="{{url('category')}}">Category</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Create</a>
					</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="d-flex align-items-center pull-right">
								<a href="{{url('/admin/category/create')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="Add Category">
									<span class="btn-label">
										<i class="fa fa-plus"></i>
									</span>
									Add Category
								</a>	
								
							</div>
						</div>
						
						
						<div class="card-body">
							<div class="table-responsive">
								<table id="example1" class="display table table-striped table-hover" >
									
									<thead>
										<tr>
											<th><b>Id</b></th>
											<th><b>Category</b></th>
											<th><b>Image</b></th>
											<th><b>Status</b></th>
											<th width="18%"><b>Action</b></th>
										</tr>
									</thead>
									
									<tbody id="tbody">
										
										@php $i=1; @endphp
										@foreach($datas as $row)
										@php $generate_url_thumb = url('public/thumbnail/'.$row->thumbnail) ;@endphp
										
										@php $generate_url = url('public/images/'.$row->image);@endphp
										<tr data-post-id={{$row->id}}>
											<td >{{$i}}</td>
											<td><center>{{$row->name}}</center></td>
											<td><center><a target="_blank" href="{{$generate_url}}"><img src="{{$generate_url_thumb}}" width="50"/></a></center></td>
											<td><center><div class="slideparam">
												<input type_status="category" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox" {{($row->status==1)?"checked":""}}  />
												<label for="{{$row->id}}" ></label>
											</div> </center></td>
											
											<td><center></center><form method="post" id="delete_form_{{$row->id}}" action="{{url('admin/category/'.$row->id)}}"  >
												@method('DELETE')
												@csrf
												<input type="hidden" name="remember_token" id="remember_token" value="{{$row->remember_token}}" />
												<input type="hidden" name="id" id="id" value="{{$row->id}}" />
												
												<button type="button" id="{{$row->id}}" url="{{url('category/'.$row->id)}}" data-toggle="modal" data-target="#view_modal" class="view btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button>  
												
												<a href="{{url('category/'.$row->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
												
												<button type="button" id="{{$row->id}}" class="btn btn-sm btn-danger delete" ><i class="fa fa-trash" aria-hidden="true"></i></button>
												
											</form> </center>
										</td>
									</tr>
									
									<?php $i++; ?>
									@endforeach
								</tbody>
							</table>
							
						</div>	
						
						
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</div>

</div>

<div class="modal fade" id="view_modal" role="dialog">
			<div class="modal-dialog modal-lg"> 
										
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header bg-success" style="color:#fff;">
			 <h4 class="modal-title"><i class="fas fa-info-circle"></i> Category Details</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			   
			</div>
				<div class="modal-body" align="center">
				   
				</div>
				<div class="modal-footer">
					<input data-dismiss="modal" value="Close" class="btn btn-primary" >
				</div>

			</div>
            </div>
		</div>

<script>

	$(document).ready(function(){
		$(".view").click(function(){
			var id = $(this).attr('id');
			
			var url = $(this).attr('url');
			$.ajax({
				url:url,
				data:{id:id},
				type:"get",
				success:function(data)
				{
					$(".modal-body").html(data);
				}
			})
		});
	</script>					 
	@endsection	