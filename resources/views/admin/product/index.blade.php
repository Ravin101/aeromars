@extends('admin.layouts.admin')
@section('title','Product')
@section('content')

	<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Product</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('products')}}">Product</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">List</a>
							</li>
						</ul>
					</div>
					<div class="row">




						<div class="col-md-12">
							<div class="card">
								<div class="card-header">

                                    <form action="{{url()->current()}}" >
                                        <div class="form-group row">
                                           <label class="col-md-1 m-t-15"><b>Search</b></label>
                                           <div class="col-md-3">
                                               <input type="text" class="form-control" id="search" name="search" placeholder="Enter Name,Phone no,Email ..." value="{{$search}}" required="">

                                           </div>
                                           <div class="col-md-1">
                                           <button type="submit" class="btn btn-success">Submit</button>
                                           </div>

                                           <div class="col-md-2">
                                           <a href="{{url()->current()}}" class="btn btn-danger">Reset</a>
                                           </div>
                                           <div class="col-md-5">
                                            <div class="d-flex align-items-center pull-right">
                                                <a href="{{url('product/create')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="Create Product">
                                                       <span class="btn-label">
                                                           <i class="fa fa-plus"></i>
                                                       </span>
                                                       Create Product
                                               </a>

                                               </div>
                                            </div>
                                       </div>
                                   </form>


								</div>
								<div class="card-body">
								@if(session('message'))
                                    <p class="alert alert-success">{{session('message')}}</p>
                                @endif


									<div class="table-responsive">
										<table id="example2" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th>Sr No</th>
													<th>Name</th>
													<th>Price</th>
													<th>Qty</th>
													<th>Thumbnail</th>
													<th>Status</th>
													<th style="width: 18%">><b><center>Action</center></b></th>
												</tr>
											</thead>

											<tbody>
											@php $i=1; @endphp
											@foreach($products as $product)

												<tr>
													<td>{{$i}}</td>
												   <td>{{$product->name}}</td>
												   <td>{{$product->price}}</td>
												   <td>{{$product->qty}}</td>
							<td><center><a target="_blank" href="{{url($product->image)}}"><img src="{{url($product->thumbnail)}}" width="50"/></a></center></td>
												   <td><center><div class="slideparam">
									<input type_status="product" type="checkbox" id="{{$product->id}}" onChange="save_admin_message_settings({{$product->id}})" name="status" value="1"  class="status_{{$product->id}} status_checkbox"   {{($product->status==1)?"checked":""}}  />
									<label for="{{$product->id}}" ></label>
									</div> </center></td>
													<td>
									<form id="delete_form_{{$product->id}}" method="post" action="{{url('admin/product/'.$product->id)}}" >
													@csrf
													@method('DELETE')
								<input type="hidden" name="remember_token" id="remember_token" value="{{$product->remember_token}}" />
									<input type="hidden" name="id" id="id" value="{{$product->id}}" />
                             
					<!-- add images -->
					<a href="{{url('admin/product/'.$product->id.'/addimg')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
							
					<!-- view -->
					<button type="button" id="{{$product->id}}" url="{{url('admin/product/'.$product->id)}}" data-toggle="modal" data-target="#view_modal" class="view btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button>

					<!-- edit -->
					<a href="{{url('admin/product/'.$product->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>

					<!-- delete -->
					<button type="button" id="{{$product->id}}" class="btn btn-sm btn-danger delete" ><i class="fa fa-trash" aria-hidden="true"></i></button>

														</form>
													</td>
												</tr>
												@php $i++; @endphp
												@endforeach

											</tbody>
										</table>
                                    </div>
                                    {{ $products->appends(request()->query())}}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-------------------Modal Start----------------->
			  <div class="container">

                        <!-- Trigger the modal with a button -->

<!-- Modal -->

<div class="modal fade" id="view_modal" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header bg-success" style="color:#fff;">
 <h4 class="modal-title"><i class="fas fa-info-circle"></i> Product Details</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
    <div class="modal-body view_body" align="center">

    </div>
   <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

</div>
                          </div>
                                </div>
</div>
	<!-------------------Modal End----------------->
        @push('scripts')
        <script>
            $(function () {
                 $('#example2').DataTable({
                   'paging'      : true,
                   'lengthChange': true,
                   'searching'   : false,
                   'ordering'    : true,
                   'info'        : true,
                   'autoWidth'   : true,
                   'pageLength'  : 50,
                   "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                   "aoColumns": [
                                   null,
                                   null,
                                   null,
                                   null,
                                     { "bSortable": false },
                                     { "bSortable": false },
                                     { "bSortable": false }
                                 ]

                 });


               });

       </script>
        @endpush


@endsection
