@extends('admin.layouts.admin')
@section('title','Product Create')
@section('content')


	<meta name="csrf-token" content="{{ csrf_token() }}" />
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Product</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('product')}}">Product</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Create</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('product')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="All Product">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Product
										</a>

									</div>
								</div>
								<form method="post" action="{{url('admin/product')}}" id="exampleValidation" class="form-horizontal" enctype="multipart/form-data">
								@csrf

									<div class="card-body">
										<div class="form-group form-show-validation row">
											<label for="category_id" class="col-lg-3 col-md-3  mt-sm-2 ">Category <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<select  class="form-control" id="category_id" name="category_id" placeholder="Category" onchange="show_subcategory()" required  >
												  <option value="">-Select-</option>

													@foreach($categories as $cat_mas)
													<option   value="{{$cat_mas->id}}" {{old('category_id')==$cat_mas->id?"selected":""}}>{{ucfirst($cat_mas->name)}}</option>
													@endforeach
												 </select>
												 <div class="text-danger">{{ $errors->first('category_id') }}</div>
											</div>
											<label for="name" class="col-lg-3  mt-sm-2 ">Subcategory<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<select  class="form-control" id="subcategory_id" name="subcategory_id" placeholder="Subcategory" required >
												  <option value="">-Select-</option>

												 </select>
												  <div class="text-danger">{{ $errors->first('subcategory_id') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="name" class="col-lg-3 col-md-3  mt-sm-2 ">Product Name <span class="required-label">*</span></label>
											<div class="col-lg-9">
												<input type="text" class="form-control" id="name" name="name" placeholder="Product Name" value="{{old('name')}}">
										  <div class="text-danger">{{ $errors->first('name') }}</div>
											</div>

										</div>
										<div class="form-group form-show-validation row">
											<label for="qty" class="col-lg-3 col-md-3  mt-sm-2 ">Product Qty <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="qty" name="qty" placeholder="Product Qty" value="{{old('qty')}}" required />
										    <div class="text-danger">{{ $errors->first('qty') }}</div>
											</div>
											<label for="code" class="col-lg-3  mt-sm-2 ">Product Code<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control" id="code" name="code" placeholder="Product Code" value="{{old('pcode')}}" required />
												<div class="text-danger">{{ $errors->first('code') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="min_qty" class="col-lg-3 col-md-3  mt-sm-2 ">Product Min Qty <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="min_qty" name="min_qty" placeholder="Product Max Qty" value="{{old('min_qty')}}" required />
										  <div class="text-danger">{{ $errors->first('min_qty') }}</div>
											</div>
											<label for="max_qty" class="col-lg-3  mt-sm-2 ">Product Max Qty<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="max_qty" name="max_qty" placeholder="Product Max Qty" value="{{old('max_qty')}}" required />
												<div class="text-danger">{{ $errors->first('max_qty') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="price" class="col-lg-3 col-md-3  mt-sm-2 ">Product Price <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild_discount" id="price" name="price" placeholder="Product Price" value="{{old('price')}}" required />
										  <div class="text-danger">{{ $errors->first('price') }}</div>
											</div>
											<label for="mrp" class="col-lg-3  mt-sm-2 ">Product Mrp<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild_discount" id="mrp" name="mrp" placeholder="Product Mrp" value="{{old('mrp')}}" required />
												<div class="text-danger">{{ $errors->first('mrp') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="unit" class="col-lg-3 col-md-3  mt-sm-2 ">Product Unit <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<select  class="form-control" id="unit" name="unit" placeholder="unit" required >
												  <option value="">-Select-</option>

													@foreach($units as $unit)
													<option   value="{{$unit->id}}" {{old('unit')==$unit->id?"selected":""}} >{{ucfirst($unit->name)}}</option>
													@endforeach
												 </select>
										  <div class="text-danger">{{ $errors->first('price') }}</div>
											</div>
											<label for="weight" class="col-lg-3  mt-sm-2 ">Product Weight<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control " id="weight" name="weight" placeholder="Product Weight" value="{{old('weight')}}" required />
												<div class="text-danger">{{ $errors->first('weight') }}</div>
											</div>
										</div>
										<!--<div class="form-group form-show-validation row">
											<label for="date_of_packing" class="col-lg-3  mt-sm-2 ">Packing Date<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control datepicker" id="date_of_packing" name="date_of_packing" placeholder="Packing Date" value="{{date('m/d/Y',strtotime(old('date_of_packing')))}}" required />
												<div class="text-danger">{{ $errors->first('date_of_packing') }}</div>
											</div>
											<label for="date_of_expire" class="col-lg-3  mt-sm-2 ">Expire Date<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control datepicker" id="date_of_expire" name="date_of_expire" placeholder="Expire Date" value="{{date('m/d/Y',strtotime(old('date_of_expire')))}}"  required />
												<div class="text-danger">{{ $errors->first('date_of_expire') }}</div>
											</div>
										</div>-->
										<div class="form-group form-show-validation row">
											<label for="name" class="col-lg-3 col-md-3  mt-sm-2 ">Product Description <span class="required-label">*</span></label>
											<div class="col-md-9 col-sm-8">
												<textarea type="text"  class="form-control" id="editor1"  name="description" placeholder="Enter Description"  required  >{{old('description')}}</textarea>
												<div class="text-danger">{{ $errors->first('description') }}</div>
											</div>
										</div>
                                        <div class="form-group row">
                                            <label class="col-md-2 m-t-15">Images</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="image" name="image[]" placeholder="Choose image" value="{{old('image')}}" multiple="">

                                            </div>
                                            <div class="col-lg-4">
                                            <img style="display:none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
                                                <span class="text-danger">{{$errors->first('image')}}</span>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 m-t-15">Thumbnail</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="thumbnail" name="thumbnail" placeholder="Choose image" >

                                            </div>
                                            <div class="col-lg-4">
                                            <img style="display:none;"  id="thumbnail_preview"  src="" width="70" class="pull-right" alt="User Image">
                                                <span class="text-danger">{{$errors->first('thumbnail')}}</span>

                                            </div>
                                        </div>

									</div>
									<div class="card-action">
                  <button type="submit" class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

        </div>

        @push('scripts')


		<script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>

		<script>


        function show_subcategory()
        {
            var category_id = $('#category_id').children('option:selected').attr('value')
            var APP_URL = {!! json_encode(url('/')) !!};
            var csrf = '{{ csrf_token() }}';
            $.ajax({
            type: 'POST',
            data: {category_id: category_id, '_token':csrf},
            url: APP_URL+'/admin/get-sub-cat' ,
            success: function(response){
            $('#subcategory_id').empty();
            $('#subcategory_id').append('<option value="">Select Sub Category</option>');
            $.each(response.subcategories,function(index,subcategory){

            $('#subcategory_id').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>');
            })
            }
            });

        }


		$(document).ready(function(){
			  CKEDITOR.editorConfig = function (config) {
			config.language = 'es';
			config.uiColor = '#F7B42C';
			config.height = 300;
			config.toolbarCanCollapse = true;

		};
		CKEDITOR.replace('editor1');
			// $("form").submit(function(){
				// $(this).find('button[type=submit]').prop('disabled', true);
			// })
			// $("form").submit(function(){
				// $(this).find('button[type=submit]').prop('disabled', true);
			// })
			/* validate */






        function readURL(input)
        {
            if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            $('#thumbnail_preview').attr('src', e.target.result);

            $('#thumbnail_preview').hide();
            $('#thumbnail_preview').fadeIn(650);
            }
        reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thumbnail").change(function() {
        readURL(this);
        });


		});






	</script>
    @endpush
@stop
