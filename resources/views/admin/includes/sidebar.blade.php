
<!-- Sidebar -->
<style>
.sidebar-wrapper {
     background: linear-gradient(to left, #ffffff  60%, #abbaab 100%);

;
}
<!--
.nav-item {
	font-family:Lucida Console;
	color: #000000d6;
} -->

.sub-item
{
	 color: #000000d6;
	 font-family:Lucida Console;
}

</style>
		<div class="sidebar">

			<div class="sidebar-background" data-background-color="dark2"></div>
			<div class="sidebar-wrapper scrollbar-inner">
			<div class="sidebar-wrapper scrollbar-inner scroll-content scroll-scrolly_visible" style="height: auto; margin-bottom: 0px; margin-right: 0px; background-color: red; max-height: 308px;" >
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{url('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>

								<span class="user-level">Admin</span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="#profile">
											<span class="link-collapse">My Profile</span>
										</a>
									</li>
									<li>
										<a href="#edit">
											<span class="link-collapse">Edit Profile</span>
										</a>
									</li>
									<li>
										<a href="#settings">
											<span class="link-collapse">Settings</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav">
						<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
							<a href="{{url('admin/dashboard')}}">
								<i class="fas fa-home"></i>
								<p>Dashboard</p>
							</a>
						</li><li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
							<a href="{{url('admin/notification')}}">
								<i class="fas fa-bell"></i>
								<p>Notifications</p>
							</a>
						</li>
						<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
							<a href="{{url('admin/banner')}}">
								<i class="fas fa-image"></i>
								<p>Banner</p>
							</a>
						</li>
						<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
							<a href="{{url('admin/user')}}" class="collapsed">
								<i class="fas fa-user"></i>
								<p>Users</p>
							</a>
						</li>
						<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
							<a data-toggle="collapse" href="#product">

								<i class="fab fa-product-hunt"></i>
								<p>Products</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{request()->segment(1)=='category' ? 'show' :''}} {{request()->segment(1)=='subcategory' ? 'show' :''}} {{request()->segment(1)=='innercategory' ? 'show' :''}} {{request()->segment(1)=='product' ? 'show' :''}}" id="product">
								<ul class="nav nav-collapse">

									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="{{url('admin/category')}}">
											<span class="sub-item">Category</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="{{url('admin/subcategory')}}">
											<span class="sub-item">Subcategory</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="{{url('admin/product')}}">
											<span class="sub-item">Products</span>
										</a>
									</li>
									<!--<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item">Today's Deal</span>
										</a>
									</li> -->
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="{{url('promocode')}}">
											<span class="sub-item">Promo Code</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

				<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
				<a data-toggle="collapse" href="#order">

								<i class="fa fa-shopping-cart"></i>
								<p> Orders</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{request()->segment(1)=='category' ? 'show' :''}} {{request()->segment(1)=='subcategory' ? 'show' :''}} {{request()->segment(1)=='innercategory' ? 'show' :''}} {{request()->segment(1)=='product' ? 'show' :''}}" id="order">
								<ul class="nav nav-collapse">

									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="{{url('admin/order')}}">
											<span class=" sub-item text-primary ">All Orders</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-success">Completed</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-warning">Pending</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-primary">Received</span>
										</a>
									</li>

									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-primary">Shipped</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-secondary">Delivered</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-danger">Canceled</span>
										</a>
									</li>
									<li class= "nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
										<a href="">
											<span class="sub-item text-danger">Failed</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

							<li class="nav-item {{request()->segment(1)=='user' ? 'active' :''}}">
								<a href="">
								<i class="fas fa-award"></i>
								<p>Today's Offers</p>
							</a>
						</li>

					</ul>
				</div>
			</div>
		</div>
		</div>
		<!-- End Sidebar -->
