</div>
</div>

<!--   Core JS Files   -->
<script src="{{url('assets/js/core/jquery.3.2.1.min.js')}}"></script>
<script src="{{url('assets/js/core/popper.min.js')}}"></script>
<script src="{{url('assets/js/core/bootstrap.min.js')}}"></script>

<!-- jQuery UI -->

<script src="{{url('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

<!-- jQuery Scrollbar -->
<script src="{{url('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>

<!-- Moment JS -->
<script src="{{url('assets/js/plugin/moment/moment.min.js')}}"></script>

<!-- Chart JS -->
<script src="{{url('assets/js/plugin/chart.js/chart.min.js')}}"></script>

<!-- jQuery Sparkline -->
<script src="{{url('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Chart Circle -->
<script src="{{url('assets/js/plugin/chart-circle/circles.min.js')}}"></script>

<!-- Datatables -->
<script src="{{url('assets/js/plugin/datatables/datatables.min.js')}}"></script>

<!-- Bootstrap Notify -->
<script src="{{url('assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

<!-- Bootstrap Toggle -->
<script src="{{url('assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>


<!-- Google Maps Plugin -->
<script src="{{url('assets/js/plugin/gmaps/gmaps.js')}}"></script>

<!-- Sweet Alert -->
<script src="{{url('assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

<!-- Azzara JS -->
<script src="{{url('assets/js/ready.min.js')}}"></script>

<!-- Azzara DEMO methods, don't include it in your project! -->
<script src="{{url('assets/js/setting-demo.js')}}"></script>

<!-- Bootbox -->
<script src="{{url('assets/js/plugin/bootbox/bootbox.min.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script>
$(function() {

    $(".alert-success").fadeOut(5000);
    $(".alert-danger").fadeOut(5000);

});
$(function () {
  $('#example1').DataTable({
  'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true

  });


});



	$(document).ready(function(){
		var img_url =  $("#img_url").val();

		// view
		$(".view").click(function(e){
	    e.preventDefault();

		$("#view_modal").modal();
			var id = $(this).attr('id');

			var url = $(this).attr('href');
			$.ajax({
			  url:url,
			  data:{id:id},
			  type:"get",
			  beforeSend: function() {// setting a timeout
					$(".modal-body").html("<img src='"+img_url+"' style='width: 15%;' />");
				  },
			  success:function(data)
			  {
				 // alert(data);
				$(".modal-body").html(data);
			  }
			})
		});

		// Delete
		$(".delete").click(function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		 bootbox.confirm({
		  message:"Are you sure you want to delete this entry?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){

			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });


		$('form').submit(function() {
			//$('#loadingDiv').show();
		});
       $('#exampleValidation').submit(function() {
			//$('#loadingDiv').show();
		});

    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
	// $(".select2").select2();

	// For quantity validation
	 $(".numeric_feild").on("focus",function(event)
	 {
		id=$(this).attr('id');
		var text = document.getElementById(id);
		text.onkeypress = text.onpaste = checkInput;
	 });
	function checkInput(e)
	{
	var e = e || event;
	var char = e.type == 'keypress'
	? String.fromCharCode(e.keyCode || e.which)
	: (e.clipboardData || window.clipboardData).getData('Text');
	if (/[^\d]/gi.test(char)) {
	return false;
	}
	}

	//For discount validation
	$(".numeric_feild_discount").keypress(function(event){
		return isNumber(event, this);
    });

	 function isNumber(evt, element)
	 {
		 var charCode = (evt.which) ? evt.which : event.keyCode
		 if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
				(charCode < 48 || charCode > 57))
				return false;

				return true;
	  }
});
function save_admin_message_settings(id)
	{
		var status = $(".status_"+id+":checked").val();
		if(status!=1)
			status =0;
		var type = $(".status_"+id).attr('type_status');
		//alert(type);
		var _token = $('input[name="_token"]').val();
		$.ajax({
			url:"{{url('update-status')}}",
			type:"post",
			data:{id:id,status:status,_token:_token,type:type},
			success:function(data){
				    var msg= "Status Updated Successfully";
					var title= "Success";
					var type= "success";
					notification_msg(msg,title,type);
			}
		});
	}
	function notification_msg(msg,title,type)
{
          var shortCutFunction = type;//'success'; //$("#toastTypeGroup input:radio:checked").val();
            //alert(shortCutFunction);
            var msg = msg;
            var title = title;
            var $showDuration = 300;
            var $hideDuration = 1000;
            var $timeOut = 5000;
            var $extendedTimeOut = 1000;
            //var toastIndex = toastCount++;

            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;

            if(typeof $toast === 'undefined'){
                return;
            }

}
</script>

@stack('scripts')


</body>
</html>
