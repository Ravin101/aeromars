
<style>
.main-header {

	background:linear-gradient(to left, #108e8e 65%, #ffffff 89%);
	<!--  linear-gradient(to left, #cc1c4d 88%, #8c2b4f 100%);-->

	   <!-- background: linear-gradient(to bottom left, #0066cc 0%, #9966ff 100%);-->
}



</style>
		<div class="main-header" >
			<!-- Logo Header -->
			<div class="logo-header">

				<a href="{{url('dashboard')}}" class="logo">
					<img src="{{url('images/aeromars.jpg')}}" alt="..."  style="width:118px;height:18px; margin-left:15px;">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="fa fa-bars"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="fa fa-ellipsis-v"></i></button>
				<div class="navbar-minimize">
					<button class="btn btn-minimize btn-rounded">
						<i class="fa fa-bars"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg">

				<div class="container-fluid">

					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>


						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{url('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<li>
									<div class="user-box">
										<div class="avatar-lg"><img src="{{url('assets/img/profile.jpg')}}" alt="image profile" class="avatar-img rounded"></div>
										<div class="u-text">
											<h4></h4>
											<p class="text-muted"></p><a href="{{url('admin/profile')}}" class="btn btn-rounded btn-secondary btn-sm">View Profile</a>
										</div>
									</div>
								</li>
								<li>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{url('admin/profile')}}">My Profile</a>


									<div class="dropdown-divider"></div>
									<a href="{{url('changepassword')}}" class="dropdown-item">Change Password</a>

									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{url('admin/logout')}}">Logout</a>
								</li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
