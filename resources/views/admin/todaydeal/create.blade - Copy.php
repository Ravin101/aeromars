@extends('layouts.admin')
@section('title','product')
@section('content')

<style>
.scrollbox {
  
  overflow-x:scroll;
}
</style>

 


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Product 
        <small>
        Add product Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li class="active">Add Product tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         

          <div class="box box-primary">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			
		
			
			
			
			
 

     <div class="col-md-12">

			
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('product')}}" >
			@csrf
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Product Title</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="title" name="title" placeholder="Product Title" value="{{old('title')}}">
					  <div class="text-danger">{{ $errors->first('title') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Main Category</label>

                    <div class="col-sm-4">
                      <select  class="form-control" id="main_category" name="main_category" placeholder="Email" >
                      <option value="">-Select-</option>
                       
                        @foreach($categoryMaster as $cat_mas)
                        <option   value="{{$cat_mas->id}}">{{ucfirst($cat_mas->category_name)}}</option>
                        @endforeach
                      </select>
					  <div class="text-danger">{{ $errors->first('main_category') }}</div>
                    </div>
                    <label for="inputEmail" class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-4">
					<div id="category" class="scrollbox" >
                    <!--  <select  class="form-control" id="category" name="category" placeholder="Email">
                        <option value="">-Select-</option>
                      </select>-->
					  
					  
					  <div class="text-danger">{{ $errors->first('category') }}</div>
                    </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Page URL [SEO]</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="titleurl" name="titleurl" placeholder="Page URL" value="{{old('titleurl')}}">
					   <div class="text-danger">{{ $errors->first('titleurl') }}</div>
                    </div>
                    <label for="inputExperience" class="col-sm-2 control-label">Product Code</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="pcode" name="pcode" placeholder="Product Code" value="{{old('pcode')}}" >
					  <div class="text-danger">{{ $errors->first('pcode') }}</div>
                    </div>
                 
                  </div>
                  
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Packing</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="packing" name="packing"  placeholder="Packing" value="{{old('packing')}}">
					  <div class="text-danger">{{ $errors->first('packing') }}</div>
                    </div>
                    <label for="inputSkills" class="col-sm-2 control-label">Scheme</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="scheme" name="scheme" placeholder="Scheme" value="{{old('scheme')}}">
					  <div class="text-danger">{{ $errors->first('scheme') }}</div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">MRP</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control numeric_feild" id="mrprice" name="mrprice" placeholder="MRP" value="{{old('mrprice')}}">
					  <div class="text-danger">{{ $errors->first('mrprice') }}</div>
                    </div>
					
					
                    <label for="inputSkills" class="col-sm-2 control-label">Product Min Qty</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control numeric_feild_phone" id="product_min_qty" name="product_min_qty" placeholder="Product Min Qty" value="{{old('product_min_qty')}}">
					    <div class="text-danger">{{ $errors->first('product_min_qty') }}</div>
                    </div>
					
				
                  </div>
                  
                  <div class="form-group">
					<label for="inputSkills" class="col-sm-2 control-label">Price to Retailer</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control numeric_feild" id="price" name="price" placeholder="Price to Retailer" value="{{old('price')}}">
					    <div class="text-danger">{{ $errors->first('price') }}</div>
                    </div>	
					
					<label for="inputSkills" class="col-sm-2 control-label">Product Qty</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control numeric_feild_phone" id="qty" name="qty" value="{{old('qty')}}" placeholder="Product Qty">
					  <div class="text-danger">{{ $errors->first('qty') }}</div>
                    </div>
					
                  </div>
                  
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Title Tag [SEO]</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="titletag" name="titletag" placeholder="Title Tag [SEO]" value="{{old('titletag')}}">
					  <div class="text-danger">{{ $errors->first('titletag') }}</div>
                    </div>
					<label for="inputSkills" class="col-sm-2 control-label">Meta Tag Keywords [SEO]</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="metakeyword" name="metakeyword" placeholder="Meta Tag Keywords [SEO]">
					  <div class="text-danger" value="{{old('metakeyword')}}">{{ $errors->first('metakeyword') }}</div>
                    </div>
					
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Meta Tag Description [SEO]</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="metadescription" name="metadescription" placeholder="Skills" value="{{old('metadescription')}}">
					   <div class="text-danger">{{ $errors->first('metadescription') }}</div>
                    </div>
               
                    <label for="inputSkills" class="col-sm-2 control-label">GST</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="gst" name="gst" placeholder="Skills" value="{{old('gst')}}">
					   <div class="text-danger">{{ $errors->first('gst') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Product Tags [For Inner Search]</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="searchkeyword" name="searchkeyword" placeholder="Product Tags [For Inner Search]" value="{{old('searchkeyword')}}">
					  <div class="text-danger">{{ $errors->first('searchkeyword') }}</div>
                    </div>
					<label for="inputSkills" class="col-sm-2 control-label">Discount</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="discount" name="discount" placeholder="Discount" value="{{old('discount')}}">
					  <div class="text-danger">{{ $errors->first('discount') }}</div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Exp Date</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control datepicker" id="datepicker" name="expiry" placeholder="Exp Date" value="{{old('expiry')}}">
					  <div class="text-danger">{{ $errors->first('expiry') }}</div>
                    </div>
					<label for="inputSkills" class="col-sm-2 control-label">Composition</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="composition" name="composition" placeholder="Composition" value="{{old('composition')}}">
					  <div class="text-danger">{{ $errors->first('composition') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Product Type</label>

                    <div class="col-sm-10">
                      <select  class="form-control" id="product_type" name="product_type" placeholder="Email">
                      <option value="0">-Select-</option>
                      <option value="1">Veg</option>
                      <option value="2">Nonveg</option>
					  </select>
					    <div class="text-danger">{{ $errors->first('product_type') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Description</label>

                    <div class="col-sm-10">
                  <textarea name="description" id="editor1" rows="10" cols="80">{{old('description')}}</textarea>
				    <div class="text-danger">{{ $errors->first('description') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
				  	<label for="inputSkills" class="col-sm-2 control-label">New Arrival</label>
				  <div class="col-md-4">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1" name="arrival" >
                       Do you want to add New Arrival in [ Front ]
					    <div class="text-danger">{{ $errors->first('arrival') }}</div>
                      </label>
                    </div>
                    
					<label style="" for="inputSkills" class="col-sm-2 control-label">Featured</label>

                    <div class="col-sm-4">
					<label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="1" name="featured"  >
                      Do you want to add Featured in [ Front ]
					  <div class="text-danger">{{ $errors->first('featured') }}</div>
                      </label>
                      
                    </div>
					
                  </div>
                  
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Sort</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="orderId" name="orderId" placeholder="Skills">
					  <div class="text-danger">{{ $errors->first('orderId') }}</div>
                    </div>
					<label for="inputSkills" class="col-sm-2 control-label">Status</label>

                    <div class="col-sm-4">
                      <select  class="form-control" id="status" name="status" placeholder="Email">
                      <option value="">-Select-</option>
                      <option  value="1">Active</option>
                      <option  value="0">Inactive</option>
					  </select>
					  <div class="text-danger">{{ $errors->first('status') }}</div>
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Product Thumb</label>

                    <div class="col-sm-10">
					<input  type="file" class="form-control" name="thumb" placeholder="Product Images" >
 <div class="text-danger">{{ $errors->first('thumb') }}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Product Images</label>

                    <div class="col-sm-10">
					<input  type="file" class="form-control" name="image[]" placeholder="Product Images" multiple>
 <div class="text-danger">{{ $errors->first('image') }}</div>
                    </div>
                  </div>
				  
	
				  
				 <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>	 
                </div>

 
  
  

		
			
           
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>

  <script>
  $(document).ready(function(){
	  CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('editor1');

 //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
	  format: 'yyyy-mm-dd',
	 startDate: 'd'
    })
 // $(function () {
            // $('.datepicker').datetimepicker({
                // format: 'YYYY-MM-DD'
            // });
        // });
    $("#main_category").change(function(){
        var id = $("#main_category").find(":selected").val();
       // alert(id);
        $.ajax({
            url:"{{url('get-category-list')}}",
            type:"get",
            data:{id:id},
            success:function(res){
            if(res)
			{
			$("#category").empty();
			$("#category").css("height","30px");
			 $.each(res,function(key,value){
				 $("#category").css("height","100px");
				 console.log("key"+value);
                    $("#category").append('<p style="margin:0px; padding:0px"><input type="checkbox" value="'+value.id+'" name="category[]">&nbsp;'+value.category_name+'</p>	');
				
                });
			}
			else
			{
				$("#category").empty();
			}
            }
        })
    });
  });
  </script>
  @endsection