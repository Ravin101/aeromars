@extends('layouts.admin')
@section('title','Today Deal Edit')
@section('content')

		
		
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Today Deal</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('todaydeal')}}">Today Deal</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Edit</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('todaydeal')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="All Today Deal">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Today Deal
										</a>	
										
									</div>
								</div>
								<form method="post" action="{{url('todaydeal/'.$todaydeal->id)}}" id="exampleValidation" class="form-horizontal" enctype="multipart/form-data">
								@csrf
								@method("PUT")
								
									<div class="card-body">
										
										<div class="form-group form-show-validation row">
											<label for="name" class="col-lg-3 col-md-3  mt-sm-2 ">Product Name <span class="required-label">*</span></label>
											<div class="col-lg-9">
												<input type="text" class="form-control" id="name" name="name" placeholder="Product Name" value="{{$todaydeal->name}}">
										  <div class="text-danger">{{ $errors->first('name') }}</div>
											</div>
											
										</div>
										<div class="form-group form-show-validation row">
											<label for="qty" class="col-lg-3 col-md-3  mt-sm-2 ">Product Qty <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="qty" name="qty" placeholder="Product Qty" value="{{$todaydeal->qty}}" required />
										  <div class="text-danger">{{ $errors->first('qty') }}</div>
											</div>
											<label for="code" class="col-lg-3  mt-sm-2 ">Product Code<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control" id="code" name="code" placeholder="Product Code" value="{{$todaydeal->code}}" required />
												<div class="text-danger">{{ $errors->first('code') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="min_qty" class="col-lg-3 col-md-3  mt-sm-2 ">Product Min Qty <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="min_qty" name="min_qty" placeholder="Product Max Qty" value="{{$todaydeal->min_qty}}" required />
										  <div class="text-danger">{{ $errors->first('min_qty') }}</div>
											</div>
											<label for="max_qty" class="col-lg-3  mt-sm-2 ">Product Max Qty<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild" id="max_qty" name="max_qty" placeholder="Product Max Qty" value="{{$todaydeal->max_qty}}" required />
												<div class="text-danger">{{ $errors->first('max_qty') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="price" class="col-lg-3 col-md-3  mt-sm-2 ">Product Price <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild_discount" id="price" name="price" placeholder="Product Price" value="{{$todaydeal->price}}" required />
										  <div class="text-danger">{{ $errors->first('price') }}</div>
											</div>
											<label for="mrp" class="col-lg-3  mt-sm-2 ">Product Mrp<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control numeric_feild_discount" id="mrp" name="mrp" placeholder="Product Mrp" value="{{$todaydeal->mrp}}" required />
												<div class="text-danger">{{ $errors->first('mrp') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
											<label for="unit" class="col-lg-3 col-md-3  mt-sm-2 ">Product Unit <span class="required-label">*</span></label>
											<div class="col-lg-3">
												<select  class="form-control" id="unit" name="unit" placeholder="unit" required >
												  <option value="">-Select-</option>
												   
													@foreach($units as $unit)
													<option   value="{{$unit->id}}" {{$todaydeal->unit==$unit->id?"selected":""}}>{{ucfirst($unit->name)}}</option>
													@endforeach
												 </select>
										  <div class="text-danger">{{ $errors->first('price') }}</div>
											</div>
											<label for="weight" class="col-lg-3  mt-sm-2 ">Product Weight<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control " id="weight" name="weight" placeholder="Product Weight" value="{{$todaydeal->weight}}" required />
												<div class="text-danger">{{ $errors->first('weight') }}</div>
											</div>
										</div>
										<!--<div class="form-group form-show-validation row">
											<label for="date_of_packing" class="col-lg-3  mt-sm-2 ">Packing Date<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control datepicker" id="date_of_packing" name="date_of_packing" placeholder="Packing Date" value="{{date('m/d/Y',strtotime($todaydeal->date_of_packing))}}" required />
												<div class="text-danger">{{ $errors->first('date_of_packing') }}</div>
											</div>
											<label for="date_of_expire" class="col-lg-3  mt-sm-2 ">Expire Date<span class="required-label">*</span></label>
											<div class="col-lg-3">
												<input type="text" class="form-control datepicker" id="date_of_expire" name="date_of_expire" placeholder="Expire Date" value="{{date('m/d/Y',strtotime($todaydeal->date_of_expire))}}"  required />
												<div class="text-danger">{{ $errors->first('date_of_expire') }}</div>
											</div>
										</div>-->
										<div class="form-group form-show-validation row">
											<label for="name" class="col-lg-3 col-md-3  mt-sm-2 ">Product Description <span class="required-label">*</span></label>
											<div class="col-md-9 col-sm-8">
												<textarea type="text"  class="form-control" id="editor1"  name="description" placeholder="Enter Description"  required  >{{$todaydeal->description}}</textarea>
												<div class="text-danger">{{ $errors->first('description') }}</div>
											</div>
										</div>
										<div class="form-group form-show-validation row">
												<label for="image" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Image<span class="required-label">*</span></label>
												<div class="col-lg-4 ">
													<input type="file" class="form-control form-control-file" name="image" id="image" placeholder="Photo" >
												</div>
												<div class="col-lg-4">
												<img   id="image_preview"  src="{{url('public/thumbnail/'.$todaydeal->thumb)}}" width="70" class="pull-right" alt="User Image">
												</div>
										</div>
									 
									</div>
									<div class="card-action">
                  <button type="submit" class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>
		<script>
				$(function() {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  });
});
		function show_subcategory()
		{
		var category_master_id = $('#category_master_id').children('option:selected').attr('value')
		
		$.ajax({
			
			headers:{
		
			'X-CSRF-TOKEN':$('#csrf_token').attr('content')
			},
			type:'get',
			url:"{{url('ajax')}}",
			data:{category_master_id:category_master_id},
			success:function(data){
		
					$('#subcategory_id').html(data);
					}
				});
		
		}
		$(document).ready(function(){
			  CKEDITOR.editorConfig = function (config) {
			config.language = 'es';
			config.uiColor = '#F7B42C';
			config.height = 300;
			config.toolbarCanCollapse = true;

		};
		CKEDITOR.replace('editor1');
			// $("form").submit(function(){
				// $(this).find('button[type=submit]').prop('disabled', true);
			// })
			// $("form").submit(function(){
				// $(this).find('button[type=submit]').prop('disabled', true);
			// })
			/* validate */

		

		

		$("#exampleValidation").validate({
			validClass: "success",
			rules: {
				name: {required: true},
				type: {required: true},
				price: {
					required: true,
					number: true,
				},
				
				
				
				
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
		});
		 function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	}); 
		});

		

		
		
		
	</script>

@stop