@extends('layouts.admin')
@section('title','Products Quantities')
@section('content')



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Product Quantities
        <small>
		@if($category)
		{{ucfirst($category)}}
		@else
			Product Quantities
		@endif
		</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('product')}}">Product</a></li>
        <li class="active">@if($category)
		{{ucfirst($category)}}
		@else
			Product Details
		@endif</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         @if(session('message'))
         <p class="alert alert-success">{{session('message')}}</p>
         @endif
          <div class="box">

            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            <a href="{{url('product/')}}" class="btn btn-primary pull-right">All Products</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('product-quantity-update')}}" >
			@csrf
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Product Name</th>
                  <th>Product Quantity</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                @foreach($products as $product)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{$product->title}}</td>
                  <td><input type="text" class="form-control input-sm" name="qty[{{$product->id}}]" value="{{$product->qty}}" />
				  <div class="text-danger"> 
				  {{ $errors->first('qty.'.$product->id) }} </div>
				  </td>
                  
				   
                 
                </tr>
                 <?php $i++; ?>
               @endforeach
              <tr>
					<td colspan="3"> 
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
				  </td></tr>
                </tbody>
                
              </table>
	  </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
$(document).ready(function(){
  $(".view").click(function(){
	  var id = $(this).attr('id');
	  //alert(id);
	  //var url = "/product/"+id+"'";
	  var url =  $(this).attr('url');
	  
	  $.ajax({
		  url:url,
		  data:{id:id},
		  type:"get",
		  success:function(data)
		  {
			  $(".modal-body").html(data);
		  }
	  })
  });
  $(".delete_product").click(function(e){
    e.preventDefault();
	var id = $(this).attr('id');
	 bootbox.confirm({
		  message:"Are you sure you want to delete this product?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){
						
			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });

 
 


 

});
</script>
  @endsection