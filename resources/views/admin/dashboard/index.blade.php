@extends('admin.layouts.admin')
@section('title','Dashboard')
@section('content')

  <div class="main-panel">
			<div class="content"> 
				<div class="page-inner">
					@if(session('message'))
         <p class="alert alert-success">{{session('message')}}</p>
         @endif
			<div class="row">
						<div class="col-md-4">
							<div class="card card-dark bg-success-gradient">
								<div class="card-body pb-0">
									<div class="h1 fw-bold float-right"></div>
									<h2 style="color:#fff;" class="mb-2">{{$user_count}}</h2>
									<p><a style="color:#fff;" href="{{url('user')}}">Users</a><p>
									<div class="pull-in sparkline-fix chart-as-background">
										
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card card-dark bg-warning-gradient">
								<div class="card-body pb-0">
									<div class="h1 fw-bold float-right"></div>
									<h2 style="color:#fff;" class="mb-2">{{$user_count}}</h2>
									<p><a style="color:#fff;" href="{{url('lottery')}}">Orders</a><p>
									<div class="pull-in sparkline-fix chart-as-background">
										
									</div>
								</div>
							</div>
						</div>
						
						
					</div>
					<div class="row">
						
						<div class="col-md-12">
						
							<div class="table-responsive">
                                    <table id="example1" class="display table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>CreatedAt</th>
                                                <th>UserType</th>
                                                <th>Profile Pic</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @php $i=1; @endphp
                                            @foreach($users as $row)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$row->first_name}}</td>
                                                <td>{{ $row->country_code}} {{$row->phone_number}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{date('d-M-Y',strtotime($row->created_at))}}</td>
                                                <td>{{$row->social_type ?? "SIGNUP"}}</td>
                                                <td><a target="_blank" href="{{url('images/'.$row->profile_pic)}}"><img src="{{url('images/'.$row->profile_pic)}}" width="50" /></a></td>
                                                <td><center><div class="slideparam">
                    <input type_status="users" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox"   {{($row->status==1)?"checked":""}}  />
                    <label for="{{$row->id}}" ></label>
                    </div> </center></td>
                                                <td>

                                    <div class="float-left mr-2"> 
                                      
                                    </div>
                                    <div class="float-left">
                                        <form action="{{ url('/user'.$row->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        
                                        <button type="button" id="{{$row->id}}" class="btn btn-sm btn-danger delete_user" ><i class="fa fa-trash" aria-hidden="true"></i></button>
                                         </form>  
                                        
                                    </div>

                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                            @endforeach
										</tbody>
                                       
                                    </table>
									
                                </div> 
						</div>
						
					</div>
					<div class="row">
						
						
						
					</div>
					
					
					
					
				</div>
			</div>
			
		</div>
		<!-------------------Modal Start----------------->	
			  <div class="container"> 
                        
                        <!-- Trigger the modal with a button --> 
                        
<!-- Modal -->
<div class="modal fade" id="view_modal" role="dialog">
<div class="modal-dialog modal-lg"> 
                            
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
	<h4 class="modal-title">Product Plans Details</h4>
	<button type="button" class="close" data-dismiss="modal">×</button>
	
	</div>
    <div class="modal-body view_body">
       
    </div>
   <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

</div>
                          </div>
                                </div>
</div>
	<!-------------------Modal End----------------->
		
		      
		    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
			
			
			 <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
             <script type="text/javascript" src="./javascript.js"></script>
          <script
             src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCJnj2nWoM86eU8Bq2G4lSNz3udIkZT4YY&sensor=false">
         </script>
    <script>


        //var socket = io.connect('http://127.0.0.1:8890');
	
</script>
	<script>
		$(document).ready(function(){
			 $('.lottery_list').DataTable({
	'paging'      : false,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
 
  });
		});

	</script>

@endsection