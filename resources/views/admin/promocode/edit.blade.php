@extends('admin.layouts.admin')
@section('title','Promo Code Edit')
@section('content')

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Promo Code</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/admin/dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{url('/admin/promocode')}}">Promo Code</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    
                   <div class="col-md-12">
                       
                    
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{url('/admin/promocode/'.$promoCode->id)}}" method="post" id="exampleValidation">
                    @csrf
                    @method("PUT")
                   <div class="card">
                    
                    
                            <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-md-2 m-t-15"></label>
                                    <div class="col-md-10">
                                         <a href="{{url('/admin/promocode')}}" class="btn btn-info pull-right btn-sm" data-toggle="tooltip" title="All Promo Code" style="float:right;">
                                            <span class="btn-label">
                                                <i class="fa fa-list"></i>
                                            </span>
                                            All Promo Code
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-4 text-right">Code</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Enter Code" value="{{$promoCode->code}}">
                                        <span class="text-danger">{{$errors->first('code')}}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 text-right">Type</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="type" name="type" >
                                            <option value="">Select type</option>
                                            <option value="flat" {{$promoCode->type=='flat' ? 'selected':''}}>Flat</option>
                                            <option value="percentage" {{$promoCode->type=='percentage' ? 'selected':''}}>Percentage</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
                               <div class="form-group row">
                                    <label class="col-lg-4 text-right">Discount</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="discount" name="discount" placeholder="Enter Discount" value="{{$promoCode->discount}}">
                                        <span class="text-danger">{{$errors->first('discount')}}</span>
                                    </div>
                                </div>
                                </div>
								 <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{url('/admin/promocode')}}" class="btn btn-danger resetBtn">Cancel</a>
                                </div>
                            </div>
                            
                        </div>
                       </form>
                        </div>
                    
                   
                </div>
                <!-- ============================================================== -->
             
                
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
         
@endsection