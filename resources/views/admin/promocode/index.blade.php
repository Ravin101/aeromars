@extends('admin.layouts.admin')
@section('title','Promo Code')
@section('content')
		
		
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Promo Code</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('promocode')}}">Promo Code</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">List</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									@if(session('message'))
										<div class="alert alert-success">{{session('message')}}</div>
									@endif
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('promocode/create')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="Add Promo Code">
											<span class="btn-label">
												<i class="fa fa-plus"></i>
											</span>
											Add Promo Code
									 </a>	
										
									</div>
								</div>
								<div class="card-body">
								<div class="table-responsive">
								<table  id="example1" class="display table table-striped table-hover" >

								<thead>
								<tr>
								<th width="10%">Id</th>
								<th width="20%">Promo Code</th>
								<th>Type</th>
								<th width="18%">Discount </th>
								 <th>Created</th>
								<th width="14%">Action</th>
								</tr>
								</thead>

								<tbody>
								@php $i=1;@endphp
								@foreach($promocodes as $row)
								<tr>
								<td>{{$i}}</td>
								<td>{{($row->code)}}</td>
								<td>{{ucfirst($row->type)}}</td>
								<td>{{($row->discount)}}</td>
								<td>{{date('d-M-Y',strtotime($row->created_at))}}</td>
								<td>
									<form method="post" id="delete_form_{{$row->id}}" action="{{url('promocode/'.$row->id)}}">
									@method('DELETE')
									@csrf
        
									<input type="hidden" name="id" id="id" value="{{$row->id}}" />
								
									<a href="{{url('promocode/'.$row->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <button type="submit" id="{{$row->id}}" class="btn btn-sm btn-danger delete" ><i class="fa fa-trash" aria-hidden="true"></i></button></center>
									</form>
								</td>
								</tr>
								@php $i++;@endphp
								@endforeach
								</tbody>
								
								</table>
								</div>
								</div>

								
							</div>
						</div>
					</div>
				</div>
			</div>
			<meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
			<input type="hidden" id="url" name="url" value="{{ url('promocode') }}" />

		</div>
		<!--modal start -->
	<div class="modal fade" id="view_modal" role="dialog">
<div class="modal-dialog modal-lg"> 
                            
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
   <h4 class="modal-title">Views Details</h4> 
<button type="button" class="close" data-dismiss="modal">&times;</button>
    
</div>
    <div class="modal-body">
       
    </div>
    <div class="modal-footer">
        <input data-dismiss="modal" value="Close" class="btn btn-primary" >
    </div>

</div>
</div>

</div>



@endsection	