@extends('admin.layouts.admin')
@section('title','Promo Code')
@section('content')
		
		
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Promo Code</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('promocode')}}">Promo Code</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Create</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('promocode')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="All Promo Code">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Promo Code
									 </a>	
										
									</div>
								</div>
								<form method="post" action="{{url('promocode')}}" id="exampleValidation" >
								@csrf
									<div class="card-body">
										
										
										<div class="form-group form-show-validation row">
											<label for="code" class="col-lg-4 text-right ">Promo Code <span class="required-label">*</span></label>
											<div class="col-lg-6">
												<input type="text"  class="form-control" id="code" name="code" placeholder="Enter Promo Code" required />
												<div class="text-danger code">{{ $errors->first('code') }}</div>
											</div>
										</div>
										 <div class="form-group row">
                                    <label class="col-lg-4 text-right">Type</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="type" name="type" >
                                            <option value="">Select type</option>
                                            <option value="flat" {{old('type')=='flat' ? 'selected':''}}>Flat</option>
                                            <option value="percentage" {{old('type')=='percentage' ? 'selected':''}}>Percentage</option>
                                        </select>
                                        <span class="text-danger">{{$errors->first('type')}}</span>
                                    </div>
                                </div>
										<div class="form-group form-show-validation row">
											<label for="discount" class="col-lg-4 text-right">Discount (%) <span class="required-label">*</span></label>
											<div class="col-lg-6">
												<input type="text"  class="form-control numeric_feild_discount" id="discount" name="discount" placeholder="Enter Discount" required />
												<div class="text-danger">{{ $errors->first('discount') }}</div>
											</div>
										</div>
									</div>
									<div class="card-action">
										<div class="row">
											<div class="col-md-12">
												<input class="btn btn-success" type="submit" value="Submit">
												<button class="btn btn-danger">Cancel</button>
											</div>										
										</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}" content="{{ csrf_token() }}" />
			<input type="hidden" id="url" name="url" value="{{ url('promocode') }}" />

		</div>
	

@endsection	