@extends('admin.layouts.admin')
@section('title','Subcategory Create')
@section('content')

<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Subcategory</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('subcategory')}}">Subcategory</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Create</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('admin/subcategory/create')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="Add Subcategory">
											<span class="btn-label">
												<i class="fa fa-plus"></i>
											</span>
											Add Subcategory
										</a>	
										
									</div>
								</div>
								
								
									<div class="card-body">
									<div class="table-responsive">
									<table id="example1" class="display table table-striped table-hover" >

									<thead>
									<tr>
									<th><b><center>Id</center></b></th>
									<th><b><center>Subcategory</center></b></th>
									<th><b><center>Category</center></b></th>
									<th><b><center>Image</center></b></th>
									<th><b><center>Status</center></b></th>
									<th style="width: 18%">><b><center>Action</center></b></th>
									</tr>
									</thead>

									<tbody id="tbody">

									@php $i=1; @endphp
									@foreach($datas as $row)
									@php $generate_url_thumb = url('public/thumbnail/'.$row->thumbnail) ;@endphp
									@php $generate_url = url('public/images/'.$row->image);@endphp
									
									<tr data-post-id={{$row->id}}>
								    <td><center>{{$i}}</center></td>
								    <td><center>{{$row->name}}</center></td>
								    <td><center>{{$row->category->name}}</center></td>
								    <td><a target="_blank" href="{{$generate_url}}"><img src="{{$generate_url_thumb}}" width="50" /></a></td>
								    <td><center><div class="form-group">
								    <div class="slideparam">
									<input type_status="subcategory" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox"   {{($row->status==1)?"checked":""}}  />
									<label for="{{$row->id}}" ></label>
									</div>  
								  
								   </div></center></td>

						               <td>
								   <form method="post" id="delete_form_{{$row->id}}" action="{{url('admin/subcategory/'.$row->id)}}"  >
									@method('DELETE')
									@csrf
									<input type="hidden" name="remember_token" id="remember_token" value="{{$row->remember_token}}" />
									<input type="hidden" name="id" id="id" value="{{$row->id}}" />
									<button type="button" id="{{$row->id}}" url="{{url('subcategory/'.$row->id)}}" data-toggle="modal" data-target="#view_modal" class="view btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button>   
									<a href="{{url('subcategory/'.$row->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
						

									<button type="button" id="{{$row->id}}" class="btn btn-sm btn-danger delete" ><i class="fa fa-trash" aria-hidden="true"></i></button>
									
								<!--	<button id="{{$row->id}}" type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger delete" data-original-title="Delete">
											<i class="fa fa-times"></i>
															</button> -->
										
									</form> 
									</td>
									</tr>

									 <?php $i++; ?>
													@endforeach
									</tbody>
									</table>

									</div>	
										
									</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="modal fade" id="view_modal" role="dialog">
			<div class="modal-dialog modal-lg"> 
										
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header bg-success" style="color:#fff;">
			 <h4 class="modal-title"><i class="fas fa-info-circle"></i> Subcategory Details</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			   
			</div>
				<div class="modal-body" align="center">
				   
				</div>
				<div class="modal-footer">
					<input data-dismiss="modal" value="Close" class="btn btn-primary" >
				</div>

			</div>
            </div>
		</div>
		<script>

/////////////REPLACE TR VALUE
// $( "#tbody" ).sortable({
// update  : function(event, ui)
// {
// var post_order_ids = new Array();
// $('#tbody tr').each(function(){
// post_order_ids.push($(this).data("post-id"));
// });
// $.ajaxSetup({
    // headers: {
        // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    // }
// });
// $.ajax({
// url:"{{url('update-order')}}",
// method:"POST",
// data:{post_order_ids:post_order_ids},
// success:function(data)
// {
// if(data){
// $(".alert-danger").hide();
// $(".alert-success ").show();
// }else{
// $(".alert-success").hide();
// $(".alert-danger").show();
// }
// }
// });
// }
// });
    
 // <script>
   // $(document).ready(function(){
  // $(".delete_product").click(function(e){
    // e.preventDefault();
	// var id = $(this).attr('id');
	 // bootbox.confirm({
		  // message:"Are you sure you want to delete this City",
		  // buttons:{ cancel: {
            // label: '<i class="fa fa-times"></i> Cancel'
			// },
			// confirm: {
				// label: '<i class="fa fa-check"></i> Confirm'
			// },
			  // },
		    // callback: function (result) {
				// if(result){
						
			  // $('#delete_form_'+id).submit();
				// }
			// }
		  // })//confirm
 // });
 // });
  // </script>


</script>

@endsection