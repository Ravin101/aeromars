@extends('admin.layouts.admin')
@section('title','Subcategory Edit')
@section('content')

		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Subcategory</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('subcategory')}}">Subcategory</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Edit</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('subcategory')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="All Subcategory">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Subcategory
										</a>	
										
									</div>
								</div>
					<form method="post" action="{{url('subcategory/'.$subcategory->id)}}" id="exampleValidation" class="form-horizontal" enctype="multipart/form-data">
						@csrf
						@method("PUT")
						@php $generate_url_thumb = url('public/thumbnail/'.$subcategory->thumb) ;@endphp
						<div class="card-body">
							<div class="form-group form-show-validation row">
							<label for="category_id" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Category <span class="required-label">*</span></label>
							<div class="col-lg-4 col-md-9 col-sm-8">
						  <select class="form-control" name="category_id"  onchange="names()" id="category_id" >
                 
					<option value="" >-Select Category-</option>
													@foreach($categories as $row)
		      <option value="{{$row->id}}" {{($subcategory->category_id==$row->id)?"selected":""}} value="{{$row->id}}">{{$row->name}}</option>
													@endforeach
											      </select>
											</div>
										</div>
					<div class="form-group form-show-validation row">
						<label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Subcategory Name <span class="required-label">*</span></label>
						<div class="col-lg-4 col-md-9 col-sm-8">
					 <input type="text" class="form-control input-sm"  name="name" id="name" placeholder="Name" value="{{$subcategory->name}}">
					</div>
											
					</div>
							<div class="form-group form-show-validation row">
											<label for="image" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Image<span class="required-label">*</span></label>
											<div class="col-lg-4 ">
												<input type="file" class="form-control form-control-file" name="image" id="image" placeholder="Photo" value="{{$subcategory->image}}" >
											</div>
											<div class="col-lg-4">
											<img id="image_preview"  src="{{$generate_url_thumb}}" width="140" class="pull-right" alt="User Image">
											</div>
											
											
										</div>
										
										
									
										
										
										
										
										
										
									</div>
									<div class="card-action">
                  <button type="submit" class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		

@endsection