@extends('admin.layouts.admin')
@section('title','Subcategory Create')
@section('content')

		
		
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Subcategory</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url('subcategory')}}">Subcategory</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Create</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('subcategory')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="All Subcategory">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Subcategory
										</a>	
										
									</div>
								</div>
								<form method="post" action="{{url('admin/subcategory')}}" id="exampleValidation" class="form-horizontal" enctype="multipart/form-data">
								@csrf
								
									<div class="card-body">
										<div class="form-group form-show-validation row">
											<label for="category_master_id" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Category  <span class="required-label">*</span></label>
											<div class="col-lg-4 col-md-9 col-sm-8">
												 <select class="form-control" name="category_id" id="category_id" >
                 
												   <option value="" >-Select-</option>
													@foreach($datas as $row)
													<option value="{{$row->id}}">{{$row->name}}</option>
													@endforeach
											      </select>
											</div>
										</div>
									<div class="form-group form-show-validation row">
											<label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Subcategory Name <span class="required-label">*</span></label>
									 <div class="col-lg-4 col-md-9 col-sm-8">
									 <input type="text" class="form-control input-sm "  name="name" id="name" placeholder="Enter name" >
											</div>
										</div>
										<!--<div class="form-group form-show-validation row">
											<label for="price" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Price <span class="required-label">*</span></label>
											<div class="col-lg-4 col-md-9 col-sm-8">
												 <input type="text" class="form-control input-sm numeric_feild_discount"  name="price" id="price" placeholder="Enter Price" maxlength="10" />
											</div>
										</div>-->
										
										
									
										<div class="form-group form-show-validation row">
											<label for="image" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Image<span class="required-label">*</span></label>
											<div class="col-lg-4 ">
												<input type="file" class="form-control form-control-file" name="image" id="image" placeholder="Photo" >
											</div>
											<div class="col-lg-4">
											<img style="display:none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
											</div>
										</div>
										
									</div>
									<div class="card-action">
                  <button type="submit" class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<script>
		$(document).ready(function(){
			$("form").submit(function(){
				$(this).find('button[type=submit]').prop('disabled', true);
			})
			/* validate */

		// validation when select change
		$("#state").change(function(){
			$(this).valid();
		})

		// validation when inputfile change
		$("#uploadImg").on("change", function(){
			$(this).parent('form').validate();
		})

		$("#exampleValidation").validate({
			validClass: "success",
			rules: {
				category_master_id: {required: true},
				name: {
					required: true
				},
				price: {
					required: true,
					numeric: true
				},
				
				image: {
					required: true, 
				
				},
				
				
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
		});
		 function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	}); 
		
		
		
		});

		

		
		
		
	</script>

@endsection