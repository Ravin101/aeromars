@extends('layouts.admin')
@section('title','Time Slot Edit')
@section('content')
<meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Content Header (Page header) -->
    
      
   
  <section class="content">
  <div class="col-md-12">
    <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border" align="center">
             
            </div>
            <!-- /.box-header -->
       
      
            <div class="main-panel">
      <div class="content">
        <div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Time Slots</h4>
			<ul class="breadcrumbs">
			<li class="nav-home">
			<a href="#">
			<i class="flaticon-home"></i>
			</a>
			</li>
			<li class="separator">
			<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
			<a href="#"> Time Slot</a>
			</li>
			<li class="separator">
			<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
			<a href="#">Add Time Slot</a>
			</li>
			
			</ul>
		</div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('timeslot')}}" class="btn btn-secondary pull-right btn-sm" data-toggle="tooltip" title="All Time Slot">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Time Slot
										</a>	
										
									</div>
                </div>
                 <form class="form-horizontal" enctype="multipart/form-data" action="{{url('/timeslot')}}" method="post" id="exampleValidation">
                  @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
					
                      <div class="form-group form-show-validation row">
                       <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Title<span class="required-label">*</span></label>
                       <div class=" col-md-9 col-sm-8">
						<input type="text" id="name" class="form-control" name="name" placeholder="Enter Name" value="{{old('name')}}" >
  
						</div>
					   </div>
						
					

                    <div class="card-action">
					  <button class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
					  <button class="btn btn-danger">Cancel</button>
					</div>
              </div>
             
        
              
         </div>
            </form>
         
          <!-- /.box -->


         
                   
        </div>
        </div>
        <br>
        <br>

   
  </div>
  </section>
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>
  <script>
  
function show_subcategory()
{
  var category_master_id = $('#category_master_id').children('option:selected').attr('value');

  $.ajax({
      
      headers:{

       'X-CSRF-TOKEN':$('#csrf_token').attr('content')
     },
      type:'get',
      url:"{{url('ajax')}}",
     
      data:{category_master_id:category_master_id,"type":"subcategory"},
       success:function(data){

              $('#subcategory_id').html(data);
            }
          });

        }
function show_innercategory()
{
  var subcategory_id = $('#subcategory_id').children('option:selected').attr('value');
  var category_master_id = $('#category_master_id').children('option:selected').attr('value');
  $.ajax({
      
      headers:{

       'X-CSRF-TOKEN':$('#csrf_token').attr('content')
     },
      type:'get',
      url:"{{url('ajax')}}",
     
      data:{subcategory_id:subcategory_id,category_master_id:category_master_id,"type":"innercategory"},
       success:function(data){

              $('#innercategory_id').html(data);
            }
          });

        }
	function show_subinnercategory()
	{
	  var inner_category_id = $('#innercategory_id').val();
	  var subcategory_id = $('#subcategory_id').children('option:selected').attr('value');
	  var category_master_id = $('#category_master_id').children('option:selected').attr('value');
	  $.ajax({
      
      headers:{
       'X-CSRF-TOKEN':$('#csrf_token').attr('content')
     },
      type:'get',
      url:"{{url('ajax')}}",
     
      data:{inner_category_id:inner_category_id,category_master_id:category_master_id,subcategory_id:subcategory_id,"type":"subinnercategory"},
       success:function(data)
	   {
              $('#sub_inner_category_id').html(data);
       }
          });

        }
  function readURL(input)
  {
    if(input.files && input.files[0]){
      var reader = new FileReader();
      reader.onload = function(e){
          $('#contract').attr('src', e.target.result);
        $('#contract').hide();
        $('#contract').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#parker_image").change(function() {
readURL(this);
});

</script>
 <script>
		$(document).ready(function(){
			  CKEDITOR.editorConfig = function (config) {
			config.language = 'es';
			config.uiColor = '#F7B42C';
			config.height = 300;
			config.toolbarCanCollapse = true;

		};
		CKEDITOR.replace('editor1');
			$("form").submit(function(){
				$(this).find('button[type=submit]').prop('disabled', true);
			})
			/* validate */

		

		$("#exampleValidation").validate({
			validClass: "success",
			rules: {
				name: {
					required: true
				},
				decription: {
					required: true
				},
				image: {
					required: true
				},
				thumb: {
					required: true
				},
				
				
				
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
		});
		 function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	}); 
	
	function readURLThumb(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#thumb_preview').attr('src', e.target.result);

		$('#thumb_preview').hide();
		$('#thumb_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#thumb").change(function() {
	readURLThumb(this);
	}); 
		
		
		
		});

		

		
		
		
	</script>
  
  @stop
