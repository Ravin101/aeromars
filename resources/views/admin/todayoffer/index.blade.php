@extends('layouts.admin')
@section('title','TodayOffers')
@section('content')



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Today's offer Tables
        <small>Today's offers Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('todayoffer')}}">Today's offers</a></li>
        <li class="active">Today's offers tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         @if(session('message'))
         <p class="alert alert-success">{{session('message')}}</p>
         @endif
          <div class="box">

            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            <a href="{{url('todayoffer/create')}}" class="btn btn-primary pull-right">Add Today's offer</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Thumb</th>
                  <th>Status</th>
                  <th>Action</th> 
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($todayoffers as $todayoffer)	
                <tr>		
                  <td>{{$i}}</td>
                  <td>{{$todayoffer->title}}</td>
                  <td>{{$todayoffer->price}}</td>
                  <td>{{$todayoffer->type}}</td>
                  <td>{!! $todayoffer->description !!}</td>
                  <td><img style="width:70px;"  src="{{asset('public/images/'.$todayoffer->image)}}"/></td>
				  <td><img style="width:70px;"  src="{{asset('public/images/'.$todayoffer->thumb)}}"/></td>
				  <td><div class="form-group">
                  <div class="slideparam">
					<input type_status="todayoffer" type="checkbox" id="{{$todayoffer->id}}" onChange="save_admin_message_settings({{$todayoffer->id}})" name="status" value="1"  class="status_{{$todayoffer->id}} status_checkbox"   {{($todayoffer->status==1)?"checked":""}}  />
					<label for="{{$todayoffer->id}}" ></label>
					</div>  
                  
                </div></td>
                  <td>
				  
      <form method="post" id="delete_form_{{$todayoffer->id}}" action="{{url('todayoffer/'.$todayoffer->id)}}"  >
        @method('DELETE')
        @csrf
        <input type="hidden" name="token" id="token" value="{{$todayoffer->token}}" />
        <input type="hidden" name="id" id="id" value="{{$todayoffer->id}}" />
        
          
          <a href="{{url('todayoffer/'.$todayoffer->id.'/edit')}}" class="btn btn-sm btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
          <button type="submit" id="{{$todayoffer->id}}" class="btn btn-sm btn-danger delete_product" ><i class="fa fa-trash" aria-hidden="true"></i></button>
              
        </form> 
                  
                  
                  
                  </td>
				   
                </tr>
				<?php $i++;?>
               @endforeach
              
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
$(document).ready(function(){
  $(".delete_product").click(function(e){
    e.preventDefault();
	var id = $(this).attr('id');
	 bootbox.confirm({
		  message:"Are you sure you want to delete this todayoffer?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){
						
			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });

 
 


 

});
</script>
  @endsection