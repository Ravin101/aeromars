@extends('admin.layouts.admin')
@section('title','Banner Create')
@section('content')
<meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Content Header (Page header) -->
    
      
   
  <section class="content">
  <div class="col-md-12">
    <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border" align="center">
             
            </div>
            <!-- /.box-header -->
       
      
            <div class="main-panel">
      <div class="content">
        <div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Banners</h4>
			<ul class="breadcrumbs">
			<li class="nav-home">
			<a href="#">
			<i class="flaticon-home"></i>
			</a>
			</li>
			<li class="separator">
			<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
			<a href="#"> Banner</a>
			</li>
			<li class="separator">
			<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
			<a href="#">Add Banner</a>
			</li>
			
			</ul>
		</div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('banner')}}" class="btn btn-dark pull-right btn-sm" data-toggle="tooltip" title="All Banner">
											<span class="btn-label">
												<i class="fa fa-list"></i>
											</span>
											All Banner
										</a>	
										
									</div>
                </div>
                 <form class="form-horizontal" enctype="multipart/form-data" action="{{url('/banner')}}" method="post" id="exampleValidation">
                  @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
					<div class="form-group form-show-validation row">
											<label for="category_master_id" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Category </label>
											<div class=" col-md-9 col-sm-8">
												 <select class="form-control " name="category_id" id="category_id" onchange="show_subcategory()"   >
                 
												   <option value="" >-Select-</option>
													@foreach($data as $row)
													<option {{old('category_id'==$row->id?"selected":"")}} value="{{$row->id}}">{{$row->name}}</option>
													@endforeach
												   </select>
												<div class="text-danger">{{ $errors->first('category_id') }}</div>
											</div>
										</div>
										
										<div class="form-group form-show-validation row">
											<label for="category_master_id" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Subcategory </label>
											<div class="col-md-9 col-sm-8">
												<select class="form-control " name="subcategory_id" id="subcategory_id"  onchange="show_innercategory()"  >
												<option value=""></option>
												</select>
												<div class="text-danger">{{ $errors->first('subcategory_id') }}</div>
											</div>
										</div>
                      <div class="form-group form-show-validation row">
                       <label for="title" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Title<span class="required-label">*</span></label>
                       <div class=" col-md-9 col-sm-8">
						<input type="text" id="title" class="form-control" name="title" placeholder="Enter Title" value="{{old('title')}}" >
  
						</div>
					   </div>
						<input type="hidden"  class="form-control" id="type" name="type" value="banner" >
					 <div class="form-group form-show-validation row">
					<label for="description" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Description<span class="required-label">*</span></label>
					<div class=" col-md-9 col-sm-8">
					<textarea type="text"  class="form-control" id="editor1"  name="description" placeholder="Enter Description"   >{{old('description')}}</textarea>
				  
					</div>
					</div>
										
									<!--	<div class="form-group form-show-validation row">
											<label for="innercategory_id" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right"> Topic <span class="required-label">*</span></label>
											<div class="col-lg-4 col-md-9 col-sm-8">
												<select class="form-control " name="innercategory_id" id="innercategory_id" onchange="show_subinnercategory()"  >
												<option value=""></option>
												</select>
												<div class="text-danger">{{ $errors->first('innercategory_id') }}</div>
											</div>
										</div>
										-->
					<div class="form-group form-show-validation row">
						<label for="image" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Image<span class="required-label">*</span></label>
						<div class="col-lg-4 ">
							<input type="file" class="form-control form-control-file" name="image" id="image" placeholder="Photo" >
						</div>
						<div class="col-lg-4">
						<img style="display:none;"  id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
						</div>
					</div>
					

                    <div class="card-action">
					  <button class="btn btn-success" name="upload" value="upload" id="submit"  >Save</button>
					  <button class="btn btn-danger">Cancel</button>
					</div>
              </div>
             
        
              
         </div>
            </form>
         
          <!-- /.box -->


         
                   
        </div>
        </div>
        <br>
        <br>

   
  </div>
  </section>
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>
  <script>
  
function show_subcategory(){
	 var category_master_id = $('#category_id').children('option:selected').attr('value')
var APP_URL = {!! json_encode(url('/')) !!};
var csrf = '{{ csrf_token() }}';
$.ajax({
type: 'POST',
data: {category_master_id: category_master_id, '_token':csrf},
url: APP_URL+'/get-sub-cat' ,
success: function(response){
$('#subcategory_id').empty();
$('#subcategory_id').append('<option value="">Select Sub Category</option>');
$.each(response.subcategories,function(index,subcategory){
	
$('#subcategory_id').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>');
})
}
});


}
function show_innercategory()
{
  var subcategory_id = $('#subcategory_id').children('option:selected').attr('value');
  var category_master_id = $('#category_master_id').children('option:selected').attr('value');
  $.ajax({
      
      headers:{

       'X-CSRF-TOKEN':$('#csrf_token').attr('content')
     },
      type:'get',
      url:"{{url('ajax')}}",
     
      data:{subcategory_id:subcategory_id,category_master_id:category_master_id,"type":"innercategory"},
       success:function(data){

              $('#innercategory_id').html(data);
            }
          });

        }
	function show_subinnercategory()
	{
	  var inner_category_id = $('#innercategory_id').val();
	  var subcategory_id = $('#subcategory_id').children('option:selected').attr('value');
	  var category_master_id = $('#category_master_id').children('option:selected').attr('value');
	  $.ajax({
      
      headers:{
       'X-CSRF-TOKEN':$('#csrf_token').attr('content')
     },
      type:'get',
      url:"{{url('ajax')}}",
     
      data:{inner_category_id:inner_category_id,category_master_id:category_master_id,subcategory_id:subcategory_id,"type":"subinnercategory"},
       success:function(data)
	   {
              $('#sub_inner_category_id').html(data);
       }
          });

        }
  function readURL(input)
  {
    if(input.files && input.files[0]){
      var reader = new FileReader();
      reader.onload = function(e){
          $('#contract').attr('src', e.target.result);
        $('#contract').hide();
        $('#contract').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#parker_image").change(function() {
readURL(this);
});

</script>
 <script>
		$(document).ready(function(){
			  CKEDITOR.editorConfig = function (config) {
			config.language = 'es';
			config.uiColor = '#F7B42C';
			config.height = 300;
			config.toolbarCanCollapse = true;

		};
		CKEDITOR.replace('editor1');
			$("form").submit(function(){
				$(this).find('button[type=submit]').prop('disabled', true);
			})
			/* validate */

		

		$("#exampleValidation").validate({
			validClass: "success",
			rules: {
				title: {
					required: true
				},
				decription: {
					required: true
				},
				image: {
					required: true
				},
				thumb: {
					required: true
				},
				
				
				
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
		});
		 function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	}); 
	
	function readURLThumb(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#thumb_preview').attr('src', e.target.result);

		$('#thumb_preview').hide();
		$('#thumb_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#thumb").change(function() {
	readURLThumb(this);
	}); 
		
		
		
		});

		

		
		
		
	</script>
  
  @stop
