@extends('admin.layouts.admin')
@section('content')
<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Banner</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="{{url()->current()}}">Banner</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">All Banners</a>
							</li>
						</ul>
					</div>
					<div class="row">




						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center pull-right">
									 <a href="{{url('admin/banner/create')}}" class="btn btn-dark pull-right btn-sm">
											<span class="btn-label">
												<i class="fa fa-plus"></i>
											</span>
											Add Banner
										</a>

									</div>
								</div>
								<div class="card-body">
								@if(session('message'))
				<p class="alert alert-success">{{session('message')}}</p>
			@endif


									<div class="table-responsive">
										<table id="example1" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th>Sr No.</th>
													<th>Title</th>
													<th>Type</th>
													<th>Decription</th>
													<th>Thumb</th>
													<th>Status</th>
													<th style="width:140px">Action</th>
												</tr>
											</thead>

											<tbody>
											@php $i=1; @endphp
											@foreach($banners as $row)

												<tr>
												<td>{{$i}}</td>
												<td>{{$row->title}}</td>
												<td>{{$row->type}}</td>
											   <td>{!! $row->description !!}</td>
											   <td><a target="_blank" href="{{url($row->image)}}"><img src="{{url($row->thumbnail)}}" width="70" /></a></td>
											   <td><center><div class="slideparam">
					<input type_status="banner" type="checkbox" id="{{$row->id}}" onChange="save_admin_message_settings({{$row->id}})" name="status" value="1"  class="status_{{$row->id}} status_checkbox"   {{($row->status==1)?"checked":""}}  />
					<label for="{{$row->id}}" ></label>
					</div> </center></td>
													<td><form method="post" id="delete_form_{{$row->id}}" action="{{url('banner/'.$row->id)}}"  >
        @method('DELETE')
        @csrf

      <!--  <input type="hidden" name="id" id="id" value="{{$row->id}}" />
    <button type="button" id="{{$row->id}}" url="{{url('banner/'.$row->id)}}" data-toggle="modal" data-target="#view_modal" class="view btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button>  -->
          <a href="{{url('banner/'.$row->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>


          <button type="submit" id="{{$row->id}}" class="btn btn-sm btn-danger delete_product" ><i class="fa fa-trash" aria-hidden="true"></i></button>

        </form>
       </td>
												</tr>
												@php $i++; @endphp
												@endforeach

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-------------------Modal Start----------------->
			  <div class="container">

                        <!-- Trigger the modal with a button -->

<!-- Modal -->

<div class="modal fade" id="view_modal" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header">
	<h4 class="modal-title">Banner Details</h4>
	<button type="button" class="close" data-dismiss="modal">&times;</button>

	</div>
    <div class="modal-body view_body">

    </div>
   <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

</div>
                          </div>
                                </div>
</div>
	<!-------------------Modal End----------------->

		<script>

$(document).ready(function(){
    $(".view").click(function(){
    var id = $(this).attr('id');

    var url = $(this).attr('url');
    $.ajax({
      url:url,
      data:{id:id},
      type:"get",
      success:function(data)
      {
        $(".modal-body").html(data);
      }
    })
    });

$(".delete_product").click(function(e){
e.preventDefault();
var id = $(this).attr('id');
bootbox.confirm({
        message:"Are you want to delete this user?",
        buttons:{
          cancel: {
          label: '<i class="fa fa-times"></i> cancel'
              },
          confirm:{
          label:  '<i class="fa fa-check"></i> confirm'
             },
                         },
 callback: function (result) {
        if(result){

        $('#delete_form_'+id).submit();
        }
      }
})
});

  });









</script>

@endsection
