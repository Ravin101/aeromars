@extends('layouts.admin')
@section('title','Products')
@section('content')



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Gift/Product Tables
        <small>Gift/Product Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('gift_product')}}">Gift/Product</a></li>
        <li class="active">Gift/Product tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         @if(session('message'))
          <p class="alert alert-success">{{session('message')}}</p>
         @endif
          <div class="box">

            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            <a href="{{url('gift-product/create')}}" class="btn btn-primary pull-right">Add Gift/Product</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Thumb</th>
                  <th>Status</th>
                  <th>Action</th> 
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($banners as $banner)	
                <tr>		
                  <td>{{$i}}</td>
                  <td>{{$banner->title}}</td>
                  <td>{{$banner->price}}</td>
                  <td>{{$banner->type}}</td>
                  <td>{!! $banner->description !!}</td>
                  <td><img style="width:70px;"  src="{{asset('public/images/'.$banner->image)}}"/></td>
				  <td><img style="width:70px;"  src="{{asset('public/images/'.$banner->thumb)}}"/></td>
				  <td><div class="form-group">
                  <div class="slideparam">
					<input type_status="banner" type="checkbox" id="{{$banner->id}}" onChange="save_admin_message_settings({{$banner->id}})" name="status" value="1"  class="status_{{$banner->id}} status_checkbox"   {{($banner->status==1)?"checked":""}}  />
					<label for="{{$banner->id}}" ></label>
					</div>  
                  
                </div></td>
                  <td>
				  
      <form method="post" id="delete_form_{{$banner->id}}" action="{{url('banner/'.$banner->id)}}"  >
        @method('DELETE')
        @csrf
        <input type="hidden" name="token" id="token" value="{{$banner->token}}" />
        <input type="hidden" name="id" id="id" value="{{$banner->id}}" />
        
          
          <a href="{{url('gift-product/'.$banner->id.'/edit')}}" class="btn btn-sm btn-primary" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
          <button type="submit" id="{{$banner->id}}" class="btn btn-sm btn-danger delete_product" ><i class="fa fa-trash" aria-hidden="true"></i></button>
              
        </form> 
                  
                  
                  
                  </td>
				   
                </tr>
				<?php $i++;?>
               @endforeach
              
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
$(document).ready(function(){
  $(".delete_product").click(function(e){
    e.preventDefault();
	var id = $(this).attr('id');
	 bootbox.confirm({
		  message:"Are you sure you want to delete this Gift/Product?",
		  buttons:{ cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
			},
			confirm: {
				label: '<i class="fa fa-check"></i> Confirm'
			},
			  },
		    callback: function (result) {
				if(result){
						
			  $('#delete_form_'+id).submit();
				}
			}
		  })//confirm
 });

 
 


 

});
</script>
  @endsection