@extends('layouts.admin')
@section('title','Gift/Product')
@section('content')

<style>
.scrollbox {
  
  overflow-x:scroll;
}
</style>

 


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Add Gift/Product 
        <small>
        Add Gift/Product Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('gift_product')}}">Gift/Product</a></li>
        <li class="active">Add Gift/Product tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         

          <div class="box box-primary">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
		

     <div class="col-md-9">

			
            <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('banner')}}" >
			@csrf
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Title</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{old('title')}}">
					  <div class="text-danger">{{ $errors->first('title')}}</div>
                    </div>
                  </div> 
				  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Price</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="price" name="price" placeholder="Price" value="{{old('name')}}">
					  <div class="text-danger">{{ $errors->first('price')}}</div>
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Type</label>

                    <div class="col-sm-10">
					<select class="form-control" id="type" name="type" placeholder="Type" value="{{old('type')}}">
					<option value="">-Select-</option>
					<option value="gift">Gift Item</option>
					<option value="product">Free Product</option>
					</select>
					
					  <div class="text-danger">{{ $errors->first('type')}}</div>
                    </div>
                  </div>  				  
                 <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Description</label>

                    <div class="col-sm-10">
                      <textarea rows="5"  cols="3" type="text" class="form-control" id="editor1" name="description" placeholder="Description" value="{{old('description')}}"></textarea>
					  <div class="text-danger">{{ $errors->first('description') }}</div>
                    </div>
                  </div>   
                 
                 <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Banner Images</label>

                    <div class="col-sm-5">
					<input  type="file" class="form-control" name="image" id="image" placeholder="Product Images" multiple>
					<div class="text-danger">{{ $errors->first('image') }}</div>
                    </div>
					<div class="col-sm-5">
					<img style="display:none;" id="image_preview"  src="" width="140" class="pull-right" alt="User Image">
					</div>
                  </div>
				  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Banner Thumb</label>

                    <div class="col-sm-5">
					<input  type="file" class="form-control" name="thumb" id="thumb" placeholder="Product Images" >
					<div class="text-danger">{{ $errors->first('thumb') }}</div>
                    </div>
					<div class="col-sm-5">
					<img style="display:none;"  id="thumb_preview"  src="" width="140" class="pull-right" alt="User Image">
					</div>
                  </div>
	
				  
				 <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>	 
            

 
  
  

		
			
           
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/ckeditor/ckeditor.js"></script>
 
  <script src="//cdn.gaic.com/cdn/ui-bootstrap/0.58.0/js/lib/angular.min.js"></script>

  <script>
  $(document).ready(function(){
	  //image preview
    function readURL(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#image_preview').attr('src', e.target.result);

		$('#image_preview').hide();
		$('#image_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#image").change(function() {
	readURL(this);
	});
	
	function readURL1(input) 
	{
		if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		$('#thumb_preview').attr('src', e.target.result);
		$('#thumb_preview').hide();
		$('#thumb_preview').fadeIn(650);
		}
	   reader.readAsDataURL(input.files[0]);
		}
	}

	$("#thumb").change(function() {
	readURL1(this);
	});
	  
	  CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('editor1');

 //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
	  format: 'yyyy-mm-dd',
	 startDate: 'd'
    })
 // $(function () {
            // $('.datepicker').datetimepicker({
                // format: 'YYYY-MM-DD'
            // });
        // });
    $("#main_category").change(function(){
        var id = $("#main_category").find(":selected").val();
       // alert(id);
        $.ajax({
            url:"{{url('get-category-list')}}",
            type:"get",
            data:{id:id},
            success:function(res){
            if(res)
			{
			$("#category").empty();
			$("#category").css("height","30px");
			 $.each(res,function(key,value){
				 $("#category").css("height","100px");
				 console.log("key"+value);
                    $("#category").append('<p style="margin:0px; padding:0px"><input type="checkbox" value="'+value.id+'" name="category[]">&nbsp;'+value.category_name+'</p>	');
				
                });
			}
			else
			{
				$("#category").empty();
			}
            }
        })
    });
  });
  </script>
  @endsection