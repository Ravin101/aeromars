<?php
	
	use Illuminate\Support\Facades\Route;
	use App\Http\Controllers\UserController;
	use App\Http\Controllers\Front\FrontController;
	use App\Http\Controllers\Front\ProductController;
	
	
	
	Route::group(['namespace'=>'App\Http\Controllers\Front'],function(){
		
		Route::get('/','LoginController@index');
		Route::get('/login','LoginController@userLogin'); 
		Route::any('dologin','LoginController@doLogin');  
		Route::get('logout','LoginController@logout');    
		Route::get('/signup','UserController@create');    
		Route::get('/dashboard','LoginController@index'); 
		Route::post ('/signup','UserController@store');
		Route::resource('product','ProductController');
		Route::resource('cart','CartController');
		Route::get('cartlist','CartController@cartList');
		Route::group(['middleware'=>'auth'],function()
		{
		
		Route::get('/myaccount','UserController@myAccount');
		});
	});
	
	
	// Auth::routes();
	// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	// Route::get('success','UserContoller@success_message');
	
	
	Route::group(['prefix'=>'admin','namespace'=>'App\Http\Controllers\Admin'],function(){
		
		Route::view('login','admin.login')->name('admin.login');
		Route::post('login','AdminController@login')->name('admin.authenticate');
	    Route::get('logout', 'UserController@logout')->name('admin.logout');
		
        Route::group(['middleware'=>'admin.auth','disablepreventback'], function(){
			
			
			Route::get('dashboard','DashboardController@index');
			Route::get('notification','UserController@notification_show');
			Route::get('notification-send-users','UserController@send_notification_users');
			Route::resource('/user', 'UserController');
			Route::resource('/category','CategoryController');
			Route::resource('/subcategory','SubcategoryController');
			Route::resource('/product','ProductController');
			Route::resource('/order','OrderController');
			Route::post('/get-sub-cat','ProductController@get_sub_cat');
			Route::resource('/banner','BannerController');
			Route::resource('/promocode','PromoCodeController');
			Route::post('banner-add/{id}','PlaylistController@banner_add_post');
			Route::get('profile','UserContoller@profile')->name('profile');
			
		});
		
	});
	
	
