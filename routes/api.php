<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace'=>'App\Http\Controllers\API'], function(){
	
	Route::post('login','PassportController@login');
	Route::post('register','PassportController@register');
	

Route::group(['middleware' => ['auth:api','user_accessible']], function(){
Route::post('details', 'PassportController@details');
Route::post('edit-profile', 'PassportController@edit_profile');
Route::post('get-profile', 'PassportController@get_profile');
Route::post('change-password', 'PassportController@change_password');
Route::post('forgot-password', 'PassportController@forgot_password');
Route::post('logout', 'PassportController@logout');
Route::get('categories', 'PassportController@categories');
Route::get('products', 'PassportController@products');
Route::get('subcategories', 'PassportController@subcategories');
Route::get('get-notification', 'PassportController@notification');
Route::get('banners', 'BannerController@get_banner');
Route::get('states','PassportController@states');
Route::get('cities','PassportController@cities');
Route::post('promocodes','PassportController@promocodes');
Route::post('timeslots','PassportController@timeslot');
Route::get('dashboard','PassportController@dashboard');


});
	
});

