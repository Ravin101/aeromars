<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Passport;
use App\Models\User;
use DB;
use Hash;
use File;
use Str;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMail;
use Auth;
use App\Models\Channel;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Banner;
use App\Models\DeviceToken;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderCancel;
use App\Models\OrderShipping;
use App\Models\OrderDelivery;
use App\Models\OtherAddress;
use App\Models\TimeSlot;
use App\Models\City;
use App\Models\State;
use App\Models\PromoCode;

class PassportController extends Controller
{
	 public $successStatus = 200;
	 public $failureStatus = 401;
	
	public function __construct()
    {
        $this->api_per_page = config('constants.api_per_page');
    }
	
	public function sendResponse($result, $message)
    {
    	$response = [
            'status' => true,
			'message' => $message,
            'data'    => $result,

        ];


        return response()->json($response, 200);
    }
	
	/**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
    	$response = [
            'status' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
	private function check_email($email)
    {
		return $user = User::where(["email"=>$email,"deleted_at"=>0])->count();
	}
   
   public function login(Request $request)
	{
		     $validator = Validator::make($request->all(),[
			'email' => 'required|email',
            'password' => 'required',
			'device_type'=>'required',
	        'device_token'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}

        $user = User::where(['email'=> $request->email,'deleted_at'=>0])->first();
     
        if($user)
        {
			
        	if($user->status==0)
	        {
			
	        	$data['status'] = false;
				$data['message'] = 'Your account deactivated by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, 200);
	        }
	        if($user->deleted_at==1)
	        { 
		      
	        	$data['status'] = false;
				$data['message'] = 'Your account deleted by administrator.Please contact to administrator.';
				$data['data'] = array();
				return response()->json($data, 200);
	        }
			if (Hash::check($request->password, $user->password))
			{
				
				try{
				$token = $user->createToken('Laravel Password Grant Client')->accessToken;
				$user->remember_token = Str::random(100);
				$user->save();
			$this->check_device_token($user->id,$request->device_token,$request->device_type);
				

				$data['status'] = true;
				$data['message'] = 'Login Success';
				$path = url('/');
				$user['path'] =  $path;
				$user['token'] =  $token;
				$data['data'] =  $user;
				}
		     catch (\Exception $e) {
             dd($e);
			 }

				//$response['data'] =  $user;
				$msg="Dear ".$user->first_name."<br>You are successfully login";
				//$this->notification($user->device_token, "Login", $msg,"login");

			    return response()->json($data, 200);
			}
			else
			{
				
				$data['status'] = false;
				$data['message'] = 'Email and password incorrect';
				$data['data'] = array();
			    return response()->json($data, 200);
			}

    }
	else
	{ 
        
				$data['status'] = false;
				$data['message'] = 'User does not exist';
				$data['data'] = array();
			    return response()->json($data, 200);
    }

	}
	
	 public function check_device_token($user_id,$device_token,$device_type)
	{
		$count = DeviceToken::where(['device_type'=>$device_type,'device_token'=>$device_token])->count();
		if($count == 0)
		{
			$devicetoken = new DeviceToken;
			$devicetoken->user_id = $user_id; 
			$devicetoken->device_token = $device_token; 
			$devicetoken->device_type = $device_type; 
			$devicetoken->save(); 
		}
	}
	
	public function delete_device_token($user_id,$device_token)
	{
		$device_token = DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->first();
		if(!$device_token)
			DeviceToken::where(['user_id'=>$user_id,'device_token'=>$device_token])->delete();
			
		

	}
	 
	 public function register(Request $request)
	{
		 // echo "<pre>";
		 // print_r($_POST);exit;
		     $validator = Validator::make($request->all(),[
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email',
            'password' => 'required',
			'device_type'=>'required',
			'device_token'=>'required',
			'phoneno'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}

		    $user = new User;
			$user->first_name = ucwords($request->first_name);
			$user->last_name = ucwords($request->last_name);
			$user->phoneno = $request->phoneno;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$user->address = ucwords($request->address);
			$user->save();
			$user['token'] =  $user->createToken('MyApp')->accessToken;
            // echo '<pre>';
			// print_r($user); exit;
			// $user->device_type = $request->device_type ??  "NA";
			// $user->device_token = $request->device_token ??  "NA";

            $this->check_device_token($user->id,$request->device_token,$request->device_type);
             // $user['token'] =  $user->createToken('MyApp')->accessToken;
		
           return $this->sendResponse($user, 'User registered successfully');
         

 }
		
		public function forgot_password(Request $request)
    {

        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

             $user = User::where("email",$request->email)->first();
             $data['status']  =  true;
             $data['message'] =  'Your password reset link sent to your registered e-mail id';


        if($user!=null)
        {

           // send sms to user
           // $digits = 6;
           // $pass = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
        	$link = url('reset-password');
           $message = "Hello ".ucwords($user->fullname)."<br>Your Reset Password link is <br> $link";
          // $res = $this->send_mobile_message_url($user->mobile_no,$message);


           // $user->password = bcrypt($pass);
           // $user->save();

            return response()->json($data, 200);
        }
        else{
             $response = "This e-mail id is not registered";
            return $this->sendError($response);
        }
    }
	
	public function reset(Request $request)
    {

        // $credentials = request()->validate([
        //     'email' => 'required|email',
        //     'token' => 'required|string',
        //     'password' => 'required|string|confirmed'
        // ]);
        $validator = Validator::make($request->all(),[
        'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|confirmed'
        ]);
        if($validator->fails())
        {


            return back()
            ->withInput()
            ->withErrors($validator);
        }
// echo "<pre>";
//     	dd(request());
//     	exit;
        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["status"=>false,"message" => "Invalid token provided"], 400);
        }

        return response()->json(["status"=>true,"message" => "Password has been successfully changed"]);
    }
		
    public function edit_profile(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'first_name' => 'required',
			'last_name' => 'required',
			 'email' => 'required|email',	
			'phoneno'=>'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{

				if($request->email != $user->email)
				{
					if($this->check_email($request->email) > 0)
					{
						$data['status'] = false;
						$data['message']= "The email already been taken.";
						return response()->json($data);
					}
				}


				if($request->hasFile('profile_pic') && $request->profile_pic->isValid())
	           {

	               $extension = $request->profile_pic->extension();
	               $fileName  = "images/".time().".$extension";
	               $request->profile_pic->move(public_path('images'),$fileName);
	           }
	           else
	           {
	               $fileName = $user->profile_pic;
	           }


				$user->first_name = ucwords($request->first_name);
				$user->last_name = ucwords($request->last_name);
				$user->phoneno = $request->phoneno;
				$user->email = $request->email;
				$user->address = $request->address;
				$user->profile_pic = $fileName;
				if($user->save())
				{
					$user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User profile updated successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);
				}
			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
	             $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
	 
	 
	 public function get_user_email($id)
	{
		$user = User::where('id',$id)->first();
		return $user;
	}
	
	public function get_profile(Request $request)
    {

			$user = Auth::user();
			$path = url('public/images/').'/';
			if($user)
			{
				 $user['path'] = $path;
		         $data['status'] =  true;
		         $data['path'] =  $path;
	             $data['message'] =  'User Detail Found Successfully';
	             $data['data'] =  $user;
		         return response()->json($data, $this->successStatus);

			}
			else
			{
				 $data['status']  = false;
		         $data['path'] =  $path;
				 $data['message'] = 'User Not Found';
		         return response()->json($data, $this->successStatus);
			}

    }
     public function change_password(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'userid' => 'required',
			'old_password' => 'required',
			'new_password' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
             if($validator->errors()->first('userid'))
			 $data['message']	= $validator->errors()->first('userid');
             else if($validator->errors()->first('old_password'))
			 $data['message']	= $validator->errors()->first('old_password');
             else if($validator->errors()->first('new_password'))
			 $data['message']	= $validator->errors()->first('new_password');
              return response()->json($data);
			}
			$user = User::find($request->userid);
			$path = url('public/images/').'/';
			if($user)
			{
				 if(Hash::check($request->old_password,$user->password))
				 {
				 	$user->update(['password' => Hash::make($request->new_password)]);
				    $data['status']  =  true;
		            $data['message'] =  'Your Password Changed Successfully';
			        return response()->json($data, $this->successStatus);
				 }
				 else
				 {
				 	 $data['status']  =  false;
		             $data['message'] =  'Old Password Does Not Match';
			         return response()->json($data, $this->successStatus);
				 }

			}
			else
			{
				 $data['status']  = false;
	             $data['message'] = 'User Not Found';
		         return response()->json($data, $this->successStatus);
			}

    }
	
	public function social_login_new(Request $request){
        try{

            $data = $request->all();

            $validator = Validator::make($request->all(),
                [
                    'social_type'  => 'required|in:GooglePlus,Facebook,Twitter',
                    'social_id' => 'required',
                    'device_type' => 'required',
                    'device_token' => 'required',
                    'email' => 'nullable',
                    'profile_picture' => 'nullable'
                ]
            );
            if($validator->fails()){
                $response['status']     = false;
                $response['message']    = $this->validationHandle($validator->messages());
                $this->response($response);
            }else{

                $check_email = false;

                $check = User::where('social_id',$data['social_id'])->exists();

                if(isset($data['email']) && $data['email'] != '')
                $check_email = User::where('email',$data['email'])->exists();

                $updateArr = [];

                if($check){

                    if(isset($data['email'])){
                        $updateArr['email'] = $data['email'];
                    }

                    if(count($updateArr) > 0)
                    User::where('social_id',$data['social_id'])->where('social_type',$data['social_type'])->update($updateArr);

                    $user = User::where('social_id',$data['social_id'])->first();


                    if($request->get('profile_picture') && !isset($user->profile_picture)){

                        $imgurl = $request->get('profile_picture');
                        $image_name = 'user_'.$user->id.'.jpeg';

                        $destinationPath = '/profile/';
                        $ppath = public_path().'/'.$destinationPath;

                        file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                        if($this->isImage('public/profile/'.$image_name)){

                            $user_img = 'public'.$destinationPath.$image_name;
                            User::where('id',$user->id)->update(['profile_picture' => $user_img]);


                        }
                    }

                    $userData = User::getProfile($user->id);
                    $jwtResponse = User::authorizeToken($user);
                    $userData->security_token = @$jwtResponse['token'];

                    $response['status']     = true;
                    $response['message']    = 'User details';
                    $response['data'] = $userData;

                    if($user){
                        UserDevices::deviceHandle([
                            "id"       =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]);
                    }

                }
                else{
                    if($check_email){

                        $user = User::where('email',$data['email'])->first();

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                $updateArr['profile_picture'] = $user_img;
                            }
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        User::where('email',$data['email'])->update($updateArr);

                        $user = User::where('email',$data['email'])->first();

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }
                    }
                    else{

                        if(isset($data['email'])){
                            $updateArr['email'] = $data['email'];
                        }

                        $updateArr['social_type'] = $data['social_type'];
                        $updateArr['social_id'] = $data['social_id'];

                        $user = User::Create($updateArr);

                        if($request->get('profile_picture')){

                            $imgurl = $request->get('profile_picture');
                            $image_name = 'user_'.$user->id.'.jpeg';

                            $destinationPath = '/profile/';
                            $ppath = public_path().'/'.$destinationPath;

                            file_put_contents($ppath.$image_name,file_get_contents($imgurl));
                            if($this->isImage('public/profile/'.$image_name)){

                                $user_img = 'public'.$destinationPath.$image_name;
                                User::where('id',$user->id)->update(['profile_picture' => $user_img]);
                            }
                        }

                        $userData = User::getProfile($user->id);
                        $jwtResponse = User::authorizeToken($user);
                        $userData->security_token = @$jwtResponse['token'];

                        $response['status']     = true;
                        $response['message']    = 'User details';
                        $response['data'] = $userData;

                        if($user){
                            UserDevices::deviceHandle([
                                "id"       =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        }

                    }
                }

                $this->response($response);

            }
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage().' on '.$e->getLine();
            $response['data'] = [];

            $this->response($response);
        }
    }
	
	 public function logout(Request $request)
    {
    	$validator = Validator::make($request->all(),[
			'device_token' => 'required',
		]);

			if($validator->fails())
			{
             $data['status'] = false;
			 $data['message']	= $validator->errors()->first();
              return response()->json($data);
			}
			$user = Auth::user();
			if($user)
			{
				$request->user()->token()->revoke();
				$this->delete_device_token($user->id,$request->device_token);
				$data['status'] = true;
				$data['message'] = 'Logout successfully';

			    return response()->json($data, 200);
			}
			else
			{
				 $data['status']  = false;
				 $data['message'] = 'User not found';
		         return response()->json($data, $this->successStatus);
			}

    }
	
	// Get all category
     public function categories(Request $request)
    {
		// echo "123";exit;
		$path = url('public/images/').'/';

    	$category = Category::where(['is_deleted'=>0,'status'=>1])->orderByDesc('id')->get();
		// echo'<pre>';
		// print_r ($category);
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Categories Found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Categories Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
    // Get all subcategory
     public function subcategories(Request $request)
    {
		$path = url('public/images/').'/';

    	$category = Subcategory::where(['is_deleted'=>0])->with('category')->orderByDesc('id')->get();
    	if($category)
    	{
    		$data['status']  = true;
			$data['message'] = 'Subcategories Found';
			$data['path']	 = $path;
			$data['data'] = $category;

		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Subcategories Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
	
	 // Get all prodcuts
    public function products(Request $request)
    {
	   	
		$path = url('public/images/').'/';
		
    	$products = Product::where(['is_deleted'=>0,'status'=>1])->with('category','subcategory','ProductImages')->orderByDesc('id')->get();
    	if($products)
    	{
    		$data['status']  = true;
			$data['message'] = 'Products Found';
			$data['path']	 = $path;
			$data['data'] = $products;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Products not found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
	
	// Get all banners
	public function banners(Request $request)
    {
		$path = url('public/images/').'/';

    	$banner = Banner::where(['is_deleted'=>0,'status'=>1])->with('playlist')->orderByDesc('id')->get();
    	if($banner)
    	{
    		$data['status']  = true;
			$data['message'] = 'Banner Found';
			$data['path']	 = $path;
			$data['data'] = $banner;
		    return response()->json($data, $this->successStatus);
    	}
    	else
    	{
    		$data['status']  = false;
			$data['message'] = 'Banner Not Found';
			$data['path']	 = $path;
			$data['data'] = '';
		    return response()->json($data, $this->successStatus);
    	}
    }
	
	//get all states
	
	public function states()
	{
		$states = DB::table('states')->get();
		if($states)
		{
			$data['status']= true;
			$data['message']= 'State list';
			$data['data']= $states;
		}
		return response()->json($data);   
	}
	
	public function cities()
	{
		$cities = DB::table('cities')->get();
		if($cities)
		{
			$data['status']= true;
			$data['message']= 'City list';
			$data['data']= $cities;
		}
	   return response()->json($data);
	}
	
	public function promocodes(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if($validator->fails())
		{
            $data['status'] = false;
             if($validator->errors()->first('code'))
			 $data['message']	= $validator->errors()->first('code');
                
             return response()->json($data);
        }
		
		 $promocode = PromoCode::where(['code'=>$request->code,'is_deleted'=>0])->first();
		if($promocode)
		{
			
			return $this->sendResponse($promocode, 'Promo Code Applied successfully.');
		}
		else{
			 $data['status']              = false;
			 $data['message'] 			  = 'Invalid Promo Code';
			 $data['data']                = '';
			  return response()->json($data);
		}
	} 
	 
	  public function order_save($order_no,$user_id,$price)
	  {
		  $order = new Order();
		  $order->order_no = $order_no;
		  $order->price = $price;
		  $order->transaction_id = '';
		  $order->status = 'pending';
		  $order->user_id = $user_id;
		  $order->order_date = date('y-m-d');
		  $order->save();
		  
	  }
	  
	  public function timeslot(Request $request)
	  {
		  $user = Auth::user();
		  $timeslots =  Timeslot::where(['is_deleted'=>1, 'status'=>1])->get();
           if($timeslots)
		   {
			   $data['status']   =  true;
			   $data['message']  =  "Time Slot is available";
			   $data ['data']    =  $timeslots;
		   }
          else
		  {
			   $data['status']   =  false;
			   $data['message']  =  "Time Slot not available";
			   $data['data']     =  '';
		  }		
             return response()->json($data,$this->successStatus);   
 	
	  }
	    
		public function dashboard(Request $request)
		{
		$category = Category::where(['is_deleted'=>0; 'status'=>'1'])->with('subcategories')->get();
		$banners   = Banner::where(['is_deleted'=>0;'status'=>'1'])->get();
		
		if($category || $banners)
		{
			$data['status']   = true;
			$data['message']  = "Dashboard details found";
			$data['category'] =  $category;
			$data['banners']  =  $banners;
		}
		else
		{
			$data['status']   = false;
			$data['message']  = "Dashboard details not found";
			$data['category'] = '';
			$data['banners']  = '';
		}
	    return response->json($data);	
	} 
	 public function notification($token, $title, $description,$type)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
		$url = url('/')."/public/images/logo.jpg";
        $notification = [
            'title' => $title,
			'description'=>$description,
			'type'=>$type,
			//'image'=>$url,
            'sound' => true,
        ];

       $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'data' => $notification
        ];

        $headers = [
            'Authorization: key=AAAAtrVxdWM:APA91bFIaXDMYYdf1Qmwpj-DEqrHe98W-bJQc3hST6BhdBqq9vtXYNdzcOKk_-L24Fxd5HpaRgUkTqrEG4IMUpN-Zuab_PRG1T79Q-l7aCJCw7SU7lkrIWqnMNK4H057fqC2o91YCid6',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }



    /////////////////////////////////////////////////////////////////////////////////////
		
}
		

	

