0<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use Validator;
use Session;
use Auth;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Innercategory;
use App\Models\User;
use Image;


class BannerController extends Controller
{
     public function index()
    {
	    $banners = Banner::where(['is_deleted'=>'0'])->get();
        return view('admin.banner.index',compact('banners'));
    }

       public function create()
    {

	      $data = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
          $dataa = Subcategory::select('name')->where(['is_deleted'=>0])->get();

		return view('admin.banner.create',compact('data','dataa'));


    }

	public function store(Request $request)
    {

        $validator = validator::make($request->all(),[
		'title' => 'required',
		'description' => 'required',
		'image' =>  'required',
		]);
		if($validator->fails())
		{
			return back()
			->withInput()
			->withErrors($validator);
		}

			$users = new Banner	;

              if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
				  $ff = $request->image->move(public_path('uploads/images'),$fileName);

                  $imageName = time().".$extension";
                  $destinationPath1 = public_path('/uploads/thumbnail');

                  $img = Image::make($ff->getRealPath());

                  $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath1.'/'.$imageName);


          }
            else
            {
                $fileName  ='uploads/images';

            }


			if($request->category_id>0)
			{
			$banner_cat_type = "main";
			$category=$request->category_id;
			}
			if($request->subcategory_id>0)
			{
			$banner_cat_type = "sub";
		    $category=$request->subcategory_id;
			}
			if($request->innercategory_id>0)
			{
			$banner_cat_type = "inner";
		    $category=$request->innercategory_id;
			}
			if($request->sub_inner_category_id>0)
			{
			$banner_cat_type = "sunbinner";
		    $category=$request->sub_inner_category_id;
			}


			// echo Auth::guard('admin')->user()->id;exit;
			$users->admin_id =  Auth::guard('admin')->user()->id;
			$users->title    = ucwords($request->title);
		 	$users->type     = $request->type;
		 	$users->category_id  = $request->category_id ? $request->category_id:0;
			$users->subcategory_id = $request->subcategory_id ? $request->subcategory_id:0;
		    $users->innercategory_id = $request->innercategory_id ? $request->innercategory_id:0;
		 	$users->thumbnail = str_replace("images","thumbnail",$fileName);
			$users->description  = ucfirst($request->description);
			$users->image = $fileName;
			$users->banner_cat_type  = $banner_cat_type ?? "";
			$users->category  = $category ?? 0;
			$users->save();



			if($request->type=="banner")
			return redirect('banner')->with('message','Banner is added successfully');

    }

	 public function get_sub_cat(Request $request)
	 {
		 $subcategories = SubCategory::where('category_id',$request->category_master_id)->select('id','name')->get();
      return response()->json([
           'subcategories' => $subcategories
]);

	 }


	  public function edit(Banner $banner)
    {
        //

          $data = Category::select('name','id')->where(['is_deleted'=>0])->get();
          $subcategory = Subcategory::where(['is_deleted'=>0,'id'=>$banner->subcategory_id])->get();
          // $innercategory = Innercategory::where(['is_deleted'=>0,'id'=>$banner->innercategory_id])->get();
          return view('admin.banner.edit',compact('banner','data','subcategory'))->with('banner',$banner);

    }

	public function update(Request $request, Banner $banner)
    {
        //
		$validator = validator::make($request->all(),[
		'title' => 'required',
		'type' => 'required',
		'description' => 'required',
		]);
		if($validator->fails())
		{
			return redirect('banner/'.$request->id.'/edit')
			->withInput()
			->withErrors($validator);
		}


			if($request->hasFile('image') && $request->image->isValid())
		   {
				  $extension = $request->image->extension();
				  $fileName  = "uploads/images/".time().".$extension";
				  $ff = $request->image->move(public_path('uploads/images'),$fileName);

                  $imageName = time().".$extension";
                  $destinationPath1 = public_path('/uploads/thumbnail');

                  $img = Image::make($ff->getRealPath());

                  $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($destinationPath1.'/'.$imageName);
			}
			else
			{
				$fileName  = $banner->image;
			}

		   if($request->category_id>0)
			{
			$banner_cat_type = "main";
			$category=$request->category_id;
			}
			if($request->subcategory_id>0)
			{
			$banner_cat_type = "sub";
		    $category=$request->subcategory_id;
			}
			if($request->innercategory_id>0)
			{
			$banner_cat_type = "inner";
		    $category=$request->innercategory_id;
			}


			$banner->admin_id   =  Auth::guard('admin')->user()->id;
		 	$banner->title          = ucwords($request->title);
		 	$banner->type           = $request->type;
			$banner->category_id  = $request->category_id ? $request->category_id:0;
			$banner->subcategory_id 		= $request->subcategory_id ? $request->subcategory_id:0;
			$banner->innercategory_id 		= $request->innercategory_id ? $request->innercategory_id:0;
			$banner->sub_inner_category_id = $request->sub_inner_category_id ? $request->sub_inner_category_id:0;
			$banner->banner_cat_type    = $banner_cat_type ?? "";
		 	$banner->thumbnail          = str_replace("images","thumbnail",$fileName);;
			$banner->description    = ucfirst($request->description);
			$banner->image          = $fileName;
			$banner->category    		= $category ?? 0;
			$banner->save();
			if($request->type=="banner")
			return redirect('banner')->with('message','Banner is updated successfully');

	 }
	 public function destroy(Banner $banner)
    {


        if($banner)
        {
            $banner->is_deleted=1;
            $banner->save();

            return back()->with('message','Banner deleted successfully');
        }
        else{
            return back()->with('message','Something wrong');
        }
    }


}
