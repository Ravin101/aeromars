<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\OtherAddress;
use Illuminate\Http\Request;

class OtherAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OtherAddress  $otherAddress
     * @return \Illuminate\Http\Response
     */
    public function show(OtherAddress $otherAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OtherAddress  $otherAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(OtherAddress $otherAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OtherAddress  $otherAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OtherAddress $otherAddress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OtherAddress  $otherAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(OtherAddress $otherAddress)
    {
        //
    }
}
