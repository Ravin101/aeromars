<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Session;
use Hash;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use Image;
use File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "string";dd();
         $datas = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
         return view('admin.category.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
          $Category = Category::max('id');
          $validator = Validator::make($request->all(),[
					'name'=>'required|min:2|max:255',
					'image'=>'required',
					
    ]);
				if($validator->fails()){
				  return redirect('/category/create')
				  ->withInput()
				  ->withError($validator);
				}

           
              if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "images".time().".$extension";
                $ff = $request->image->move(public_path('images'),$fileName);
				$thumbnail = time().".$extension";;
				$destinationPath = public_path('thumbnail');
				$img = Image::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);

				
          }
            else
            {
                $fileName  ='default.jpg';
                
            }
			
	$user = new Category;
    $user->name = ucfirst($request->name);
    $user->image = $fileName;
    $user->thumbnail  = $thumbnail;
    $user->save();
    $type = "Category Added Suceesfully";
    $msg = "your";
	
  
  
    //session::flash('message','Thank you !Your process is complete successfuly.');
    return redirect('admin/category')->with('Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $users = $category;
        return view('admin.category.edit',compact('users'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = validator::make($request->all(),[
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "uploads/images/".time().".$extension";
                $ff = $request->image->move(public_path('uploads/images'),$fileName);

            	$thumbnail = time().".$extension";;
				$destinationPath = public_path('/uploads/thumbnail');
				$img = ImageResize::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);

				$thumbnail = "uploads/thumbnail/".$thumbnail;
                       
            }
            else
            {
                $fileName  = $category->image;
                $thumbnail = $category->thumbnail;
            }
        $category->name = ucfirst($request->name);
        $category->image = $fileName;
        $category->thumbnail = $thumbnail;

        if($category->save())
        {
            return redirect('category')->with('message','Category Updated Successfully');
        }
        else
        {
            return back()->with('message','Category Not Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
     
        $category->is_deleted = 1;
		
        if($category->save())
        {
            return redirect('admin/category')->with('message','Category Deleted Successfully');
        }
        else
        {
            return back()->with('message','Category Not Deleted');
        }
    }
}
