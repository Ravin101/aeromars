<?php
	
	namespace App\Http\Controllers\Admin;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use Auth;
	use App\Models\Admin;
	
	class AdminController extends Controller
	{
		
	    public function login(Request $request)
		
		{
			$credential = $request->only('email', 'password');
			
			if(Auth::guard('admin')->attempt($credential,$request->remember))
			{
				$user = Admin::where('email',$request->email)->first();
				Auth::guard('admin')->login($user);
				return redirect('admin/dashboard')->with('status','You are successfully login.');
			}
			return redirect('admin/dashboard')->with('status','Failed to process login');
		}
		
		
		public function logout()
		
		{
			Auth::guard('admin')->logout();
			return redirect('admin.login');
		}
	}
