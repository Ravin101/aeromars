<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\OrderCancel;
use Illuminate\Http\Request;

class OrderCancelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderCancel  $orderCancel
     * @return \Illuminate\Http\Response
     */
    public function show(OrderCancel $orderCancel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderCancel  $orderCancel
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderCancel $orderCancel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderCancel  $orderCancel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderCancel $orderCancel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderCancel  $orderCancel
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderCancel $orderCancel)
    {
        //
    }
}
