<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\PromoCode;
use Illuminate\Http\Request;
use Validator;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function index()
    {

		$promocodes = PromoCode::where(['is_deleted'=>0])->orderBy("id","desc")->get();
		return view('admin.promocode.index',compact('promocodes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
		// echo "1"; exit;
        return view ('admin.promocode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	      // echo "2"; exit;

		 $validator = Validator::make($request->all(),[
            'code' => 'required',
            'discount' => 'required',
            'type' => 'required',
    ]);

			if($validator->fails()){
			 return back()
			->withInput()
            ->withErrors($validator);
			}
        $promocode = new PromoCode();
		$promocode->code = $request->code ?? 0;
        $promocode->type = $request->type ?? 'flat';
        $promocode->discount = $request->discount ?? 0;

        if($promocode->save())

        {
			// echo '<pre>';
			// print_r ($promocode);exit;
			return redirect('/promocode')->with('message','Promo code added successfully');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */

	public function show(PromoCode $promoCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function edit(PromoCode $promoCode)
    {
        return view('admin.promocode.edit',compact('promoCode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromoCode $promoCode)
    {
         {
       $validator = Validator::make($request->all(),[
            'code' => 'required',
            'discount' => 'required',
            'type' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }

        $promocode->code = $request->code ?? 0;
        $promocode->type = $request->type ?? 'flat';
        $promocode->discount = $request->discount ?? 0;
        if($promocode->save())
        {
            return redirect('admin/promocode')->with('message','Prome code updated successfully');
        }
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PromoCode  $promoCode
     * @return \Illuminate\Http\Response
     */
   public function destroy(PromoCode $promocode)
    {

        $promocode->is_deleted = 1;
        if($promocode->save())
        {
            return redirect('/promocode')->with('message','Promo code deleted successfully');
        }
    }
}
