<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;
use App\Models\ProductImage;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use ImageResize;
use File;
use Auth;


  class ProductController extends Controller
{
    public $paginate_no;

  public function __construct()
    {
        $this->paginate_no = config('constants.paginate_no');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->query())
        {
            $search = trim($request->search);
            $products = Product::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere('name','like','%'.$search.'%');
                $query->orWhere('description','like','%'.$search.'%');
                $query->orWhere('qty','like','%'.$search.'%');
                $query->orWhere('price','like','%'.$search.'%');
                $query->orWhere('mrp','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id','desc')->paginate($this->paginate_no);
        }
        else
        {
            $search = '';
         $products = Product::where(['is_deleted'=>0])->orderBy('id','desc')->paginate(50);
        }
		// echo '<pre>';
        // print_r($products);exit();

        return view('admin.product.index',compact('products','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

		$categories = Category::where(['is_deleted'=>0])->get();
		$subcategories = Subcategory::where(['is_deleted'=>0])->get();
		$units = DB::table('units')->get();

		return view('admin.product.create',compact('categories','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'qty' => 'required',
            'min_qty' => 'required',
            'max_qty' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'description' => 'required',
            'image' => 'required',
            'unit' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $images = [];
        if($request->hasFile('image'))
        {
          if($files=$request->file('image')){
                  foreach($files as $file){
                      $name = "uploads/images/".time().$file->getClientOriginalName();
                      $ff = $file->move(public_path('/uploads/images'),$name);

                      $imageName = time().$file->getClientOriginalName();
                      $destinationPath = public_path('/uploads/images');
                      $destinationPath1 = public_path('/uploads/thumbnail');

                      $img = ImageResize::make($ff->getRealPath());

                      $img->resize(400, 400, function ($constraint) {
                        $constraint->aspectRatio();
                        })->save($destinationPath1.'/'.$imageName);

                    //   $img->resize(800, 800, function ($constraint) {
                    //   $constraint->aspectRatio();
                    //   })->save($destinationPath.'/'.$imageName);



                      $images[]=$name;
                  }
              }

                $fileName = implode("|",$images);


        }
        else
        {
              $fileName = "uploads/images/default.jpg";
        }
        if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
        {
            $extension1 = $request->thumbnail->extension();
            $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
            $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);

            $imageNameThumb = time().".$extension1";;
            $destinationPath1 = public_path('/uploads/thumbnail');

            $img1 = ImageResize::make($ff1->getRealPath());
            $img1->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
                 })->save($destinationPath1.'/'.$imageNameThumb);
        }
        else
        {
                $fileNameThumb = "default.jpg";
        }

		$product = new Product();
		$product->name = ucwords($request->name);
		$product->category_id = $request->category_id ?? 0;
		$product->subcategory_id = $request->subcategory_id ?? 0;
		$product->code = $request->code ?? 0;
		$product->description = ucwords($request->description);
		$product->qty = $request->qty ?? 0;
		$product->min_qty = $request->min_qty ?? 0;
		$product->max_qty = $request->max_qty ?? 0;
		$product->mrp = $request->mrp ? $request->mrp:0;
		$product->price = $request->price ? $request->price:0;
		$product->date_of_packing = date("Y-m-d",strtotime($request->date_of_packing));
		$product->date_of_expire = date("Y-m-d",strtotime($request->date_of_expire));
		$product->image = $fileName;
		$product->thumbnail = $fileNameThumb;
		$product->weight = $request->weight ?? 0;
		$product->unit = $request->unit ?? "";
		$product->product_type = $request->product_type ?? 0;
		if($product->save())
		{
            if(count($images) > 0)
            {
                foreach ($images as $key => $value) {

                    $productimages = new ProductImage;
                    $productimages->product_id = $product->id;
                    $productimages->image = $value;
                    $productimages->thumbnail = str_replace("images","thumbnail",$value);
                    $productimages->save();
                }

            }
			return redirect('admin/product')->with('message','Product Added Successfully');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
	 public function get_sub_cat(Request $request)
	 {
		 $subcategories = SubCategory::where(['category_id'=>$request->category_id,'is_deleted'=>0])->select('id','name')->get();
      return response()->json([
           'subcategories' => $subcategories
]);

	 }


    public function show(Product $product)
    {

	    $id=Auth::guard('admin')->user()->id;

       $users = DB::table('products')
	   ->where(['deleted_at'=>'0','admin_id'=>$id, 'id'=>$request->id])->get();

	    $output='';
        $output.= '
        <div class="table-responsive">
           <table class="table table-bordered">';
		   {
	  $i=0;
	  foreach($users as $row)
	  {
           $output .= '
               <tr>
                    <td width="30%"><label>Product Name</label></td>
                    <td width="70%">'.ucfirst($row->name).'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Category</label></td>
                    <td width="70%">'.$row->cat_name.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Subcategory</label></td>
                    <td width="70%">'.$row->sub_name.'</td>
               </tr>

			   <tr>
                    <td width="30%"><label>Code</label></td>
                    <td width="70%">'.$row->code.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Description</label></td>
                    <td width="70%">'.$row->description.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Quantity</label></td>
                    <td width="70%">'.$row->qty.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Min Quantity</label></td>
                    <td width="70%">'.$row->min_qty.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Max Quantity</label></td>
                    <td width="70%">'.$row->max_qty.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Price</label></td>
                    <td width="70%">'.$row->price.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>MRP</label></td>
                    <td width="70%">'.$row->mrp.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Unit</label></td>
                    <td width="70%">'.$row->unit.'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Weight</label></td>
                    <td width="70%">'.$row->weight.'</td>
               </tr>


			   <tr>
                    <td width="30%"><label>Created Date</label></td>
                    <td width="70%">'.date("d-M-Y",strtotime($row->created_at)).'</td>
               </tr>
			   <tr>
                    <td width="30%"><label>Image</label></td>
                    <td width="70%"><a href="'.url('public/images/'.$row->image).'" ><img src="'.url('public/thumbnail/'.$row->thumb).'" width="80" /></a></td>
               </tr>


			   <tr>
                    <td width="30%"><label>Status</label></td>
                    <td width="70%">'.$status.'</td>
               </tr>

			   ';
	$i++;
      }
      }
     $output .= "</table></div>";
     echo $output;
    }


    public function edit(Product $product)
    {
        // echo $product->ProductSingleImage->image;die;
		$categories = Category::where(['is_deleted'=>0])->get();
		$subcategories = Subcategory::where(['is_deleted'=>0,'category_id'=>$product->category_id])->get();
		$units = DB::table('units')->get();
          // $product = Product::where(['is_deleted'=>0])->orderByDesc('id')->get();
		//   echo "<pre>";
		//   print_r ($units);exit;
		  return view('admin.product.edit',compact('product','categories','subcategories','units'));
    }


    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'qty' => 'required',
            'min_qty' => 'required',
            'max_qty' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'description' => 'required',
            'unit' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
        $images = [];

        if($request->hasFile('image'))
        {
          if($files=$request->file('image')){
                  foreach($files as $file){
                      $name = "uploads/images/".time().$file->getClientOriginalName();
                      $ff = $file->move(public_path('/uploads/images'),$name);

                      $imageName = time().$file->getClientOriginalName();
                      $destinationPath = public_path('/uploads/images');
                      $destinationPath1 = public_path('/uploads/thumbnail');

                      $img = ImageResize::make($ff->getRealPath());

                      $img->resize(400, 400, function ($constraint) {
                        $constraint->aspectRatio();
                        })->save($destinationPath1.'/'.$imageName);

                    //   $img->resize(800, 800, function ($constraint) {
                    //   $constraint->aspectRatio();
                    //   })->save($destinationPath.'/'.$imageName);


                      $images[]=$name;
                  }
              }

                $fileName = implode("|",$images);


        }
        else
        {
              $fileName = $product->image;
        }

        if($request->hasFile('thumbnail') && $request->thumbnail->isValid())
        {
          $extension1 = $request->thumbnail->extension();
          $fileNameThumb  = "uploads/thumbnail/". time().".$extension1";
          $ff1 = $request->thumbnail->move(public_path('uploads/thumbnail'),$fileNameThumb);

         $thumbnail1 = time().".$extension1";;
                  $destinationPath1 = public_path('/uploads/thumbnail');
                  $img1 = ImageResize::make($ff1->getRealPath());
                  $img1->resize(200, 200, function ($constraint) {
                  $constraint->aspectRatio();
                  })->save($destinationPath1.'/'.$thumbnail1);
        }
        else
        {
              $fileNameThumb = $product->thumbnail;
        }


		$product->name = ucwords($request->name);
		$product->category_id = $request->category_id ?? 0;
		$product->subcategory_id = $request->subcategory_id ?? 0;
		$product->code = $request->code ?? 0;
		$product->description = ucwords($request->description);
		$product->qty = $request->qty ?? 0;
		$product->min_qty = $request->min_qty ?? 0;
		$product->max_qty = $request->max_qty ?? 0;
		$product->mrp = $request->mrp ? $request->mrp:0;
		$product->price = $request->price ? $request->price:0;
		$product->date_of_packing = date("Y-m-d",strtotime($request->date_of_packing));
		$product->date_of_expire = date("Y-m-d",strtotime($request->date_of_expire));
		$product->image = $fileName;
		$product->thumbnail = $fileNameThumb;
		$product->weight = $request->weight ?? 0;
		$product->unit = $request->unit ?? "";
		$product->product_type = $request->product_type ?? 0;
		if($product->save())
		{
            if(count($images) > 0)
            {
                foreach ($images as $key => $value) {
                    $productimages = new ProductImage;
                    $productimages->product_id = $product->id;
                    $productimages->image = $value;
                    $productimages->thumbnail = str_replace("images","thumbnail",$value);
                    $productimages->save();
                }

            }
         return redirect('admin/product')->with('message','Product Updated Successfully');
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
   public function destroy(Product $product)
    {

        $product->is_deleted = 1;
        if($product->save())
        {
            return redirect('admin/product')->with('message','Product deleted successfully');
        }
        else
        {
            return back()->with('message','Product not deleted');
        }
    }
}
