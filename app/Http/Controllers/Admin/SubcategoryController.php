<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Subcategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use ImageResize;
use Image;
use File;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $datas = Subcategory::with('category')->where(['is_deleted'=>0])->get();
		// echo '<pre>';
        // print_r($datas);exit();

        return view('admin.subcategory.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $datas = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view('admin.subcategory.create',compact('datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
   {
	     $validator = Validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        'image' => 'required',
   ]);

    	if($validator->fails()){
        return back()
      ->withInput()
      ->withErrors($validator);
    }




              if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "images".time().".$extension";
                $ff = $request->image->move(public_path('images'),$fileName);
				$thumbnail = time().".$extension";;
				$destinationPath = public_path('thumbnail');
				$img = Image::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);


          }
            else
            {
                $fileName  ='default.jpg';

            }
     $subcategory = new Subcategory;

	 $subcategory->name = ucfirst($request->name);
	 $subcategory->category_id = $request->category_id;
	 $subcategory->image = $fileName;
     $subcategory->thumbnail = $thumbnail;

    $subcategory->save();
    $type = "Subcategory added successfully";
    $msg = "your";

       return redirect('admin/subcategory')->with('message','Subcategory Added Successfully');

  }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {

        $categories = Category::where(['is_deleted'=>0])->orderByDesc('id')->get();
        return view('admin.subcategory.edit',compact('categories','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        //
        $validator = validator::make($request->all(),[
        'category_id' => 'required',
        'name' => 'required',
        ]);
        if($validator->fails())
        {
            return back()
            ->withInput()
            ->withErrors($validator);
        }
		    if($request->hasFile('image') && $request->image->isValid())
           {
                $extension = $request->image->extension();
                $fileName  = "images".time().".$extension";
                $ff = $request->image->move(public_path('images'),$fileName);
				$thumbnail = time().".$extension";;
				$destinationPath = public_path('thumbnail');
				$img = Image::make($ff->getRealPath());
				$img->resize(200, 200, function ($constraint) {
				$constraint->aspectRatio();
				})->save($destinationPath.'/'.$thumbnail);


          }
            else
            {
                $fileName  ='default.jpg';
                $thumbnail = $subcategory->thumbnail;
            }
        $subcategory->name = ucfirst($request->name);
        $subcategory->category_id = $request->category_id;
        $subcategory->image = $fileName;
        $subcategory->thumbnail = $thumbnail;

        if($subcategory->save())
        {
            return redirect('/subcategory')->with('message','Subcategory updated successfully');
        }
        else
        {
            return back()->with('message','Subcategory not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        //
        $subcategory->is_deleted = 1;
        if($subcategory->save())
        {
            return redirect('admin/subcategory')->with('message','Subcategory deleted successfully');
        }
        else
        {
            return back()->with('message','Subcategory not deleted');
        }
    }
}
