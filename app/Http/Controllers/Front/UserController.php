<?php
	
	namespace App\Http\Controllers\Front;
	use App\Http\Controllers\controller;
	use Illuminate\Http\Request;
	use App\Models\User;
	use Validator;
	use App\Models\Product;
	use App\Models\Banner;
	use App\Models\Category;
	
	class UserController extends Controller
	{
		
		public function index()
		{
			return view('front.dashboard.index');
		}
		
		public function create()
		
		{
			return view('front.users.create');	
		}
		
		
	   public function myAccount()
	       {
		   // echo "123";exit;
		       return view ('front.users.myaccount');
		   }
		
		
		public function store(Request $request)
		
		{
			
			// echo "<pre>";
			// print_r($_POST); exit;
		    $validator = Validator::make($request->all(),[
			'email' => 'required|email',
            'password' => 'required',
			'phoneno'=>'required',
			]);
			
		    $user = new User;
			$user->email = $request->email;
			$user->phoneno = $request->phoneno;
			$user->password = bcrypt($request->password);
			$user->save();
			
			
			$request->session()->flash('message', 'You are registered successfully');
			return redirect('/signup');
			
		}
		
		public function show(Registration $registration)
		{ 
			
			
		}
		
		
		public function edit(Registration $registration)
		{
			
			
		}
		
		
		
		public function update(Request $request, Registration $registration)
		{
			
			
		}
		
		function destroy()
		{
			
		}
		
	}
