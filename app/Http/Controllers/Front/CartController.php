<?php
	
	namespace App\Http\Controllers\Front;
	use App\Http\Controllers\Controller;
	use App\Models\Cart;
	use App\Models\Product;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Validator;
	use Session;
	use Auth;
	
	class CartController extends Controller
	{
		/**
			* Display a listing of the resource.
			*
			* @return \Illuminate\Http\Response
		*/
		public function index()
		{
			
		}
		
		/**
			* Show the form for creating a new resource.
			*
			* @return \Illuminate\Http\Response
		*/
		public function create()
		{
			//
		}
		
		/**
			* Store a newly created resource in storage.
			*
			* @param  \Illuminate\Http\Request  $request
			* @return \Illuminate\Http\Response
		*/
		
		
		
		
		public function cart_check($user_id,$product_id)
		
		{
		  $cart = Cart::where(['user_id'=>$user_id,'product_id'=>$product_id])->count();
		}
		
		
		public function cartList(Request $request)
		{
		
		   $products = Cart::where('user_id',Auth::user()->id)->get();
		
		   return view ('front/cart/cartlist',compact('products'));
			
	      }
		
		public function store(Request $request)
		{
			$validator = validator::make($request->all(),[
            'product_id' => 'required|numeric|min:1',
            'qty' => 'required|numeric|min:1',
            ]);
            if($validator->fails())
            {
                $data['result'] = false;
                $data['errors'] = $validator->errors();
                return response()->json($data);
				
			}
            else
            {
                $user_id = 1;
                $product = Product::find($request->product_id);
				
                if($this->cart_check($user_id,$request->product_id) == 0)
                {
                    $cart = new Cart;
				}
                else
                {
                    $cart = Cart::where(['user_id'=>$user_id,'product_id'=>$request->product_id])->first();
				}
				$cart->user_id = 1;
				$cart->product_id = $request->product_id ?? 0;
				$cart->name = $product->name;
				$cart->thumbnail = $product->thumbnail;
				$cart->qty = $request->qty;
				$cart->price = $product->price;
				$cart->discount = $product->discount ?? 0;
				$cart->total_price = $request->qty * $product->price ?? 0;
				$cart->save();
				$data['result'] = true;
				$data['data'] = $cart;
				$data['message'] = '<p class="alert alert-success">Product added to cart.</p>';
				
				return response()->json($data);
				
                
				
				
			}
		}
		
		/**
			* Display the specified resource.
			*
			* @param  \App\Models\Cart  $cart
			* @return \Illuminate\Http\Response
		*/
		public function show(Cart $cart)
		{
			return view('front.product.show',compact('product'));
		}
		
		/**
			* Show the form for editing the specified resource.
			*
			* @param  \App\Models\Cart  $cart
			* @return \Illuminate\Http\Response
		*/
		public function edit(Cart $cart)
		{
			//
		}
		
		/**
			* Update the specified resource in storage.
			*
			* @param  \Illuminate\Http\Request  $request
			* @param  \App\Models\Cart  $cart
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request, Cart $cart)
		{
			
			
		}
		
		/**
			* Remove the specified resource from storage.
			*
			* @param  \App\Models\Cart  $cart
			* @return \Illuminate\Http\Response
		*/
		public function destroy(Cart $cart)
		{
		   
		  
		}
	}
