<?php
	
	namespace App\Http\Controllers\Front;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use App\Providers\RouteServiceProvider;
	use Illuminate\Foundation\Auth\AuthenticatesUsers;
	use App\Models\Product;
	use App\Models\Banner;
	use App\Models\User;
	use App\Models\Cart;
	use App\Models\Category;
	use Validator;
	use Auth;
	
	class LoginController extends Controller
	{
		
		public function userLogin()
		
		{
			return view('front.users.login');	
		}
	    
		
		public function doLogin(Request $request)
		
		{
		     // echo "123"; exit;
		  
			
    $validatedData = $request->validate([
'email' => 'required|email',
'password' => 'required|min:5'

]);
// echo $request->email; 
// echo $request->password; exit;

			
			if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
			{
			 
				return redirect('dashboard');
			}
			else{
				return back()->with('error_message','Invalid email or password');
			}
			
			
		}
		
		
	  	public function index(Request $request)
        
		{
            $products = Product::where(['is_deleted'=>0])->orderby('id','desc')->paginate(6);
            $banners = Banner::where(['is_deleted'=>0])->orderby('id','desc')->get();
			
		    $search = '';
            $cat = '';
			
            if($request->query())
            {
                $search = trim($request->search);
                $cat = trim($request->cat);
                $products = Product::where(['is_deleted'=>0])
                ->when($search,function($q,$search){
                    return $q->where(function($query) use ($search){
                        $query->orWhere('name','like','%'.$search.'%');
                        $query->orWhere('description','like','%'.$search.'%');
                        $query->orWhere('qty','like','%'.$search.'%');
                        $query->orWhere('price','like','%'.$search.'%');
                        $query->orWhere('mrp','like','%'.$search.'%');
                        $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
					});
				})
                ->when($cat,function($q,$cat){
                    return $q->whereHas('category', function($query) use ($cat)
                    {
                        $query->where('name', 'LIKE', '%'.$cat.'%');
					});
				})
                ->orderByDesc('id','desc')->paginate(6);
			}
			
            return view('front.dashboard.index',compact('products','banners','search','cat'));
		}
		
		// public function product_detail($id)
		// {
			// $products = Product::where('id', $id)->first();
			// return view('front.product_details',compact('products'));
		// }
		
		
		public function logout()
		
		 {
               // echo "123";exit;		
		    Auth::logout();
			return redirect('dashboard');
		}
	}
