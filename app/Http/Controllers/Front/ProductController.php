<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use ImageResize;
use File;
use Auth;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->query())
        {
           echo $search = trim($request->search);die;
            $products = Product::where(['is_deleted'=>0])
            ->where(function($query) use ($search){
                $query->orWhere('name','like','%'.$search.'%');
                $query->orWhere('description','like','%'.$search.'%');
                $query->orWhere('qty','like','%'.$search.'%');
                $query->orWhere('price','like','%'.$search.'%');
                $query->orWhere('mrp','like','%'.$search.'%');
                $query->orWhere('created_at','like','%'.date('Y-m-d',strtotime($search)).'%');
            })
            ->orderByDesc('id','desc')->paginate($this->paginate_no);
        }
        else
        {
            $search = '';
         $products = Product::where(['is_deleted'=>0])->orderBy('id','desc')->paginate(50);
        }
		// echo '<pre>';
        // print_r($datas);exit();

        return view('admin.product.index',compact('datas','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	}


    public function show(Product $product)
    {


		return view('front.product.show',compact('product'));
    }


    public function edit(Product $product)
    {
		
	}

    public function update(Request $request, Product $product)
    {
       
	   
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
   public function destroy(Product $product)
    {

        $product->is_deleted = 1;
        if($product->save())
        {
            return redirect('/product')->with('message','Product deleted successfully');
        }
        else
        {
            return back()->with('message','Product not deleted');
        }
    }
}
