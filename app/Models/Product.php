<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function ProductImages(){

	  return $this->hasMany(ProductImage::class);
  }
  public function ProductSingleImage(){

    return $this->hasOne(ProductImage::class);
}
    public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function subcategory()
	{
		return $this->belongsTo(Subcategory::class);
	}
}
