<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
	
	public function user(){
		return $this->belongsTo(User::class);
	}
   
    public function OrderDetail(){
		return $this->hasMany(OrderDetails::class);
	}
	
	public function OrderCancel(){
		return $this->hasOne(OrderCancel::class);
	}
	
	public function OrderDelivery(){
		return $this->hasOne(OrderDelivery::class);
	}
	
	public function OrderShipping(){
		return $this->hasOne(OrderShipping::class);
	}
}
